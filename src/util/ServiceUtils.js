// @flow
export const ServiceIconTypes = {
  ChargingPoint: "charging-station",
  Clothing: "tshirt",
  CommunityCentre: "building",
  EmergencyAccommodation: "hotel",
  EmergencyRelief: "procedures",
  Food: "utensils",
  HealthService: "medkit",
  Laundry: "socks",
  LongTermAccommodation: "home",
  Shower: "shower",
  Toilet: "toilet-paper",
  Transport: "car",
  Water: "tint",
  WiFi: "wifi"
};

export const getIconForService = (serviceId: string) => {
  const serviceIcon = ServiceIconTypes[serviceId];
  if (serviceIcon) {
    return serviceIcon;
  }
  return "tint";
};
