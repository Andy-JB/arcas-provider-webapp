// @flow
class StringUtils {
  filterArrayByString(mainArr, searchText) {
    if (!searchText || searchText === "") {
      return mainArr;
    }

    return mainArr.filter(itemObj =>
      this.searchInObj(itemObj, searchText.toLowerCase())
    );
  }

  searchInObj(itemObj, searchText) {
    let flag = false;
    Object.values(itemObj).forEach(element => {
      if (typeof element === "string" || element instanceof String) {
        if (this.searchInString(element, searchText)) {
          flag = true;
        }
      }
    });

    return flag;
  }

  searchInString(value, searchText): boolean {
    return value
      .toString()
      .toLowerCase()
      .includes(searchText);
  }

  truncateString(str: string, length: number = 50) {
    // eslint-disable-line
    const maxLength = length;
    if (str.length > maxLength) {
      return `${str.substring(0, maxLength)}...`;
    }
    return str;
  }
}
export default new StringUtils();
