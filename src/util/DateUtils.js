// @flow
import moment from "moment";

class DateUtils {
  get currentDay(): string {
    const date = moment(new Date(), "YYYY/MM/DD");
    const day = date.format("dddd");
    return day;
  }

  get currentTime(): string {
    const date = moment(new Date(), "YYYY/MM/DD");
    const time = date.format("HH:mm");
    return time;
  }

  isTimeInBetween(
    startTime: string,
    endTime: string,
    currentTime: string
  ): boolean {
    const startMoment = moment(startTime, "HH:mm");
    const endMoment = moment(endTime, "HH:mm");
    const currentMoment = moment(currentTime, "HH:mm");
    return (
      currentMoment.isAfter(startMoment) && currentMoment.isBefore(endMoment)
    );
  }

  getFormattedTime(timeIn24Hr: string): string {
    const timeMoment = moment(timeIn24Hr, "HH:mm");
    return timeMoment.format("hh:mm A");
  }

  getFormattedTimeDuration(startTime: string, endTime): string {
    return `${this.getFormattedTime(startTime)} - ${this.getFormattedTime(
      endTime
    )}`.toLowerCase();
  }
}
export default new DateUtils();
