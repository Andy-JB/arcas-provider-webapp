// @flow
import { Form } from "mobx-react-form";
import validatorjs from "validatorjs";
import dvr from "mobx-react-form/lib/validators/DVR";
import moment from "moment";

class MobxForm extends Form {

  constructor(fields: Array<Object>, onSuccess: Function, onError: Function) {
    super(
      { fields },
      {
        hooks: {
          onSuccess,
          onError
        }
      }
    );
  }

  prefillData(data: Object) {
    Object.keys(data).forEach(dataKey => {
      if (this.fields.has(dataKey)) {
        const field = this.fields.get(dataKey);
        let prefilledValue = data[dataKey];
        if (data[dataKey].constructor.name === "Timestamp") {
          prefilledValue = moment(data[dataKey].seconds * 1000).format(
            "YYYY-MM-DD"
          );
        }
        field.value = prefilledValue;
      }
    });
  }

  plugins() {
    const rules = {
      website: {
        function: (value) =>
          value.match(
            /^(?:(?:https?):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/
          ),
        message: "The :attribute url is not in the required format http://"
      }
    };

    return {
      dvr: dvr({
        package: validatorjs,
        extend: ({validator,form}) => {
          // here we can access the `validatorjs` instance (validator)
          // and we can add the rules using the `register()` method.
          Object.keys(rules).forEach(key =>
            validator.register(key, rules[key].function, rules[key].message)
          );
        }
      }),
    };
  }
}

export default MobxForm;
