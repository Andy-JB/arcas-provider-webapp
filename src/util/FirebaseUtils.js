// @flow
import { action } from "mobx";
import { db, storage } from "../firebase";

type FirebaseWhere = {
  key: string,
  clause?: string,
  value: any,
  comparator: any
};

class FirebaseUtils {
  @action
  uploadImage(image: any, path: string) {
    return storage
      .ref()
      .child(path)
      .put(image)
      .then(snapshot => snapshot.ref.getDownloadURL());
  }

  @action
  uploadMultipleImages(
    imageReference: string,
    images: Array<any>
  ): Promise<any> {
    const urls = [];
    if (images.length > 0) {
      const uploadPromises = images.map((image, index) => {
        const validImageFile = image.imagePreviewFile;
        if (validImageFile != null) {
          return this.uploadImage(
            validImageFile,
            `${imageReference}${index}`
          ).then(url => {
            urls.push({ url, index });
          });
        }
        urls.push({ url: image.url, index });
        return Promise.resolve();
      });
      return Promise.all(uploadPromises).then(() =>
        urls.sort(this.compare).map(image => image.url)
      );
    }
    return Promise.resolve(urls);
  }

  compare(a: any, b: any) {
    if (a.index < b.index) return -1;
    if (a.index > b.index) return 1;
    return 0;
  }

  @action
  moveDocument(oldCollection: string, newCollection: string, docId: string) {
    return db
      .collection(oldCollection)
      .doc(docId)
      .get()
      .then(doc => {
        if (doc.exists) {
          // add in new location
          db.collection(newCollection)
            .add(doc.data())
            .then(() => {
              // delete old document
              db.collection(oldCollection)
                .doc(docId)
                .delete();
            });
        }
        return doc;
      });
  }

  @action
  updateDocument(collection: string, docId: string, data: Object) {
    const updatedDocument = data;
    updatedDocument.updatedAt = new Date();
    return db
      .collection(collection)
      .doc(docId)
      .update(updatedDocument)
      .catch(err => {
        throw err;
      });
  }

  @action
  updateGeoDocument(collection: string, docId: string, data: Object) {
    const updatedDocument = data;
    updatedDocument.d.updatedAt = new Date();
    return db
      .collection(collection)
      .doc(docId)
      .update(updatedDocument)
      .catch(err => {
        throw err;
      });
  }

  @action
  addDocument(collection: string, data: Object) {
    const updatedDocument = data;
    updatedDocument.updatedAt = new Date();
    updatedDocument.createdAt = new Date();
    return db
      .collection(collection)
      .add(updatedDocument)
      .catch(err => {
        throw err;
      });
  }

  @action
  setNewDocument(collection: string, docId: string, data: Object) {
    return db
      .collection(collection)
      .doc(docId)
      .set(data)
      .catch(err => {
        throw err;
      });
  }

  @action
  getDocument(collection: string, docId: string) {
    return db
      .collection(collection)
      .doc(docId)
      .get()
      .catch(err => {
        throw err;
      });
  }

  @action
  createReferenceWhere(
    collectionRef: string,
    whereDataClause: Array<FirebaseWhere>
  ) {
    let ref = db.collection(collectionRef);
    whereDataClause.forEach(whereClause => {
      ref = ref.where(
        whereClause.key,
        whereClause.comparator,
        whereClause.value
      );
    });
    return ref;
  }

  @action
  createReferenceWhereWithLimitOrder(
    collectionRef: string,
    whereDataClause: Array<FirebaseWhere>,
    limit: number,
    orderField: string
  ) {
    let ref = db.collection(collectionRef);
    whereDataClause.forEach((whereClause: any) => {
      ref = ref.where(
        whereClause.key,
        whereClause.comparator,
        whereClause.value
      );
    });
    return ref.orderBy(orderField).limit(limit);
  }

  getDocReference(path: string) {
    return db.doc(path);
  }
}

export default new FirebaseUtils();
