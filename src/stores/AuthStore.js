// @flow
import { observable, action, computed } from "mobx";
import { auth } from "../firebase";

const actionCodeSettings = {
  // URL you want to redirect back to. The domain (www.example.com) for this
  // URL must be whitelisted in the Firebase Console.
  encodedUrl: (email, fname, lname) =>
    `https://arcas-bd925.firebaseapp.com/signup?email=${email}&fname=${fname}&lname=${lname}`,
  url: "https://arcas-bd925.firebaseapp.com/signup?",
  // This must be true.
  handleCodeInApp: true
};

class AuthStore {
  @observable
  isInitialLoad = true;

  @observable
  isLoading = false;

  @observable
  errors = undefined;

  @observable
  userAuthData = null;

  constructor() {
    this.resetStore();
  }

  resetStore() {
    this.isInitialLoad = true;
    this.isLoading = false;
    this.errors = undefined;
    this.userAuthData = null;
  }

  @computed
  get isAuthenticated(): boolean {
    return this.userAuthData != null;
  }

  @computed
  get userId(): string {
    if (this.userAuthData) {
      return this.userAuthData.uid;
    }
    return "";
  }

  @computed
  get userEmail(): string {
    if (this.userAuthData) {
      return this.userAuthData.email;
    }
    return "";
  }

  @action
  async doSignOut() {
    auth.signOut();
  }

  @action
  async doChangePassword(
    email: string,
    oldPassword: string,
    newPassword: string
  ) {
    if (this.userAuthData) {
      try {
        await auth.signInWithEmailAndPassword(email, oldPassword);
        await auth.currentUser.updatePassword(newPassword);
        await this.doSignOut();
        this.isLoading = false;
      } catch (e) {
        throw e;
      }
    }
  }

  @action
  async login(email: string, password: string) {
    this.isLoading = true;
    this.errors = undefined;
    try {
      const res = await auth.signInWithEmailAndPassword(email, password);
      this.userAuthData = res.user;
      this.isLoading = false;
    } catch (e) {
      this.errors = e.message;
      this.isLoading = false;
      throw e;
    }
  }

  @action
  async signup(email: string, password: string) {
    this.isLoading = true;
    this.errors = undefined;
    try {
      const res = await auth.createUserWithEmailAndPassword(email, password);
      this.userAuthData = res.user;
      this.isLoading = false;
      return this.userAuthData;
    } catch (err) {
      this.errors = err.message;
      this.isLoading = false;
      throw err;
    }
  }

  @action
  async inviteUser(email: string, fname: string, lname: string): void {
    this.isLoading = true;
    this.errors = undefined;
    try {
      const encodedUrl = actionCodeSettings.encodedUrl(
        encodeURIComponent(email),
        encodeURIComponent(fname),
        encodeURIComponent(lname)
      );
      await auth.sendSignInLinkToEmail(email, encodedUrl);
      window.localStorage.setItem("emailForSignIn", email);
      this.isLoading = false;
    } catch (err) {
      this.errors = err.message;
      this.isLoading = false;
      throw err;
    }
  }

  @action
  async forgotPassword(email: string): void {
    this.isLoading = true;
    this.errors = undefined;
    try {
      await auth.sendPasswordResetEmail(email);
      this.isLoading = false;
    } catch (err) {
      this.errors = err.message;
      this.isLoading = false;
      throw err;
    }
  }

  @action
  async verifyPasswordResetCode(actionCode: string): void {
    this.isLoading = true;
    this.errors = undefined;
    try {
      await auth.verifyPasswordResetCode(actionCode);
      this.isLoading = false;
    } catch (err) {
      this.errors = err.message;
      this.isLoading = false;
      throw err;
    }
  }

  @action
  async resetPassword(actionCode: string, newPassword: string) {
    this.isLoading = true;
    this.errors = undefined;
    try {
      await auth.confirmPasswordReset(actionCode, newPassword);
      this.isLoading = false;
    } catch (err) {
      this.errors = err.message;
      this.isLoading = false;
      throw err;
    }
  }
}

export default new AuthStore();
