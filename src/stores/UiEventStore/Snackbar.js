// @flow
import React from "react";
import { observable } from "mobx";
import SnackbarComponent from "@material-ui/core/Snackbar";
import SnackbarContentWrapper from "common/components/SnackBarContentWrapper";

class Snackbar {
  @observable
  anchorOrigin: Object;

  @observable
  anchorOrigin: Object;

  @observable
  open: boolean;

  @observable
  autoHideDuration: number;

  @observable
  variant: string;

  @observable
  message: string;

  constructor() {
    this.anchorOrigin = {
      vertical: "bottom",
      horizontal: "left"
    };
    this.open = false;
    this.autoHideDuration = 5000;
    this.variant = "info";
    this.message = "";
    this.onOpen = this.onOpen.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  onClose = () => {
    this.resetSnackbar();
  };

  onOpen = (message: string, variant: string) => {
    this.open = true;
    this.variant = variant;
    this.message = message;
  };

  resetSnackbar = () => {
    this.open = false;
    this.message = "";
  };

  uiComponent() {
    return (
      <SnackbarComponent
        anchorOrigin={this.anchorOrigin}
        open={this.open}
        onClose={this.onClose}
        autoHideDuration={this.autoHideDuration}
      >
        <SnackbarContentWrapper
          variant={this.variant}
          onClose={this.onClose}
          message={this.message}
        />
      </SnackbarComponent>
    );
  }
}

export default Snackbar;
