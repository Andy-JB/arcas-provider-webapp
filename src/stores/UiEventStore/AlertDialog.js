// @flow
import React from "react";
import { observable, action } from "mobx";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

function Transition(props: Object) {
  return <Slide direction="up" {...props} />;
}

const defaultParams = {
  title: "Confirm",
  description: "",
  positiveButtonTitle: "Yes",
  negativeButtonTitle: "No",
  onPositiveButtonClicked: null,
  onNegativeButtonClicked: null
};

class AlertDialog {
  @observable
  open: boolean;

  @observable
  title: string;

  @observable
  description: string;

  @observable
  positiveButtonTitle: string;

  @observable
  negativeButtonTitle: string;

  @observable
  onPositiveButtonClicked: any;

  @observable
  onNegativeButtonClicked: any;

  @observable
  key: any;

  constructor() {
    this.open = false;
    this.title = "";
    this.description = "";
    this.positiveButtonTitle = "";
    this.negativeButtonTitle = "";
    this.onPositiveButtonClicked = null;
    this.onNegativeButtonClicked = null;
  }

  @action
  onOpen = (dialogParams: Object) => {
    this.open = true;
    const finalParams = this.syncDefaultParams(dialogParams);
    this.title = finalParams.title;
    this.description = finalParams.description;
    this.positiveButtonTitle = finalParams.positiveButtonTitle;
    this.negativeButtonTitle = finalParams.negativeButtonTitle;
    this.onPositiveButtonClicked = finalParams.onPositiveButtonClicked;
    this.onNegativeButtonClicked = finalParams.onNegativeButtonClicked;
    this.key = finalParams.key;
  };

  onClose = () => {
    this.resetAlertDialog();
  };

  handlePositiveClicked = () => {
    if (this.onPositiveButtonClicked) {
      this.onPositiveButtonClicked();
    }
    this.onClose();
  };

  handleNegativeClicked = () => {
    if (this.onNegativeButtonClicked) {
      this.onNegativeButtonClicked();
    }
    this.onClose();
  };

  resetAlertDialog() {
    this.open = false;
    this.title = "";
    this.description = "";
    this.positiveButtonTitle = "";
    this.negativeButtonTitle = "";
    this.onPositiveButtonClicked = null;
    this.onNegativeButtonClicked = null;
  }

  uiComponent() {
    return (
      <Dialog
        key={this.key}
        open={this.open}
        TransitionComponent={Transition}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{this.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {this.description}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleNegativeClicked} color="primary">
            {this.negativeButtonTitle}
          </Button>
          <Button onClick={this.handlePositiveClicked} color="primary">
            {this.positiveButtonTitle}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  @action
  syncDefaultParams = (dialogParams: Object): Object => {
    const paramKeys = Object.keys(defaultParams);
    paramKeys.forEach(key => {
      if (Object.prototype.hasOwnProperty.call(dialogParams, key)) {
        if (dialogParams[key] !== undefined) {
          defaultParams[key] = dialogParams[key];
        }
      }
    });
    return defaultParams;
  };
}

export default AlertDialog;
