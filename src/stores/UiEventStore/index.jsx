// @flow
import { observable, action } from "mobx";
import Snackbar from "./Snackbar";
import Lightbox from "./Lightbox";
import AlertDialog from "./AlertDialog";

class UiEventStore {
  @observable
  snackbar: Snackbar;

  @observable
  lightbox: Lightbox;

  @observable
  alertDialog: AlertDialog;

  constructor() {
    this.snackbar = new Snackbar();
    this.lightbox = new Lightbox();
    this.alertDialog = new AlertDialog();
  }

  @action
  triggerSnackbarSuccess(message: string) {
    this.snackbar.onOpen(message, "success");
  }

  @action
  triggerSnackbarFail(message: string) {
    this.snackbar.onOpen(message, "error");
  }
}

export default new UiEventStore();
