// @flow
import { observable, action, computed, runInAction } from "mobx";
import FirebaseUtils from "util/FirebaseUtils";
import { db } from "../firebase";

const orgPath = "organisation";
const userPath = "users";

class UserStore {
  SUPER: Array<string> = ["super"];

  ORGADMIN: Array<string> = ["orgadmin", ...this.SUPER];

  ORGUSER: Array<string> = ["orguser", ...this.ORGADMIN];

  @observable
  all: Array<any>;

  @observable
  user: any;

  @observable
  isUploadingImage: boolean = false;

  @observable
  pendingReferences: number;

  constructor() {
    this.resetStore();
  }

  resetStore() {
    this.user = null;
    this.all = [];
    this.pendingReferences = 0;
    this.isUploadingImage = false;
  }

  @action
  createOrUpdate(data: Object) {
    const orgDocumentRef = db.doc(`organisation/${data.organisation}`);
    const roleDocumentRef = db.doc(`roles/orgadmin`);

    db.collection(userPath).add({
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      organisation: orgDocumentRef,
      isOnboarded: false,
      role: roleDocumentRef
    });
  }

  @action
  saveUser(userId: string, orgId: string, user: Object, role: ?string): void {
    const orgDocumentRef = FirebaseUtils.getDocReference(
      `organisation/${orgId}`
    );
    const roleName = role || "orgadmin";
    const roleDocumentRef = FirebaseUtils.getDocReference(`roles/${roleName}`);
    return FirebaseUtils.setNewDocument(userPath, userId, {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      role: roleDocumentRef,
      organisation: orgDocumentRef,
      isOnboarded: false
    })
      .then(ref => ref)
      .catch(err => {
        throw err;
      });
  }

  @action
  saveOrganization(userId: string, user: Object): void {
    return FirebaseUtils.addDocument(orgPath, {
      orgName: user.name,
      phone: user.phone,
      email: user.email,
      abn: user.abn
    })
      .then(ref => {
        this.saveUser(userId, ref.id, user, undefined);
      })
      .catch(err => {
        throw err;
      });
  }

  getOrganisationReference() {
    if (this.currentOrganisation) {
      const orgId = this.currentOrganisation.id;
      return db.collection("/organisation").doc(orgId);
    }
    return null;
  }

  @action
  uploadAvatar(image: any) {
    if (this.user) {
      const orgId = this.user.organisation.id;
      this.isUploadingImage = true;
      return FirebaseUtils.uploadImage(
        image,
        `organisation/${orgId}/avatar`
      ).then(url =>
        FirebaseUtils.updateDocument(orgPath, this.user.organisation.id, {
          avatar: url
        }).then(() => {
          this.isUploadingImage = false;
        })
      );
    }
    return Promise.reject();
  }

  @action
  retrieveUserByEmail(email: string): void {
    const collectionRef = db.collection(userPath);
    collectionRef.where("email", "==", email).onSnapshot(
      {
        includeMetadataChanges: true
      },
      data => {
        const isAdded = data.docChanges().some(x => x.type === "added");
        if (data.docs[0] && isAdded) {
          const userData = data.docs[0].data();

          // Retrieve document reference data
          runInAction(() => {
            if (!this.user) {
              this.user = {};
            }
            this.pendingReferences = Object.keys(userData).length;
            Object.keys(userData).forEach(key => {
              if (key === "organisation" || key === "role") {
                const docReferenceId = userData[key].id;
                db.doc(userData[key].path).onSnapshot(doc => {
                  if (doc.exists && this.user) {
                    const newData = doc.data();
                    newData.id = docReferenceId;
                    this.user[key] = newData;
                  }
                  this.reducePendingReference();
                });
              } else {
                this.user[key] = userData[key];
                this.reducePendingReference();
              }
            });
          });
        }
      }
    );
  }

  @action
  reducePendingReference() {
    if (this.pendingReferences > 0) {
      this.pendingReferences -= 1;
    }
  }

  @action
  getAllUsers() {
    this.all = [];
    db.collection(userPath).onSnapshot(collection => {
      this.all.length = 0; // reset the array
      collection.forEach(doc => {
        let { role } = doc.data();
        if (role) {
          role.get().then(resRole => {
            role = resRole.data();
            role.id = resRole.id;
            const org = doc.data().organisation;
            if (org) {
              org.get().then(resOrg => {
                const o: any = {
                  ...doc.data(),
                  ...{ role },
                  ...{ roleType: role.type },
                  ...{
                    fullName: `${doc.data().firstName} ${doc.data().lastName}`
                  },
                  ...{ id: doc.id },
                  ...{ orgName: resOrg.data() ? resOrg.data().orgName : "None" }
                };
                this.all.push(o);
              });
            }
          });
        }
      });
    });
  }

  @action
  delete(id: string) {
    db.collection(userPath)
      .doc(id)
      .delete();
  }

  @action
  archiveUser(id: string) {
    db.collection(userPath)
      .doc(id)
      .update({ enabled: false });
  }

  @action
  restoreUser(id: string) {
    db.collection(userPath)
      .doc(id)
      .update({ enabled: true });
  }

  @computed
  get users(): Array<any> {
    if (!this.all) {
      this.getAllUsers();
    }
    return this.all;
  }

  @computed
  get currentOrganisation() {
    if (this.user) {
      return this.user.organisation;
    }
    return undefined;
  }

  @action
  updateName(name: string) {
    if (this.currentOrganisation) {
      const { id } = this.currentOrganisation;
      const data = { orgName: name };
      FirebaseUtils.updateDocument(orgPath, id, data);
    }
    return Promise.reject();
  }

  @action
  updateAbn(abn: string) {
    if (this.currentOrganisation) {
      const { id } = this.currentOrganisation;
      const data = { abn };
      FirebaseUtils.updateDocument(orgPath, id, data);
    }
    return Promise.reject();
  }

  @action
  updatePhone(phone: string) {
    if (this.currentOrganisation) {
      const { id } = this.currentOrganisation;
      const data = { phone };
      FirebaseUtils.updateDocument(orgPath, id, data);
    }
    return Promise.reject();
  }

  @action
  updateFirstName(firstName: string) {
    if (this.user) {
      const { id } = this.user;
      const data = { firstName };
      FirebaseUtils.updateDocument(orgPath, id, data);
    }
    return Promise.reject();
  }

  @action
  updateLastName(lastName: string) {
    if (this.user) {
      const { id } = this.user;
      const data = { lastName };
      FirebaseUtils.updateDocument(orgPath, id, data);
    }
    return Promise.reject();
  }

  @action
  updateEmail(email: string) {
    if (this.user) {
      const { id } = this.user;
      const data = { email };
      FirebaseUtils.updateDocument(orgPath, id, data);
    }
    return Promise.reject();
  }

  @computed
  get isUserOnboarded(): boolean {
    return false;
  }

  @computed
  get orgLogo() {
    if (this.user && this.user.organisation) {
      const { avatar } = this.user.organisation;
      if (!avatar || avatar === "") {
        return null;
        // avatar = ArcasLogo;
      }
      return avatar;
    }
    return null;
  }

  @computed
  get role() {
    if (this.user && this.user.role) {
      return this.user.role.id;
    }
    return undefined;
  }

  isAuthorised(allowed: Array<string>, def: boolean): boolean {
    if (allowed && this.user && this.user.role) {
      return allowed.includes(this.user.role.id);
    }
    return def;
  }

  isOrganisation(orgName: string) {
    const organisation = this.currentOrganisation;
    if (organisation) {
      return organisation.orgName === orgName;
    }
    return false;
  }
}

export default new UserStore();
