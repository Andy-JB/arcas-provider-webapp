// @flow
import { observable, action, computed } from "mobx";
import { db } from "../firebase";

const REF_DATA_COLLECTION = "app-reference-data";
const RefTypes = {
  Genders: "gender-groups",
  ServiceCapacity: "service-capacity-types",
  AgeGroups: "age-groups",
  ServiceCategories: "service-category-types"
};

class RefStore {
  @observable
  ageRefs: Array<any> = [];

  @observable
  genderRefs: Array<any> = [];

  @observable
  capacityRefs: Array<any> = [];

  @observable
  serviceCategories: Array<any> = [];

  @observable
  isRefDataLoaded = false;

  @action
  fetchAppReferenceData(): void {
    return db
      .collection(REF_DATA_COLLECTION)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          const refData = doc.data();
          const ageGroup = refData[RefTypes.AgeGroups];
          this.setValueInStore(ageGroup, RefTypes.AgeGroups);
          const genderGroup = refData[RefTypes.Genders];
          this.setValueInStore(genderGroup, RefTypes.Genders);
          const capacityGroup = refData[RefTypes.ServiceCapacity];
          this.setValueInStore(capacityGroup, RefTypes.ServiceCapacity);
          const categoryGroup = refData[RefTypes.ServiceCategories];
          this.setValueInStore(categoryGroup, RefTypes.ServiceCategories);
        });
      })
      .catch(error => {
        throw error;
      });
  }

  @action
  setValueInStore(data: Object, refType: any) {
    Object.keys(data).forEach(prop => {
      switch (refType) {
        case RefTypes.Genders:
          this.genderRefs.push({
            id: prop,
            name: data[prop]
          });
          break;
        case RefTypes.AgeGroups:
          this.ageRefs.push({
            id: prop,
            name: data[prop]
          });
          break;
        case RefTypes.ServiceCapacity:
          this.capacityRefs.push({
            id: prop,
            name: data[prop]
          });
          break;
        case RefTypes.ServiceCategories:
          this.serviceCategories.push({
            id: prop,
            name: data[prop]
          });
          break;
        default:
          break;
      }
    });
  }

  @action
  fetchRefData(): void {
    this.isRefDataLoaded = false;
    Promise.all([this.fetchAppReferenceData()])
      .then(() => {
        this.isRefDataLoaded = true;
      })
      .catch(() => {
        this.isRefDataLoaded = false;
      });
  }

  @action
  getServiceCategoryName(categoryId: string): string {
    if (this.serviceCategories != null && this.serviceCategories.length > 0) {
      const category = this.serviceCategories.find(
        service => service.id === categoryId
      );
      if (category) {
        return category.name;
      }
    }
    return "";
  }

  @action
  getAgeRestrictionName(ageRestrictionId: string): string {
    if (this.ageRefs != null && this.ageRefs.length > 0) {
      const capacityRef = this.ageRefs.find(age => age.id === ageRestrictionId);
      if (capacityRef) {
        return capacityRef.name;
      }
    }
    return "";
  }

  @action
  getGenderRestrictionName(genderRestrictionId: string): string {
    if (this.genderRefs != null && this.genderRefs.length > 0) {
      const gender = this.genderRefs.find(g => g.id === genderRestrictionId);
      if (gender) {
        return gender.name;
      }
    }
    return "";
  }

  getGenderRestrictionsForService(service: Object) {
    const genderRefs = service.gender;
    const genderRestrictions = [];
    if (service) {
      genderRefs.forEach(gender => {
        genderRestrictions.push(this.getGenderRestrictionName(gender));
      });
    }
    return genderRestrictions;
  }

  getAgeRestrictionsForService(service: Object) {
    const agesRestrictions = [];
    if (service) {
      const agesRefs = service.age;
      agesRefs.forEach(age => {
        agesRestrictions.push(this.getAgeRestrictionName(age));
      });
    }
    return agesRestrictions;
  }

  @computed
  get defaultCapacityId() {
    const capacityRef = this.capacityRefs.find(
      capacity => capacity.id === "NA"
    );
    if (capacityRef) {
      return capacityRef.id;
    }
    return "";
  }

  getCapacityName(capacityId: string) {
    if (this.capacityRefs != null && this.capacityRefs.length > 0) {
      const capacityRef = this.capacityRefs.find(
        capacity => capacity.id === capacityId
      );
      if (capacityRef) {
        return capacityRef.name;
      }
    }
    return "";
  }

  getCapacityId(capacityName: string) {
    if (this.capacityRefs != null && this.capacityRefs.length > 0) {
      const capacity = this.capacityRefs.find(x => x.name === capacityName);
      if (capacity) {
        return capacity.id;
      }
    }
    return "";
  }
}

export default new RefStore();
