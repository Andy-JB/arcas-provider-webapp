// @flow
import { action, computed, observable } from "mobx";
import postcodes from "common/data/postcodes";
import { db } from "../firebase";

const locationPath = "sa_postcodes";

class PostcodeStore {
  @observable
  locationOptions: ?Array<any> = undefined;

  @observable
  suburbOptions: ?Array<any> = undefined;

  @observable
  filtering: boolean = false;

  @action
  getLocation(suburb: string, state: string) {
    this.filtering = true;
    return db
      .collection(locationPath)
      .where("state_abbr", "==", state)
      .where("suburb", "==", suburb)
      .get()
      .then(querySnapshot => {
        let skip = false;
        let d;
        querySnapshot.forEach(doc => {
          if (doc && !skip) {
            d = doc.data();
            skip = true;
          }
        });
        this.filtering = false;
        return d;
      })
      .catch(() => undefined);
  }

  @computed
  get suburbOnlyOptions() {
    if (this.suburbOptions) {
      return this.suburbOptions;
    }
    this.suburbOptions = [];
    postcodes.map(data => {
      if (
        data.state_abbr === "SA" &&
        parseInt(data.postcode, 10) &&
        parseInt(data.postcode, 10)
          .toString()
          .startsWith("5")
      ) {
        if (this.suburbOptions) {
          this.suburbOptions.push({
            value: data.suburb,
            label: data.suburb
          });
          return this.suburbOptions;
        }
      }
      return undefined;
    });
    return this.suburbOptions;
  }

  @computed
  get suburbPostcodeOptions() {
    if (this.locationOptions) {
      return this.locationOptions;
    }
    this.locationOptions = [];
    postcodes.map(data => {
      if (
        data.state_abbr === "SA" &&
        parseInt(data.postcode, 10) &&
        parseInt(data.postcode, 10)
          .toString()
          .startsWith("5")
      ) {
        if (this.locationOptions) {
          this.locationOptions.push({
            value: data.suburb,
            label: `${data.suburb} (${data.postcode})`
          });
          return this.locationOptions;
        }
      }
      return undefined;
    });
    return this.locationOptions;
  }
}

export default new PostcodeStore();
