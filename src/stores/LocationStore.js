// @flow
import { observable, action, computed } from "mobx";
import FirebaseUtils from "util/FirebaseUtils";
import { GeoFirestore } from "geofirestore";
import firebase from "firebase";
import { db, functions } from "../firebase";
import RefStore from "./RefStore";

const announcementPath = "provider-announcements";
const servicesPath = "provider-services";
const locationPath = "provider-location";
const archivedLocationPath = "archived-provider-locations";

class LocationStore {
  @observable
  location: ?Object;

  @observable
  services: Array<Object>;

  @observable
  feedback: Array<any>;

  @observable
  isUploadingImage: boolean;

  @observable
  isLocationRetrieved: boolean;

  @observable
  isGettingServices: boolean;

  @observable
  isUpdatingData: boolean;

  @observable
  locationSnapshot: any;

  @observable
  servicesSnapshot: any;

  constructor() {
    this.resetStore();
  }

  resetStore() {
    this.location = null;
    this.isGettingServices = false;
    this.isUpdatingData = false;
    this.isUploadingImage = false;
    this.isLocationRetrieved = false;
    this.announcements = [];
    this.feedback = [];
    this.services = [];
    if (this.locationSnapshot != null) {
      this.locationSnapshot();
    }
    if (this.servicesSnapshot != null) {
      this.servicesSnapshot();
    }
  }

  @action
  createOrUpdate(organisation: Object, location: Object): void {
    const orgDocumentRef = FirebaseUtils.getDocReference(
      `organisation/${organisation.id}`
    );
    const locationObj = location;
    locationObj.organisation = orgDocumentRef;
    locationObj.orgName = organisation.orgName;
    locationObj.orgAvatar = organisation.avatar ? organisation.avatar : "";
    return this.createGeoLocation(
      locationPath,
      locationObj.latLng.lng,
      locationObj.latLng.lat,
      locationObj
    );
  }

  @action
  createGeoLocation(
    collectionName: string,
    longitude: number,
    latitude: number,
    data: any
  ) {
    const locationRef = db.collection(collectionName);
    const geoFirestore = new GeoFirestore(locationRef);
    const ref = db.collection(collectionName).doc();
    return geoFirestore.set(ref.id, {
      coordinates: new firebase.firestore.GeoPoint(latitude, longitude),
      ...data
    });
  }

  @action
  retrieveLocation(orgId: string): void {
    const orgDocumentRef = db.collection("organisation").doc(orgId);
    this.isLocationRetrieved = false;
    this.locationSnapshot = db
      .collection(locationPath)
      .where("d.organisation", "==", orgDocumentRef)
      .onSnapshot(querySnapshot => {
        querySnapshot.docChanges().forEach(change => {
          if (change.type !== "removed") {
            this.location = change.doc.data().d;
            this.location.id = change.doc.id;
            this.retrieveServices();
          }
        });
        this.isLocationRetrieved = true;
      });
  }

  @action
  uploadLocationImage(image: any, index: number) {
    if (this.location) {
      const locationId = this.location.id;
      this.isUploadingImage = true;
      return FirebaseUtils.uploadImage(
        image,
        `locations/${locationId}/${index}`
      ).then(url => {
        const imagePath = `d.images.${index}`;
        const data = { [`${imagePath}`]: url };
        FirebaseUtils.updateDocument(locationPath, locationId, data).then(
          () => {
            this.isUploadingImage = false;
          }
        );
      });
    }
    return Promise.reject();
  }

  @action
  async deleteLocation() {
    if (this.location) {
      const locationId = this.location.id;
      await FirebaseUtils.moveDocument(
        locationPath,
        archivedLocationPath,
        locationId
      );
      this.resetStore();
    }
  }

  @action
  deleteLocationImage(newImages: any) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.images": newImages };
      return FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updateTitle(newTitle: string) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.title": newTitle };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updatePhone(phone: string) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.phone": phone };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updateEmail(email: string) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.email": email };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updateWebsite(website: string) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.website": website };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updateDescription(newDescription: string) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.description": newDescription.trim() };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updateAddress(newLocation: Object) {
    if (this.location) {
      const locationId = this.location.id;
      const data = {
        "d.address": newLocation.address,
        "d.latLng": newLocation.latLng
      };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @action
  updateOpeningHours(newOpeningHours: Object) {
    if (this.location) {
      const locationId = this.location.id;
      const data = { "d.openingHours": newOpeningHours };
      FirebaseUtils.updateDocument(locationPath, locationId, data);
    }
    return Promise.reject();
  }

  @computed
  get currentLocation() {
    if (this.location) {
      return this.location;
    }
    return null;
  }

  @computed
  get address() {
    if (this.location) {
      return this.location.address;
    }
    return undefined;
  }

  @computed
  get concatenatedAddress() {
    if (this.location && this.location.address) {
      return this.location.address.formattedAddress;
    }
    return "";
  }

  @computed
  get openingHours() {
    if (this.location) {
      return this.location.openingHours;
    }
    return undefined;
  }

  // Services
  @action
  createOrUpdateService(service: Object): Promise<void> {
    if (this.currentLocation) {
      const locationId = this.currentLocation.id;
      const serviceObj = service;
      const { category, age, gender } = serviceObj;

      // save location, category, age, gender as reference
      const locationDocRef = FirebaseUtils.getDocReference(
        `${locationPath}/${locationId}`
      );

      serviceObj.category = category;
      serviceObj.capacity = RefStore.defaultCapacityId;
      serviceObj.isEnabled = true;
      serviceObj.age = age;
      serviceObj.gender = gender;

      serviceObj.location = locationDocRef;
      return FirebaseUtils.addDocument(servicesPath, serviceObj)
        .then(ref => ref)
        .catch(err => {
          throw err;
        });
    }
    return Promise.reject();
  }

  @action
  mapToDocumentReference(
    values: Array<any>,
    collectionPath: string
  ): Array<Object> {
    const docRefs = [];
    values.map(id =>
      docRefs.push(FirebaseUtils.getDocReference(`${collectionPath}/${id}`))
    );
    return docRefs;
  }

  @action
  retrieveServices(showProgress: boolean = false): void {
    if (showProgress) {
      this.isGettingServices = true;
    }
    if (this.currentLocation && this.currentLocation.id) {
      const locationId = this.currentLocation.id;
      const locDocumentRef = db.collection(locationPath).doc(locationId);
      this.servicesSnapshot = db
        .collection(servicesPath)
        .where("location", "==", locDocumentRef)
        .onSnapshot(querySnapshot => {
          this.services = [];
          querySnapshot.forEach(doc => {
            let service = {};
            service = doc.data();
            service.id = doc.id;
            this.services.push(service);
          });
          if (showProgress) {
            this.isGettingServices = false;
          }
        });
    }
  }

  @action
  updateServiceAge(serviceId: string, ageIds: Array<string>) {
    const data = { age: ageIds };
    return FirebaseUtils.updateDocument(servicesPath, serviceId, data);
  }

  @action
  updateServiceGender(serviceId: string, genderIds: Array<string>) {
    const data = { gender: genderIds };
    return FirebaseUtils.updateDocument(servicesPath, serviceId, data);
  }

  @action
  updateServiceCategory(serviceId: string, categoryId: string) {
    const data = { category: categoryId };
    return FirebaseUtils.updateDocument(servicesPath, serviceId, data);
  }

  @action
  updateServiceOpeningHours(serviceId: string, updatedOpeningHours: Object) {
    const data = { openingHours: updatedOpeningHours };
    return FirebaseUtils.updateDocument(servicesPath, serviceId, data);
  }

  @action
  updateServiceCapacity(serviceId: any, capacityId: any) {
    const data = { capacity: capacityId };
    return FirebaseUtils.updateDocument(servicesPath, serviceId, data);
  }

  @action
  updateServiceEnable(serviceId: any, enable: boolean) {
    const data = { isEnabled: enable };
    return FirebaseUtils.updateDocument(servicesPath, serviceId, data);
  }

  @action
  deleteService(serviceId: string) {
    db.collection(servicesPath)
      .doc(serviceId)
      .delete();
  }

  @action
  getServiceCategoryName(service: any): string {
    if (service) {
      return RefStore.getServiceCategoryName(service.category);
    }
    return "";
  }

  @action
  async removeAnnouncement(announcementId: string) {
    this.isUpdatingData = true;
    try {
      await FirebaseUtils.updateDocument(announcementPath, announcementId, {
        deleted: true
      });
      this.getAnnouncements();
      this.isUpdatingData = false;
    } catch (e) {
      this.isUpdatingData = false;
    }
  }

  @action
  async createOrUpdateAnnouncement(
    data: Object,
    existingKey: ?string,
    currentLocation: any
  ) {
    let ref;
    if (existingKey) {
      ref = db.collection(announcementPath).doc(existingKey);
    } else {
      ref = db.collection(announcementPath).doc();
    }
    this.isUpdatingData = true;
    const references = {
      location: db.collection(locationPath).doc(currentLocation.id),
      organisation: FirebaseUtils.getDocReference(
        `organisation/${data.organisation}`
      )
    };
    await ref.set({
      title: data.title,
      description: data.description,
      eventDate: new Date(data.eventDate),
      location: references.location,
      organisation: references.organisation,
      orgName: data.orgName,
      orgAvatar: data.orgAvatar,
      isEvent: data.isEvent,
      startTime: data.startTime,
      endTime: data.endTime,
      deleted: false,
      createdAt: new Date(),
      updatedAt: new Date()
    });
    this.isUpdatingData = false;
  }

  @action
  async getAnnouncement(docId: string): Promise<any> {
    const document = await FirebaseUtils.getDocument(announcementPath, docId);
    return document;
  }

  @action
  async getAnnouncements(quantity: number = 100) {
    if (this.currentLocation && this.currentLocation.id) {
      const locationId = this.currentLocation.id;
      const locDocumentRef = db.collection(locationPath).doc(locationId);
      const snapshot = await FirebaseUtils.createReferenceWhereWithLimitOrder(
        "provider-announcements",
        [
          {
            key: "deleted",
            comparator: "==",
            value: false
          },
          {
            key: "location",
            comparator: "==",
            value: locDocumentRef
          }
        ],
        quantity,
        "updatedAt"
      ).get();
      if (snapshot.docs) {
        return snapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        }));
      }
      return [];
    }
    return [];
  }

  @action
  getAllFeedback(): Promise<void> {
    if (this.currentLocation && this.currentLocation.id) {
      const locationId = this.currentLocation.id;
      const locDocumentRef = db.collection(locationPath).doc(locationId);
      return FirebaseUtils.createReferenceWhere("provider-feedback", [
        {
          key: "location",
          comparator: "==",
          value: locDocumentRef
        }
      ])
        .get()
        .then(snapshot => {
          const documents = [];
          snapshot.forEach(doc => {
            const data = { id: doc.id, ...doc.data() };
            // data.timestamp = moment(data.timestamp.seconds * 1000);
            documents.push(data);
          });
          this.feedback = documents;
          return documents;
        });
    }
    return Promise.reject();
  }

  @action
  getGooglePlaces(apiKey: string, input: string) {
    const googlePlaces = functions.httpsCallable("getGooglePlaces");
    return googlePlaces({ apiKey, input })
      .then(results => results.data.predictions)
      .catch(error => error);
  }

  @action
  getGooglePlaceDetails(apiKey: string, placeId: string) {
    const googlePlaces = functions.httpsCallable("getGooglePlaceDetails");
    return googlePlaces({ apiKey, placeId })
      .then(place => place.data.place)
      .catch(error => error);
  }
}

export default new LocationStore();
