// @flow

const styles = (theme: any) => ({
  formContainer: {
    paddingTop: 20,
    width: "80%",
    paddingBottom: 50,
    margin: "auto"
  },
  offerImage: {
    "list-style-type": "none"
  },
  offerImageSrc: {
    height: 250,
    width: "100%",
    objectFit: "cover"
  },
  imageUploadContainer: {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30
  },
  uploadHeading: {
    paddingBottom: 15
  },
  buttonContainer: {
    textAlign: "right",
    paddingTop: 15
  },
  submitButton: {
    width: 200
  },
  stepperContainer: {
    width: "100%",
    padding: 10
  },
  reviewHeading: {
    paddingTop: 30,
    paddingLeft: 20
  },
  stepperButtonContainer: {
    paddingBottom: 30,
    paddingRight: 20,
    paddingTop: 20,
    textAlign: "right"
  },
  reviewContainer: {
    paddingTop: 30,
    width: "100%"
  },
  reviewHeadingContainer: {
    paddingLeft: 20,
    paddingTop: 30
  },
  reviewAnswerContainer: {
    paddingTop: 20
  },
  reviewImage: {
    height: "auto",
    width: 100
  },
  titleImagesContainer: {
    paddingLeft: 20,
    width: "100%"
  },
  contactDetailsContainer: {
    paddingLeft: 20,
    paddingTop: 20,
    [theme.breakpoints.up("md")]: {
      paddingTop: 0,
      paddingLeft: 40
    }
  },
  dateContainer: {
    [theme.breakpoints.up("md")]: {
      paddingTop: 20
    }
  },
  dateInput: {
    [theme.breakpoints.down("sm")]: {
      paddingTop: 20
    }
  },
  deleteButton: {
    backgroundColor: "red",
    "&:hover": {
      backgroundColor: "red",
      opacity: 0.9
    }
  },
  titleBar: {
    background:
      "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
      "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
  },
  deleteIcon: {
    color: "white"
  },
  filterLocationContainer: {
    paddingTop: 15,
    paddingBottom: 15,
    zindex: 999
  }
}); // eslint-disable-line

export default styles;
