// @flow
import React, { Component } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { computed, reaction } from "mobx";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { createMuiTheme } from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";
import pink from "@material-ui/core/colors/pink";
import withStyles from "@material-ui/core/styles/withStyles";
import LoginScreen from "features/authentication/screens/LoginScreen";
import LoadingScreen from "common/screens/LoadingScreen";
import DashboardScreen from "features/dashboard";
import SignupScreen from "features/authentication/screens/SignupScreen";
import ForgotPasswordScreen from "features/authentication/screens/ForgotPasswordScreen";
import ResetPasswordScreen from "features/authentication/screens/ResetPasswordScreen";
import ManageUsersScreen from "features/admin/screens/ManageUsersScreen";
import MessageLogsScreen from "features/admin/screens/MessageLogsScreen";
import SettingsScreen from "features/settings/screens/SettingsScreen";
import PageNotFoundScreen from "common/screens/PageNotFoundScreen";
import AddLocationScreen from "features/location/screens/AddLocationScreen";
import LocationOverviewScreen from "features/location/screens/LocationOverviewScreen";
import CssBaseline from "@material-ui/core/CssBaseline";
import SupplierOffersTabScreen from "features/supplier-offers/screens/SupplierOffersTabScreen";
import ViewOfferScreen from "features/supplier-offers/screens/ViewOfferScreen";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faTshirt,
  faBuilding,
  faHotel,
  faProcedures,
  faUtensils,
  faMedkit,
  faSocks,
  faShower,
  faToiletPaper,
  faCar,
  faTint,
  faWifi,
  faHome,
  faChargingStation,
  faSmileWink,
  faFrown
} from "@fortawesome/free-solid-svg-icons";
import { auth } from "./firebase";
import PrivateRoute from "./PrivateRoute";
import history from "./appHistory";
import AddOfferScreen from "./features/supplier-offers/screens/AddOfferScreen";
import AddAnnouncementScreen from "./features/location/screens/AddAnnouncementScreen";
import AddServiceScreen from "./features/location/screens/AddServiceScreen";

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink
  },
  overrides: {
    MuiListItem: {}
  }
});

const styles = () => ({
  // eslint-disable-line
  root: {
    minHeight: "calc(100vh - 130px)"
  }
});

type Props = {
  AuthStore: any,
  UserStore: any,
  LocationStore: any,
  OrgStore: any,
  RefStore: any,
  UiEventStore: any,
  classes: any
};

@inject(
  "AuthStore",
  "UserStore",
  "LocationStore",
  "ActivityLogsStore",
  "RefStore",
  "OrgStore",
  "UiEventStore"
)
@observer
class App extends Component<Props> {
  render() {
    const { classes, UiEventStore, AuthStore } = this.props;
    if (this.requiresData) {
      return <LoadingScreen />;
    }
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Router history={history}>
          <div className={classes.root}>
            <Switch>
              <Redirect exact from="/" to="/login" />
              {this.privateRoutes().map(x => (
                <PrivateRoute key={x.path} exact {...x} />
              ))}
              {this.publicRoutes().map(x => (
                <Route key={x.path} exact {...x} />
              ))}
              <Route path="/404" component={PageNotFoundScreen} />
              <Redirect
                from="*"
                to={AuthStore.isAuthenticated ? "/404" : "/login"}
              />
            </Switch>
            {UiEventStore.snackbar.open && this.renderSnackbar()}
            {UiEventStore.lightbox.isOpen && this.renderLightbox()}
            {UiEventStore.alertDialog.open && this.renderAlertDialog()}
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }

  componentWillMount() {
    this.registerOnAuthStateChanged();
    this.props.RefStore.fetchRefData();
  }

  componentDidMount() {
    this.addIconLibraries();
  }

  registerOnAuthStateChanged = () => {
    this.setupReactions();
    auth.onAuthStateChanged(user => {
      if (user) {
        this.props.AuthStore.userAuthData = user;
        this.props.UserStore.retrieveUserByEmail(user.email);
      } else {
        this.props.AuthStore.resetStore();
        this.props.UserStore.resetStore();
        this.props.LocationStore.resetStore();
      }
      this.props.AuthStore.isInitialLoad = false;
    });
  };

  isLoggedIn = () => {
    return (
      this.props.AuthStore.isAuthenticated &&
      this.props.UserStore.user &&
      this.props.LocationStore.isLocationRetrieved
    );
  };

  setupReactions = () => {
    // reaction(
    //   () => this.isLoggedIn(),
    //   () => {
    //     // history.push("/dashboard");
    //   }
    // );
    // reaction(
    //   () =>
    //     this.props.AuthStore.isAuthenticated &&
    //     this.props.UserStore.user &&
    //     this.props.LocationStore.isLocationRetrieved,
    //   () => {
    //     // history.push("/dashboard");
    //   }
    // );
    reaction(
      () =>
        this.props.UserStore.user &&
        this.props.UserStore.currentOrganisation != null,
      () => {
        const organisation = this.props.UserStore.currentOrganisation;
        if (organisation) {
          this.props.LocationStore.retrieveLocation(organisation.id);
        }
      }
    );
  };

  addIconLibraries() {
    library.add(
      faTshirt,
      faBuilding,
      faHotel,
      faProcedures,
      faUtensils,
      faMedkit,
      faSocks,
      faShower,
      faToiletPaper,
      faCar,
      faTint,
      faWifi,
      faHome,
      faChargingStation,
      faSmileWink,
      faFrown
    );
  }

  renderSnackbar = () => {
    const { UiEventStore } = this.props;
    const { snackbar } = UiEventStore;
    return snackbar.uiComponent();
  };

  renderLightbox = () => {
    const { UiEventStore } = this.props;
    return UiEventStore.lightbox.lightboxComponent;
  };

  renderAlertDialog = () => {
    const { UiEventStore } = this.props;
    return UiEventStore.alertDialog.uiComponent();
  };

  privateRoutes() {
    const { isAuthenticated } = this.props.AuthStore;
    return [
      {
        path: "/locations/addlocation",
        component: AddLocationScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/locations/addservice",
        component: AddServiceScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/locations/announcements/add",
        component: AddAnnouncementScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/locations/announcements/edit/:id",
        component: AddAnnouncementScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/locations/overview",
        component: LocationOverviewScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/settings",
        component: SettingsScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/dashboard",
        component: DashboardScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/supplieroffers",
        component: SupplierOffersTabScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/supplieroffers/add",
        component: AddOfferScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/supplieroffers/edit/:id",
        component: AddOfferScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/supplieroffers/view/:id",
        component: ViewOfferScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/admin/users",
        component: ManageUsersScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/admin/auditactivity",
        component: MessageLogsScreen,
        accessRule: isAuthenticated
      },
      {
        path: "/login",
        component: LoginScreen,
        accessRule: isAuthenticated === false,
        redirectUrl: "/dashboard"
      },
      {
        path: "/signup",
        component: SignupScreen,
        accessRule: isAuthenticated === false,
        redirectUrl: "/dashboard"
      }
    ];
  }

  publicRoutes() {
    return [
      {
        path: "/forgotpassword",
        component: ForgotPasswordScreen
      },
      {
        path: "/resetpassword",
        component: ResetPasswordScreen
      }
    ];
  }

  @computed
  get requiresData() {
    const { UserStore, AuthStore, RefStore, LocationStore } = this.props;
    const { isInitialLoad, isAuthenticated } = AuthStore;
    if (isInitialLoad) {
      return true;
    }
    if (isAuthenticated) {
      return (
        !UserStore.currentOrganisation ||
        UserStore.pendingReferences > 0 ||
        !RefStore.isRefDataLoaded ||
        !LocationStore.isLocationRetrieved
      );
    }
    return false;
  }
}

export default withStyles(styles)(App);
