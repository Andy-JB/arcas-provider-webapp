// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import Grid from "@material-ui/core/Grid";
import SideBar from "common/components/SideBar";
import NavigationBar from "common/components/NavigationBar";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import List from "@material-ui/core/List";
import ListAlt from "@material-ui/icons/ListAltOutlined";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import LocalOffer from "@material-ui/icons/LocalOfferOutlined";
import history from "appHistory";
import Button from "@material-ui/core/Button";
import {
  FadeInUpAnimation,
  FadeInAnimation,
  AnimatedView
} from "common/animations";
import LocationIcon from "@material-ui/icons/LocationOn";
import HelpIcon from "@material-ui/icons/Help";
import MaterialTooltip from "@material-ui/core/Tooltip";
import ServiceDashboardCard from "common/cards/ServiceDashboardCard";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any
};

@observer
class ProviderDashboardView extends Component<Props> {
  @observable
  isOpen: boolean = false;

  @observable
  mobileOpen: boolean;

  constructor(props: Object) {
    super(props);
    setTimeout(this.startAnimating, 0);
  }

  startAnimating = () => {
    this.isOpen = !this.isOpen;
  };

  @action
  handleDrawerToggle = () => {
    this.mobileOpen = !this.mobileOpen;
  };

  renderServiceCards = () => {
    const { controller, classes } = this.props;
    const { isServicesLoading, openServicesObjects } = controller;
    if (isServicesLoading) {
      return (
        <div className={classes.servicesProgress}>
          <CircularProgress />
        </div>
      );
    }
    if (openServicesObjects && openServicesObjects.length === 0) {
      return (
        <div className={classes.servicesProgress}>
          <Typography variant="body1">
            No active services at the moment.
          </Typography>
        </div>
      );
    }

    return openServicesObjects.map(x => (
      <ServiceDashboardCard
        key={`${x.category} - ${x.id}`}
        service={x}
        onCapacityChange={controller.onCapacityChange}
        toggleServiceEnabled={controller.toggleServiceEnabled}
      />
    ));
  };

  render() {
    const { classes, controller } = this.props;
    const {
      handleViewSupplierOffers,
      handleViewAnnouncementBoard,
      locationName
    } = controller;
    return (
      <AnimatedView
        pose={this.isOpen ? "open" : "closed"}
        className={classes.root}
      >
        <NavigationBar onNavbarPress={this.handleDrawerToggle} />
        <SideBar
          mobileOpen={this.mobileOpen}
          onClose={this.handleDrawerToggle}
        />
        <Grid container className={classes.appContent}>
          <Grid container className={classes.pageDescriptionContainer}>
            <Grid item xs={12} className={classes.pageTitleContainer}>
              <FadeInAnimation>
                <LocationIcon className={classes.pageIcon} />
              </FadeInAnimation>
              <Typography
                className={classes.pageTitle}
                variant="h5"
                component="h1"
              >
                <FadeInAnimation>Dashboard - {locationName}</FadeInAnimation>
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={12} sm={12} md={12}>
              <FadeInUpAnimation>
                <Typography
                  variant="h6"
                  component="h2"
                  className={classes.servicesHeading}
                >
                  Manage Service Capacity
                  <MaterialTooltip
                    title="Services become available when it is within opening hours for a particular service."
                    placement="right-start"
                  >
                    <HelpIcon />
                  </MaterialTooltip>
                </Typography>
                <Typography variant="body1" className={classes.serviceDesc}>
                  Selections will be shown the mobile app users and reset every
                  night.
                </Typography>
              </FadeInUpAnimation>
            </Grid>
            <Grid container className={classes.serviceCardContainer}>
              {this.renderServiceCards()}
            </Grid>

            <Grid container className={classes.supplierAnnouncementContainer}>
              <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                <FadeInUpAnimation className={classes.offerCardContainer}>
                  <Typography
                    className={classes.locationHeading}
                    variant="h6"
                    component="h2"
                  >
                    My Current Offers
                  </Typography>
                  <Paper className={classes.statPaperContainer}>
                    <div className={classes.statDataContainer}>
                      {this.getSupplierOffersContent()}
                      <div className={classes.buttonContainer}>
                        <Button
                          onClick={handleViewSupplierOffers}
                          color="primary"
                        >
                          View My Offers
                        </Button>
                      </div>
                    </div>
                  </Paper>
                </FadeInUpAnimation>
              </Grid>

              <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                <FadeInUpAnimation className={classes.offerCardContainer}>
                  <Typography
                    className={classes.locationHeading}
                    variant="h6"
                    component="h2"
                  >
                    My Recent Announcements
                  </Typography>
                  <Paper className={classes.statPaperContainer}>
                    <div className={classes.statDataContainer}>
                      {this.getAnnouncementsContent()}
                      <div className={classes.buttonContainer}>
                        <Button
                          onClick={handleViewAnnouncementBoard}
                          color="primary"
                        >
                          View Announcement Board
                        </Button>
                      </div>
                    </div>
                  </Paper>
                </FadeInUpAnimation>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </AnimatedView>
    );
  }

  getSupplierOffersContent = () => {
    const { classes, controller } = this.props;
    const { isSupplierOffersLoaded, supplierObjects } = controller;
    if (!isSupplierOffersLoaded) {
      return (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      );
    }
    if (supplierObjects.length === 0) {
      return (
        <Typography variant="body1">You have no current offers</Typography>
      );
    }
    return (
      <List>
        {supplierObjects.map(offer => (
          <ListItem
            button
            key={offer.id}
            onClick={() =>
              this.navigateToUrl(`/supplieroffers/view/${offer.id}`)
            }
          >
            <LocalOffer />
            <ListItemText
              primary={offer.title}
              secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    className={classes.listItemInline}
                    color="textPrimary"
                  >
                    Ending at - {offer.endDate}
                  </Typography>
                </React.Fragment>
              }
            />
          </ListItem>
        ))}
      </List>
    );
  };

  @action
  navigateToUrl = (url: string) => {
    history.push(url);
  };

  getAnnouncementsContent = () => {
    const { classes, controller } = this.props;
    const { isAnnouncementsLoading, announcements } = controller;
    if (isAnnouncementsLoading) {
      return (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      );
    }
    if (announcements.length === 0) {
      return (
        <Typography variant="body1">
          You have no recent announcements
        </Typography>
      );
    }
    return (
      <List>
        {announcements.map(announcement => (
          <ListItem
            button
            key={announcement.id}
            onClick={() => {
              this.navigateToUrl(
                `/locations/announcements/edit/${announcement.id}`
              );
            }}
          >
            <ListAlt />
            <ListItemText
              primary={announcement.title}
              secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    className={classes.listItemInline}
                    color="textPrimary"
                  >
                    Event on - {announcement.eventDate}
                  </Typography>
                </React.Fragment>
              }
            />
          </ListItem>
        ))}
      </List>
    );
  };
}

export default withStyles(styles)(ProviderDashboardView);
