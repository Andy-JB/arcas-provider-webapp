// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { action, computed, observable } from "mobx";
import moment from "moment";
import history from "appHistory";
import ProviderDashboardView from "./ProviderDashboardView";
import DateUtils from "../../../util/DateUtils";

type Props = {
  UserStore: any,
  SupplierOfferStore: any,
  LocationStore: any,
  RefStore: any
};

const OFFER_COUNT = 3;

@inject(
  "AuthStore",
  "UserStore",
  "SupplierOfferStore",
  "LocationStore",
  "RefStore"
)
@observer
class ProviderDashboardScreen extends Component<Props> {
  @observable timer: any = null;

  @observable
  announcements: Array<any> = null;

  componentDidMount() {
    const { SupplierOfferStore, UserStore, LocationStore } = this.props;
    SupplierOfferStore.getSubsetMyOffers(
      UserStore.currentOrganisation.orgName,
      OFFER_COUNT
    );
    this.retrieveAnnouncements();
    LocationStore.retrieveServices(true);
    this.timer = setInterval(() => {
      LocationStore.retrieveServices(false);
    }, 30000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    if (this.props.UserStore) {
      return <ProviderDashboardView controller={this} />;
    }
    return <div />;
  }

  retrieveAnnouncements = () => {
    const { LocationStore } = this.props;
    LocationStore.getAnnouncements(OFFER_COUNT).then(announcements => {
      this.announcements = announcements.map(x => {
        return {
          id: x.id,
          title: x.title,
          description: x.description,
          eventDate: moment(x.eventDate.toDate()).format("DD MMMM YYYY")
        };
      });
    });
  };

  @computed
  get locationName() {
    return this.props.LocationStore.location.title;
  }

  @computed
  get isSupplierOffersLoaded(): boolean {
    const { SupplierOfferStore } = this.props;
    return SupplierOfferStore.myData != null;
  }

  @computed
  get isAnnouncementsLoading(): boolean {
    return this.announcements == null;
  }

  @computed
  get isServicesLoading(): boolean {
    const { LocationStore } = this.props;
    return LocationStore.isGettingServices;
  }

  @computed
  get openServicesObjects(): Array<Object> {
    const { LocationStore, RefStore } = this.props;
    if (LocationStore.services) {
      return LocationStore.services
        .filter(service => this.isServiceCurrentlyOpen(service))
        .map(service => {
          const sessionEndTime = this.getCurrentSessionEndTime(service);
          return {
            id: service.id,
            category: LocationStore.getServiceCategoryName(service),
            isEnabled: service.isEnabled,
            endTime: DateUtils.getFormattedTime(sessionEndTime),
            capacityLevel: RefStore.getCapacityName(service.capacity)
          };
        })
        .sort((a, b) => (a.category > b.category ? 1 : -1));
    }
    return [];
  }

  getCurrentSessionEndTime = (service: any): string => {
    const currentSession = this.getCurrentSession(service);
    if (currentSession) {
      return currentSession.endTime;
    }
    return "";
  };

  isServiceCurrentlyOpen = (service: any): boolean =>
    this.getCurrentSession(service) != null;

  getCurrentSession = (service: any): ?Object => {
    const { currentDay, currentTime, isTimeInBetween } = DateUtils;
    const todayOpeningHours = service.openingHours[currentDay];
    if (todayOpeningHours.isOpen) {
      const openingTimes = todayOpeningHours.times;
      const currentSession = openingTimes.filter(time =>
        isTimeInBetween(time.startTime, time.endTime, currentTime)
      );
      if (currentSession != null && currentSession.length > 0) {
        return currentSession[0];
      }
    }
    return null;
  };

  @computed
  get supplierObjects(): Array<Object> {
    const { SupplierOfferStore } = this.props;
    if (SupplierOfferStore.myData) {
      return SupplierOfferStore.myData.map(x => {
        let image;
        if (x.images && x.images.length > 0) {
          image = x.images[0]; // eslint-disable-line
        }
        return {
          id: x.id,
          title: x.title,
          description: x.description,
          location: x.location,
          avatar: x.orgAvatar,
          subtitle: x.orgName,
          image,
          endDate: moment(x.endDate.toDate()).format("DD MMMM YYYY")
        };
      });
    }
    return [];
  }

  @action
  handleViewSupplierOffers = () => {
    history.push("/supplieroffers");
  };

  @action
  handleViewAnnouncementBoard = () => {
    history.push("/locations/overview#announcements");
  };

  @action
  toggleServiceEnabled = (serviceId: any, isEnabled: boolean) => {
    const { LocationStore } = this.props;
    LocationStore.updateServiceEnable(serviceId, isEnabled);
  };

  @action
  onCapacityChange = (serviceId: any, label: string) => {
    const { RefStore, LocationStore } = this.props;
    const capacityId = RefStore.getCapacityId(label);
    LocationStore.updateServiceCapacity(serviceId, capacityId);
  };
}

export default ProviderDashboardScreen;
