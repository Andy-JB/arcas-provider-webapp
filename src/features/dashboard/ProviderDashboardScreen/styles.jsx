// @flow
const drawerWidth = 250;

const styles = (theme: any) => ({
  root: {
    flexGrow: 1,
    zindex: 1,
    overflow: "hidden",
    position: "relative",
    display: "flex",
    width: "100%",
    marginBottom: 100
  },
  pageIcon: {
    marginTop: 45,
    marginLeft: 20,
    height: 45,
    width: 45,
    color: "#fff",
    display: "block"
  },
  pageDescriptionContainer: {
    marginTop: 60,
    marginBottom: 20,
    height: 130,
    backgroundColor: "#2d323e",
    display: "block"
  },
  pageTitleContainer: {
    display: "flex",
    marginBottom: 0
  },
  pageTitle: {
    paddingTop: 55,
    paddingLeft: 15,
    color: "#fff"
  },
  pageSubtitle: {
    color: "#fff",
    paddingLeft: 80
  },
  topChartContent: {
    flexGrow: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(4),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginBottom: 30,
    marginTop: -300,
    maxHeight: 400
  },
  chartContainer: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    minHeight: 200,
    padding: 10
  },
  locationContent: {
    flexGrow: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(4),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginBottom: 30,
    marginTop: 10,
    maxHeight: 400
  },
  pagePaperContainer: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    minHeight: 300,
    padding: 10
  },
  offerCardContainer: {
    [theme.breakpoints.up("sm")]: {
      height: "100%",
      maxHeight: 320
    }
  },
  statPaperContainer: {
    height: "100%",
    padding: 10,
    margin: 10,
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    }
  },
  statDataContainer: {
    padding: 10
  },
  statContentContainer: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginTop: 0
  },
  supplierAnnouncementContainer: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    }
  },
  appContent: {
    [theme.breakpoints.up("md")]: {
      paddingLeft: drawerWidth
    },
    paddingBottom: 60
  },
  textSameLine: {
    display: "flex"
  },
  whiteText: {
    color: "#222",
    paddingBottom: 10
  },
  locationHeading: {
    color: "#222",
    paddingLeft: 10
  },
  servicesHeading: {
    color: "#222",
    paddingLeft: 30
  },
  whiteHeading: {
    color: "#fff",
    paddingBottom: 10
  },
  statCaption: {
    paddingTop: 40
  },
  buttonContainer: {
    textAlign: "right"
  },
  dashboardCount: {
    color: "black"
  },
  barchart: {
    width: 200,
    height: 60
  },
  serviceCard: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginTop: 10
  },
  serviceSwitch: {
    padding: 0,
    margin: 0
  },
  allServiceCard: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 10,
      marginRight: 10
    },
    marginTop: 10,
    backgroundColor: "#039be5"
  },
  serviceCardText: {},
  serviceCardContainer: {
    marginBottom: 20,
    padding: 10
  },
  slider: {},
  serviceCardHeading: {
    paddingTop: 10,
    paddingLeft: 20
  },
  trafficContainer: {
    padding: 20
  },
  sliderContainer: {
    paddingTop: 20
  },
  serviceDesc: {
    paddingLeft: 30,
    paddingRight: 30
  },
  // tabContainer: {
  //   flexGrow: 1,
  //   borderTopLeftRadius: 8,
  //   borderTopRightRadius: 8,
  //   borderBottomLeftRadius: 8,
  //   borderBottomRightRadius: 8,
  //   backgroundColor: theme.palette.background.paper,
  //   height: 400,
  //   marginLeft: theme.spacing(4),
  //   marginRight: theme.spacing(4),
  //   [theme.breakpoints.down("sm")]: {
  //     marginLeft: 0,
  //     marginRight: 0
  //   }
  // },
  tabs: {
    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(4)
  },
  tab: {
    height: 59
  },
  progress: {
    display: "flex",
    justifyContent: "center",
    margin: theme.spacing(2)
  },
  servicesProgress: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  listItemInline: {
    display: "inline"
  }
});

export default styles;
