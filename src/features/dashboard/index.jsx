// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import ProviderDashboardScreen from "./ProviderDashboardScreen";
import NoLocationDashboardScreen from "./NoLocationDashboardScreen";

type Props = {
  UserStore: any,
  SupplierOfferStore: any,
  LocationStore: any,
  RefStore: any
};

@inject(
  "AuthStore",
  "UserStore",
  "SupplierOfferStore",
  "LocationStore",
  "RefStore"
)
@observer
class DashboardScreen extends Component<Props> {
  render() {
    const { LocationStore } = this.props;
    if (this.props.UserStore && LocationStore.currentLocation) {
      return <ProviderDashboardScreen {...this.props} />;
    }
    return <NoLocationDashboardScreen {...this.props} />;
  }
}

export default DashboardScreen;
