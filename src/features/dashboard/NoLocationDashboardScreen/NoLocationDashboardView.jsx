// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import AddLocation from "@material-ui/icons/AddLocation";
import DashboardIcon from "@material-ui/icons/DashboardOutlined";
import CircularProgress from "@material-ui/core/CircularProgress";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import NoImageIcon from "media/images/add-location.png";
import DashboardLayout from "common/layouts/DashboardLayout";
import { FadeInUpAnimation, AnimatedView } from "common/animations";
import history from "appHistory";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any
};

@observer
class NoLocationDashboardView extends Component<Props> {
  steps = [
    {
      target: ".currentoffers",
      content: "This is my awesome feature!"
    }
  ];

  @observable
  isOpen: boolean = false;

  @observable
  mobileOpen: boolean;

  constructor(props: Object) {
    super(props);
    setTimeout(this.startAnimating, 0);
  }

  startAnimating = () => {
    this.isOpen = !this.isOpen;
  };

  @action
  handleDrawerToggle = () => {
    this.mobileOpen = !this.mobileOpen;
  };

  @action
  navigateToUrl = (url: string) => {
    history.push(url);
  };

  render() {
    const { classes } = this.props;

    return (
      <AnimatedView
        pose={this.isOpen ? "open" : "closed"}
        className={classes.root}
      >
        <DashboardLayout pageTitle="Dashboard" headerIcon={DashboardIcon}>
          <Grid container>
            <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
              <FadeInUpAnimation className={classes.offerCardContainer}>
                <Typography
                  className={classes.locationHeading}
                  variant="h6"
                  component="h2"
                >
                  My Current Offers
                </Typography>
                <Paper className={classes.statPaperContainer} square>
                  <div className={classes.statDataContainer}>
                    {this.getSupplierOffersContent()}
                    <div className={classes.buttonContainer}>
                      <Button
                        onClick={() => this.navigateToUrl("/supplieroffers")}
                        color="primary"
                      >
                        View My Offers
                      </Button>
                    </div>
                  </div>
                </Paper>
              </FadeInUpAnimation>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
              <FadeInUpAnimation className={classes.offerCardContainer}>
                <Typography
                  className={classes.locationHeading}
                  variant="h6"
                  component="h2"
                >
                  All Public Offers
                </Typography>
                <Paper className={classes.statPaperContainer} square>
                  <div className={classes.statDataContainer}>
                    {this.getAllOffersContent()}
                    <div className={classes.buttonContainer}>
                      <Button
                        onClick={() =>
                          this.navigateToUrl("/supplieroffers#alloffers")
                        }
                        color="primary"
                      >
                        View Public Offers
                      </Button>
                    </div>
                  </div>
                </Paper>
              </FadeInUpAnimation>
            </Grid>
            <Grid
              container
              onClick={() => this.navigateToUrl("/locations/addlocation")}
              className={classes.noLocationsContainer}
            >
              <Paper className={classes.noLocationsCard} square>
                <Grid item xs={12}>
                  <AddLocation className={classes.iconNormal} />
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    variant="subtitle1"
                    className={classes.noLocationsTitle}
                  >
                    You have not added a Location for your Organisation.
                  </Typography>
                  <Typography variant="body1">
                    If you would you would like to offer services directly to
                    Users, you can add a Location here.
                  </Typography>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </DashboardLayout>
      </AnimatedView>
    );
  }

  getSupplierOffersContent = () => {
    const { classes, controller } = this.props;
    const { isSupplierOffersLoaded, supplierObjects } = controller;
    if (!isSupplierOffersLoaded) {
      return (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      );
    }
    if (supplierObjects.length === 0) {
      return (
        <Typography variant="body1">You have no current offers</Typography>
      );
    }
    return (
      <List>
        {supplierObjects.map(offer => (
          <ListItem
            button
            key={offer.id}
            onClick={() =>
              this.navigateToUrl(`/supplieroffers/view/${offer.id}`)
            }
          >
            <ListItemAvatar>
              {offer.image ? (
                <Avatar alt={offer.title} src={offer.image} />
              ) : (
                <Avatar alt={offer.title} src={NoImageIcon} />
              )}
            </ListItemAvatar>
            <ListItemText
              primary={offer.title}
              secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    className={classes.listItemInline}
                    color="textPrimary"
                  >
                    Ending at - {offer.endDate}
                  </Typography>
                </React.Fragment>
              }
            />
          </ListItem>
        ))}
      </List>
    );
  };

  getAllOffersContent = () => {
    const { classes, controller } = this.props;
    const { isAllOffersLoaded, latestOffersObjects } = controller;
    if (!isAllOffersLoaded) {
      return (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      );
    }
    if (latestOffersObjects.length === 0) {
      return (
        <Typography variant="subtitle1">You have no current offers</Typography>
      );
    }
    return (
      <List>
        {latestOffersObjects.map(offer => (
          <ListItem
            button
            key={offer.id}
            onClick={() =>
              this.navigateToUrl(`/supplieroffers/view/${offer.id}`)
            }
          >
            <ListItemAvatar>
              {offer.image ? (
                <Avatar alt={offer.title} src={offer.image} />
              ) : (
                <Avatar alt={offer.title} src={NoImageIcon} />
              )}
            </ListItemAvatar>
            <ListItemText
              primary={offer.title}
              secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    className={classes.listItemInline}
                    color="textPrimary"
                  >
                    Ending at - {offer.endDate}
                  </Typography>
                </React.Fragment>
              }
            />
          </ListItem>
        ))}
      </List>
    );
  };
}

export default withStyles(styles)(NoLocationDashboardView);
