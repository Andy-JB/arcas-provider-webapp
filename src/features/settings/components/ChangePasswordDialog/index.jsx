// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import styles from "./styles";

type Props = {
  controller: any,
  form: any,
  open: boolean,
  classes: any,
  handleClose: () => mixed,
  displayLoading: boolean
};

@observer
class ChangePasswordDialog extends Component<Props> {
  render() {
    const {
      controller,
      handleClose,
      form,
      open,
      classes,
      displayLoading
    } = this.props;
    // const errors = form.errors();
    const existingPassword = form.$("existingPassword");
    const password = form.$("password");
    const passwordConfirm = form.$("passwordConfirm");
    return (
      <div>
        <Dialog
          open={open}
          onClose={() => controller.handleDialogClose()}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle
            className={classes.changePasswordTitle}
            id="form-dialog-title"
          >
            Change password
          </DialogTitle>
          <form className={classes.changePasswordForm} onSubmit={form.onSubmit}>
            <DialogContent>
              <DialogContentText className={classes.changePasswordInstructions}>
                {displayLoading
                  ? "Updating your password"
                  : "Complete the following to change your password"}
              </DialogContentText>
              {displayLoading ? (
                this.renderLoading()
              ) : (
                <div>
                  <TextField
                    {...existingPassword.bind()}
                    autoComplete="password"
                    // helperText={errors.existingPassword}
                    // error={errors.existingPassword != null}
                    className={classes.changePasswordField}
                    fullWidth
                  />
                  <TextField
                    {...password.bind()}
                    placeholder="New Password"
                    autoComplete="password"
                    // helperText={errors.password}
                    // error={errors.password != null}
                    className={classes.changePasswordField}
                    fullWidth
                  />
                  <TextField
                    {...passwordConfirm.bind()}
                    placeholder="Re-enter new Password"
                    autoComplete="password"
                    // helperText={errors.passwordConfirm}
                    // error={errors.passwordConfirm != null}
                    className={classes.changePasswordField}
                    fullWidth
                  />
                </div>
              )}
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="primary">
                Save
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }

  renderLoading = () => {
    const { classes } = this.props;
    return (
      <div className={classes.loading}>
        <CircularProgress />
      </div>
    );
  };
}

export default withStyles(styles)(ChangePasswordDialog);
