// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { observer, inject } from "mobx-react";
import Grid from "@material-ui/core/Grid";
import EditTextDialog from "common/dialogs/EditTextDialog";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any,
  UserStore: any
};

@inject("UserStore")
@observer
class PersonalSettingsView extends Component<Props> {
  renderEditDialogs = () =>
    this.props.controller.editDialogs.map(dialog => (
      <EditTextDialog
        key={dialog.title}
        dialogOpen={dialog.isOpen}
        onClose={dialog.onClose}
        buttonTitle="Update"
        dialogTitle="Personal Details Update"
        defaultValue={dialog.defaultValue}
        onUpdate={dialog.onEdit}
        textFieldLabel={dialog.label}
        validationRules={dialog.rules}
      />
    ));

  render() {
    const { controller, classes, UserStore } = this.props;

    const { openEditDialog } = controller;
    return (
      <div>
        <Grid
          container
          spacing={10}
          className={classes.generalSettingsContainer}
        >
          {/*  About You Section */}
          <Grid item xs={12} sm={6}>
            <Grid container direction="column">
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      First Name
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("first")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Typography variant="body2" align="left">
                    {UserStore.user.firstName}
                  </Typography>
                </Grid>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Last Name
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("last")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Typography variant="body2" align="left">
                    {UserStore.user.lastName}
                  </Typography>
                </Grid>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Email
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("email")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Typography variant="body2" align="left">
                    {UserStore.user.email}
                  </Typography>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </Grid>
        {/* <ChangePasswordDialog
          form={changePasswordForm}
          open={isChangingPassword}
          controller={controller}
          handleClose={closePasswordModal}
          displayLoading={changePasswordLoading}
        /> */}
        {this.renderEditDialogs()}
      </div>
    );
  }
}

export default withStyles(styles)(PersonalSettingsView);
