// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import PersonalSettingsView from "./PersonalSettingsView";

type Props = {
  UserStore: any,
  AuthStore: any,
  UiEventStore: any
};

@inject("AuthStore", "UserStore", "UiEventStore")
@observer
class PersonalSettingsScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  openModals: {
    isUpdatingDisplayPicture: boolean,
    isChangingPassword: boolean
  };

  @observable
  changePasswordLoading: boolean;

  @observable
  openDialogTitle: string;

  constructor(props: Object) {
    super(props);
    this.openModals = {
      isUpdatingDisplayPicture: false,
      isChangingPassword: false
    };
    this.changePasswordLoading = false;
  }

  render() {
    return <PersonalSettingsView controller={this} />;
  }

  @action
  handleDialogClose = () => {
    Object.keys(this.openModals).forEach(key => {
      this.openModals[key] = false;
    });
  };

  @action
  closePasswordModal = () => {
    this.openModals.isChangingPassword = false;
  };

  @action
  openPasswordModal = () => {
    this.openModals.isChangingPassword = true;
  };

  @action
  closeDisplayPictureModal = () => {
    this.openModals.isUpdatingDisplayPicture = false;
  };

  @action
  openDisplayPictureModal = () => {
    this.openModals.isUpdatingDisplayPicture = true;
  };

  @action
  handleSubmitImage = (imagePreviewFile: any) => {
    const { UserStore } = this.props;
    UserStore.uploadAvatar(imagePreviewFile).then(() => {
      this.closeDisplayPictureModal();
    });
  };

  @action
  onChangePasswordSuccess(form: Object) {
    const { AuthStore, UserStore, UiEventStore } = this.props;
    const { email } = UserStore.user;
    const { existingPassword, password } = form.values();
    this.changePasswordLoading = true;
    AuthStore.doChangePassword(email, existingPassword, password)
      .then(() => {
        this.changePasswordLoading = false;
        UiEventStore.triggerSnackbarSuccess("Password changed successfully.");
        this.closePasswordModal();
      })
      .catch(() => {
        UiEventStore.triggerSnackbarFail("Wrong existing password entered.");
        this.changePasswordLoading = false;
      });
  }

  @action
  onChangePasswordFail(): void {}

  @computed
  get changePasswordFields(): Array<any> {
    return [
      {
        name: "existingPassword",
        type: "password",
        label: "Existing Password",
        rules: "required|string"
      },
      {
        name: "password",
        type: "password",
        label: "New Password",
        rules: "required|string|between:5,25"
      },
      {
        name: "passwordConfirm",
        type: "password",
        label: "Password Confirmation",
        rules: "required|string|same:password"
      }
    ];
  }

  @action
  openEditDialog = (dialogName: string) => {
    this.openDialogTitle = dialogName;
  };

  @computed
  get user(): Object {
    return this.props.UserStore.user;
  }

  @computed
  get currentOrganisation(): Object {
    return this.props.UserStore.currentOrganisation;
  }

  @computed
  get editDialogs(): Array<Object> {
    return [
      {
        title: "first",
        label: "First Name",
        defaultValue: this.user.firstName,
        rules: "required|string|max:60",
        storeRef: this.props.UserStore.updateFirstName.bind(this)
      },
      {
        title: "last",
        label: "Last Name",
        defaultValue: this.user.lastName,
        rules: "required|string|max:60",
        storeRef: this.props.UserStore.updateLastName.bind(this)
      },
      {
        title: "email",
        label: "Email",
        defaultValue: this.user.email,
        rules: "required|string|max:60",
        storeRef: this.props.UserStore.updateEmail.bind(this)
      }
    ].map(dialog => {
      const updatedDialog: any = dialog;
      updatedDialog.onClose = () => {
        this.openDialogTitle = "";
      };
      updatedDialog.onEdit = (newValue: string) => {
        updatedDialog.storeRef(newValue);
        this.openDialogTitle = "";
      };
      updatedDialog.isOpen = this.openDialogTitle === updatedDialog.title;
      return updatedDialog;
    });
  }
}

export default PersonalSettingsScreen;
