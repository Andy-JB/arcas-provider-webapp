// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import OrgSettingsView from "./OrgSettingsView";

type Props = {
  UserStore: any
};

@inject("UserStore")
@observer
class OrganisationSettingsScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  openModals: {
    isUpdatingDisplayPicture: boolean,
    isChangingPassword: boolean
  };

  @observable
  changePasswordLoading: boolean;

  @observable
  openDialogTitle: string;

  constructor(props: Object) {
    super(props);
    this.openModals = {
      isUpdatingDisplayPicture: false,
      isChangingPassword: false
    };
    this.changePasswordLoading = false;
  }

  render() {
    return <OrgSettingsView controller={this} />;
  }

  @action
  handleDialogClose() {
    Object.keys(this.openModals).forEach(key => {
      this.openModals[key] = false;
    });
  }

  @action
  closePasswordModal() {
    this.openModals.isChangingPassword = false;
  }

  @action
  openPasswordModal() {
    this.openModals.isChangingPassword = true;
  }

  @action
  closeDisplayPictureModal = () => {
    this.openModals.isUpdatingDisplayPicture = false;
  };

  @action
  openDisplayPictureModal = () => {
    this.openModals.isUpdatingDisplayPicture = true;
  };

  @action
  handleSubmitImage = (imagePreviewFile: any) => {
    const { UserStore } = this.props;
    UserStore.uploadAvatar(imagePreviewFile).then(() => {
      this.closeDisplayPictureModal();
    });
  };

  @action
  openEditDialog = (dialogName: string) => {
    this.openDialogTitle = dialogName;
  };

  @computed
  get user(): Object {
    return this.props.UserStore.user;
  }

  @computed
  get currentOrganisation(): Object {
    return this.props.UserStore.currentOrganisation;
  }

  @computed
  get editDialogs(): Array<Object> {
    return [
      {
        title: "orgName",
        label: "Organisation Name",
        defaultValue: this.currentOrganisation.orgName,
        rules: "required|string|max:60",
        storeRef: this.props.UserStore.updateName.bind(this)
      },
      {
        title: "abn",
        label: "ABN",
        defaultValue: this.currentOrganisation.abn,
        rules: "required|numeric|digits:11",
        storeRef: this.props.UserStore.updateAbn.bind(this)
      },
      {
        title: "phone",
        label: "Phone",
        defaultValue: this.currentOrganisation.phone,
        rules: "required|numeric|digits:10",
        type: "tel",
        storeRef: this.props.UserStore.updatePhone.bind(this)
      }
    ].map(dialog => {
      const updatedDialog: any = dialog;
      updatedDialog.onClose = () => {
        this.openDialogTitle = "";
      };
      updatedDialog.onEdit = (newValue: string) => {
        updatedDialog.storeRef(newValue);
        this.openDialogTitle = "";
      };
      updatedDialog.isOpen = this.openDialogTitle === updatedDialog.title;
      return updatedDialog;
    });
  }
}

export default OrganisationSettingsScreen;
