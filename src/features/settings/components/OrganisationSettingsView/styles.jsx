// @flow
const styles = (theme: any) => ({
  root: {
    flexGrow: 1
  },
  layout: {
    [theme.breakpoints.up("md")]: {
      marginLeft: 90,
      marginRight: 90
    },
    paddingTop: 30
  },
  heading: {
    marginBottom: 5
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  pageHeading: {
    marginBottom: 20,
    marginTop: 10
  },
  submitButton: {
    marginTop: 30
  },
  editButton: {
    width: 20,
    height: 20,
    padding: 0
  },
  changePasswordField: {
    marginBottom: 25
  },
  dropzone: {
    width: "100%",
    minHeight: 100,
    height: "100%",
    border: "1px solid black"
  },
  panelContainer: {},
  imagePreview: {
    height: 100,
    width: 100
  },
  settingSection: {
    padding: 15,
    width: "100%"
  },
  generalSettingsContainer: {
    padding: 30
  },
  settingRow: {},
  settingHeading: {
    paddingTop: 10
  },
  settingSecondaryHeading: {
    color: theme.palette.text.secondary
  },
  settingsRow: {
    padding: 20
  },
  avatarPicture: {
    maxHeight: 150,
    maxWidth: 150,
    overflow: "hidden",
    marginBottom: 20
  },
  noAvatarPicture: {
    height: 80,
    width: 80,
    overflow: "hidden",
    marginBottom: 20
  },
  updateAvatarButtonRow: {
    textAlign: "right"
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary
  },
  displayPictureContainer: {
    paddingLeft: 50
  },
  profileInformationField: {
    paddingBottom: 15
  }
});

export default styles;
