// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { observer, inject } from "mobx-react";
import Grid from "@material-ui/core/Grid";
import EditTextDialog from "common/dialogs/EditTextDialog";
import FileUploadDialog from "common/dialogs/FileUploadDialog";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import ImageSearch from "@material-ui/icons/ImageSearch";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any,
  UserStore: any
};

@inject("UserStore")
@observer
class OrgSettingsView extends Component<Props> {
  renderEditDialogs = () =>
    this.props.controller.editDialogs.map(dialog => (
      <EditTextDialog
        key={dialog.title}
        dialogOpen={dialog.isOpen}
        onClose={dialog.onClose}
        buttonTitle="Update"
        dialogTitle="Organisation Update"
        defaultValue={dialog.defaultValue}
        onUpdate={dialog.onEdit}
        textFieldLabel={dialog.label}
        validationRules={dialog.rules}
      />
    ));

  render() {
    const { controller, classes, UserStore } = this.props;
    const { isUpdatingDisplayPicture } = controller.openModals;
    const {
      closeDisplayPictureModal,
      handleSubmitImage,
      openEditDialog
    } = controller;

    return (
      <div>
        <Grid
          container
          spacing={10}
          className={classes.generalSettingsContainer}
        >
          {/*  About You Section */}
          <Grid item xs={12} sm={6}>
            <Grid container direction="column">
              <Grid container>
                <Grid item xs={12}>
                  <Typography variant="caption" className={classes.heading}>
                    Organisation Logo
                  </Typography>
                </Grid>
                <Grid container direction="row-reverse">
                  <Grid item xs={1}>
                    <IconButton
                      className={classes.editButton}
                      aria-label="Edit"
                      onClick={controller.openDisplayPictureModal}
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    onClick={controller.openDisplayPictureModal}
                  >
                    {UserStore.orgLogo ? (
                      <img
                        src={UserStore.orgLogo}
                        alt="avatar"
                        className={classes.avatarPicture}
                      />
                    ) : (
                      <ImageSearch className={classes.noAvatarPicture} />
                    )}
                  </Grid>
                </Grid>
              </Grid>{" "}
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Organisation Name
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("orgName")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Typography variant="body2" align="left">
                    {UserStore.user.organisation.orgName}
                  </Typography>
                </Grid>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Organisation ABN
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("abn")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Typography variant="body2" align="left">
                    {UserStore.currentOrganisation.abn}
                  </Typography>
                </Grid>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Organisation Phone
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("phone")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Typography variant="body2" align="left">
                    {UserStore.currentOrganisation.phone}
                  </Typography>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </Grid>

        <FileUploadDialog
          displayLoading={UserStore.isUploadingImage}
          openDialog={isUpdatingDisplayPicture}
          onClose={closeDisplayPictureModal}
          onSubmit={handleSubmitImage}
        />
        {this.renderEditDialogs()}
      </div>
    );
  }
}

export default withStyles(styles)(OrgSettingsView);
