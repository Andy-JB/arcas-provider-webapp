// @flow

const formInputAdornments = {
  inputAdornmentLabel: {
    paddingLeft: 6,
    paddingTop: 15,
    fontSize: 14
  }
};

const filterSidebar = {
  filterLocationContainer: {
    paddingTop: 15,
    paddingBottom: 15,
    zIndex: 999
  },
  filterSearchContainer: {
    paddingTop: 10,
    paddingBottom: 10
  },
  filterSearchTextField: {
    height: 40
  }
};

const styles = (theme: any) => ({
  ...formInputAdornments,
  ...filterSidebar,
  headerDescription: {
    paddingLeft: 50,
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 10,
      paddingBottom: 10
    }
  },
  root: {
    [theme.breakpoints.up("md")]: {
      width: "100%"
    }
  },
  distanceSlider: {
    paddingTop: 15,
    paddingBottom: 15,
    zIndex: 0
  },
  sliderControl: {
    marginTop: 10,
    marginBottom: 10
  },
  distHeading: {
    marginBottom: 25
  },
  clearFilter: {
    marginTop: 15,
    float: "right"
  },
  resultsSubheading: {
    fontWeight: 500,
    color: "#191919"
  },
  locationField: {
    paddingTop: 10
  }
});

export default styles;
