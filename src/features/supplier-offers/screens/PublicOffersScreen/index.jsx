// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { computed, action, observable } from "mobx";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import history from "appHistory";
import LoadingScreen from "common/screens/LoadingScreen";
import PublicOffersView from "./PublicOffersView";
import styles from "./styles";

type Props = {
  SupplierOfferStore: any,
  PostcodeStore: any,
  GeoFireStore: any,
  isActiveTab: boolean
};

const defaultLocation = { value: "Adelaide", label: "Adelaide (5000)" };

@inject(
  "AuthStore",
  "UserStore",
  "SupplierOfferStore",
  "GeoFireStore",
  "PostcodeStore"
)
@observer
class SupplierOffersTabScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  filters: {
    distance: number,
    suburb: Object,
    searchString: string
  };

  @observable
  queriedParameters;

  @observable
  filterOn = false;

  @observable
  resultsHeading = "";

  @observable
  filtering = false;

  constructor(props: Object) {
    super(props);
    this.setDefaultFilters();
  }

  componentDidMount() {
    const { SupplierOfferStore, GeoFireStore } = this.props;
    GeoFireStore.deleteAllData();
    SupplierOfferStore.allData = null;
    SupplierOfferStore.getAllOffers();
  }

  render() {
    const { SupplierOfferStore } = this.props;
    if (!this.filterOn && SupplierOfferStore.allData == null) {
      return <LoadingScreen />;
    }
    return (
      <PublicOffersView
        {...this.props}
        controller={this}
        loading={this.isLoading}
        suburb={this.filters.suburb}
      />
    );
  }

  @action
  handleSuburbChange = selectedOption => {
    const selection = (() => {
      if (selectedOption && selectedOption.length > 0) {
        return selectedOption.pop();
      }
      return null;
    })();
    this.filters.suburb = selection;
  };

  @action
  handleSearchStringChange = event => {
    this.filters.searchString = event.target.value;
  };

  @action
  handleSliderChange = (event, value) => {
    this.filters.distance = value;
    this.filters = {
      ...this.filters,
      distance: value
    };
  };

  @action
  parseOfferData(x: any) {
    let image;
    if (x.images && x.images.length > 0) {
      image = x.images[0]; // eslint-disable-line
    }
    return {
      id: x.id,
      title: x.title,
      description: x.description,
      location: x.location,
      avatar: x.orgAvatar,
      subtitle: x.orgName,
      image,
      startDate: moment(x.startDate.toDate()).format("DD MMMM YYYY"),
      endDate: moment(x.endDate.toDate()).format("DD MMMM YYYY")
    };
  }

  @action
  didViewOffer = (offerId: string) => {
    history.push(`supplieroffers/view/${offerId}`);
  };

  @action
  applyFilters = () => {
    const { searchString, distance } = this.filters;
    const { GeoFireStore, PostcodeStore } = this.props;
    this.filterOn = true;
    GeoFireStore.deleteAllData();
    PostcodeStore.getLocation(this.selectedSuburb.value, "SA").then(data => {
      this.updateHeading();
      GeoFireStore.filterData(
        "supplier-offers",
        data.longitude,
        data.latitude,
        distance,
        this.selectedSuburb.value,
        searchString
      );
    });
  };

  @action
  updateHeading() {
    if (this.filterOn) {
      const { searchString, distance } = this.filters;
      if (searchString !== "") {
        this.resultsHeading = `Showing results containing '${searchString}' within ${distance} km of ${this.selectedSuburb.label}`;
      } else {
        this.resultsHeading = `Showing results within ${distance} km of ${this.selectedSuburb.label}`;
      }
    } else {
      this.resultsHeading = "Showing all offers";
    }
  }

  @computed
  get selectedSuburb(): string {
    const { suburb } = this.filters;
    if (suburb) {
      return suburb;
    }
    return defaultLocation;
  }

  @action
  setDefaultFilters = () => {
    this.filters = {
      suburb: defaultLocation,
      distance: 1,
      searchString: ""
    };
  };

  @action
  clearFilter = () => {
    const { GeoFireStore } = this.props;
    this.setDefaultFilters();
    GeoFireStore.deleteAllData();
    this.filterOn = false;
    this.updateHeading();
  };

  @computed
  get isLoading() {
    const { GeoFireStore, PostcodeStore } = this.props;
    return PostcodeStore.filtering || (this.filterOn && GeoFireStore.isLoading);
  }

  @computed
  get suburbPostcodeOptions() {
    const { PostcodeStore } = this.props;
    return PostcodeStore.suburbPostcodeOptions;
  }

  @computed
  get marks(): Array<{ label: string, value: number }> {
    return [1, 5, 10, 25, 50, 100].map(x => {
      return {
        label: "",
        value: x
      };
    });
  }

  @computed
  get filtering(): boolean {
    return this.props.PostcodeStore.filtering;
  }

  @computed
  get supplierObjects(): Array<Object> {
    const { SupplierOfferStore, GeoFireStore, isActiveTab } = this.props;
    if (GeoFireStore.allData && GeoFireStore.allData.length > 0) {
      return GeoFireStore.allData.map(x => this.parseOfferData(x));
    }
    if (SupplierOfferStore.allData && isActiveTab && !this.filterOn) {
      return SupplierOfferStore.allData.map(x => this.parseOfferData(x));
    }
    return [];
  }
}

export default withStyles(styles)(SupplierOffersTabScreen);
