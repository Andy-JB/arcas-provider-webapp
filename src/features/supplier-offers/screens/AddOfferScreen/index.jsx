// @flow
import React, { Component } from "react";
import { computed, action, observable } from "mobx";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import DashboardOverlapLayout from "common/layouts/DashboardOverlapLayout";
import MobxForm from "util/MobxForm";
import history from "appHistory";
import moment from "moment";
import LocalOffer from "@material-ui/icons/LocalOfferOutlined";
import LoadingScreen from "common/screens/LoadingScreen";
import styles from "./styles";
import AddOfferView from "./AddOfferView";

type Props = {
  SupplierOfferStore: any,
  PostcodeStore: any,
  UserStore: any,
  location: any,
  match: any
};

type OfferImage = {
  url: any,
  imagePreviewFile: any
};

@inject("AuthStore", "UserStore", "SupplierOfferStore", "PostcodeStore")
@observer
class AddOfferScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  contactForm: any;

  @observable
  offerForm: any;

  @observable
  imageLimit: number;

  @observable
  images: Array<OfferImage>;

  @observable
  isUploadingImage: boolean;

  @observable
  showCancelDialog: boolean;

  @observable
  activeStep: number;

  @observable
  imageKey: number = -1;

  @observable
  suburb = {
    value: this.suggestedContactDetails().location,
    label: this.suggestedContactDetails().location
  };

  constructor(props: Object) {
    super(props);
    this.imageLimit = 6;
    this.isUploadingImage = false;
    this.showCancelDialog = false;
    this.images = [];
    this.onFormSuccess = this.onFormSuccess.bind(this);
    this.onFormError = this.onFormError.bind(this);
    this.createNewOffer = this.createNewOffer.bind(this);
    this.didEditImage = this.didEditImage.bind(this);
    this.didDeleteImage = this.didDeleteImage.bind(this);
    this.addNewImage = this.addNewImage.bind(this);
    this.closeImageUpload = this.closeImageUpload.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleCancelConfirm = this.handleCancelConfirm.bind(this);
    this.handleCancelClose = this.handleCancelClose.bind(this);
    this.handleSuburbChange = this.handleSuburbChange.bind(this);
    this.setupScreen();
  }

  setupScreen = () => {
    this.setupOfferFields();
    this.setupContactFields();
    this.setupInitialData().then(() => {
      if (this.isEditing) {
        if (!this.isEditingMyOffer) {
          history.push("/supplieroffers");
        }
        if (this.editingOffer.images) {
          this.images = this.editingOffer.images.map(url => ({ url }));
        }
      }
      this.setupOfferFields();
      this.setupContactFields();
      this.activeStep = 0;
    });
  };

  render() {
    return (
      <DashboardOverlapLayout
        animatePaper
        pageTitle={this.pageTitle}
        headerIcon={LocalOffer}
      >
        {this.displayLoading === true ? (
          <LoadingScreen />
        ) : (
          <AddOfferView
            controller={this}
            offerForm={this.offerForm}
            contactForm={this.contactForm}
          />
        )}
      </DashboardOverlapLayout>
    );
  }

  componentWillUnmount() {
    const { SupplierOfferStore } = this.props;
    SupplierOfferStore.editingOffer = null;
  }

  @action
  handleSuburbChange = selectedOption => {
    this.suburb = selectedOption;
  };

  @computed
  get suburbOptions() {
    const { PostcodeStore } = this.props;
    return PostcodeStore.suburbOnlyOptions;
  }

  setupInitialData = () => {
    const { SupplierOfferStore } = this.props;
    if (this.isEditing && !this.editingOffer) {
      return SupplierOfferStore.getDocument(this.props.match.params.id)
        .then(doc => {
          SupplierOfferStore.editingOffer = { ...doc.data().d, id: doc.id };
        })
        .catch(() => {
          history.push("/supplieroffers");
        });
    }
    return Promise.resolve();
  };

  @computed
  get isEditingMyOffer(): boolean {
    if (this.editingOffer) {
      return this.props.UserStore.isOrganisation(this.editingOffer.orgName);
    }
    return false;
  }

  @computed
  get pageTitle(): string {
    if (this.isEditing) {
      return "Edit offer";
    }
    return "Add new offer";
  }

  @computed
  get editingOffer(): any {
    const { SupplierOfferStore } = this.props;
    return SupplierOfferStore.editingOffer;
  }

  @computed
  get prefilledData(): Object {
    if (this.isEditing && this.editingOffer) {
      return this.editingOffer;
    }
    return this.suggestedContactDetails();
  }

  @computed
  get displayLoading(): boolean {
    const { SupplierOfferStore } = this.props;
    if (this.isEditing && !this.editingOffer) {
      return true;
    }
    if (SupplierOfferStore.isUpdatingData) {
      return true;
    }
    return false;
  }

  @computed
  get isEditing(): boolean {
    const { location } = this.props;
    return location.pathname.includes("edit");
  }

  @computed
  get steps(): Array<any> {
    return ["Offer details", "Images", "Contact"];
  }

  @computed
  get currentFormValues() {
    return {
      ...this.offerForm.values(),
      ...this.contactForm.values()
    };
  }

  @computed
  get backDisabled(): boolean {
    return this.activeStep === 0;
  }

  @computed
  get contentForStep() {
    switch (this.activeStep) {
      case 0:
        return "OFFER_DETAILS";
      case 1:
        return "IMAGES";
      case 2:
        return "CONTACT";
      case 3:
        return "REVIEW";
      default:
        return "LOADING";
    }
  }

  @action
  handleNext = e => {
    switch (this.contentForStep) {
      case "OFFER_DETAILS":
        this.offerForm.submit(e);
        break;
      case "CONTACT":
        this.contactForm.submit(e);
        break;
      case "IMAGES":
        this.goNext();
        break;
      case "REVIEW":
      default:
        this.createNewOffer();
        break;
    }
  };

  @action
  handleCancelConfirm = () => {
    this.showCancelDialog = false;
    history.push("/supplieroffers");
  };

  @action
  handleCancelClose = () => {
    this.showCancelDialog = false;
  };

  @action
  handleCancel = () => {
    this.showCancelDialog = true;
  };

  @action
  handleSubmitImage = (imagePreviewFile: any, url: string) => {
    if (this.imageKey === -1) {
      this.images.push({ url, imagePreviewFile });
    } else {
      this.images[this.imageKey] = { url, imagePreviewFile };
    }
    this.closeImageUpload();
  };

  @action
  closeImageUpload = () => {
    this.isUploadingImage = false;
    this.imageKey = -1;
  };

  @action
  didEditImage = (index: number) => {
    this.imageKey = index;
    this.isUploadingImage = true;
  };

  @action
  didDeleteImage = (url: string) => {
    this.images = this.images.filter(x => x.url !== url);
  };

  @action
  addNewImage = () => {
    this.didEditImage(-1);
  };

  @computed
  get showAddImageTile(): boolean {
    return this.images.length < this.imageLimit;
  }

  @action
  createNewOffer = () => {
    const { SupplierOfferStore, UserStore } = this.props;
    const orgDetails = {
      orgName: UserStore.currentOrganisation.orgName,
      orgAvatar: UserStore.currentOrganisation.avatar
        ? UserStore.currentOrganisation.avatar
        : "",
      organisation: UserStore.currentOrganisation.id
    };
    SupplierOfferStore.createOrUpdate(
      {
        ...this.offerForm.values(),
        ...this.contactForm.values(),
        location: this.suburb.value,
        images: this.images,
        ...orgDetails
      },
      this.isEditing ? this.editingOffer.id : null
    ).then(() => {
      history.push("/supplieroffers");
    });
  };

  @action
  goNext = () => {
    this.activeStep += 1;
  };

  @action
  goBack = () => {
    this.activeStep -= 1;
  };

  @action
  handleBack = () => {
    this.goBack();
  };

  suggestedContactDetails = () => {
    const { UserStore } = this.props;
    return {
      location: "Adelaide",
      email: UserStore.user.email,
      phone: UserStore.currentOrganisation.phone
    };
  };

  setupContactFields(): void {
    const fields = [
      {
        name: "location",
        label: "Location",
        rules: "required|string|between:3,30"
      },
      {
        name: "email",
        type: "email",
        label: "Contact Email (Optional)",
        rules: "email|string|max:100"
      },
      {
        name: "phone",
        label: "Contact Number",
        rules: "required|numeric|digits:10"
      }
    ];
    this.contactForm = new MobxForm(
      fields,
      this.onFormSuccess,
      this.onFormError
    );
    this.contactForm.prefillData(this.prefilledData);
  }

  setupOfferFields = () => {
    const todayMoment = moment(new Date()).format("YYYY-MM-DD");
    const fields = [
      {
        name: "title",
        label: "Title",
        rules: "required|string|between:3,60"
      },
      {
        name: "description",
        label: "Description",
        rules: "required|string|max:500"
      },
      {
        name: "today",
        type: "date",
        label: "Current Date",
        value: todayMoment
      },
      {
        name: "startDate",
        type: "date",
        label: "Start Date",
        rules: "required|date|after_or_equal:today",
        value: ""
      },
      {
        name: "endDate",
        type: "date",
        label: "End Date",
        rules: "required|date|after_or_equal:startDate",
        value: ""
      }
    ];
    this.offerForm = new MobxForm(fields, this.onFormSuccess, this.onFormError);
    this.offerForm.prefillData(this.prefilledData);
  };

  @action
  onFormSuccess = () => {
    this.goNext();
  };

  @action
  onFormError = () => {};
}

export default withStyles(styles)(AddOfferScreen);
