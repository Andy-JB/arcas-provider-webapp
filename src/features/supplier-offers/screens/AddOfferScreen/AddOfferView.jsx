// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer, inject } from "mobx-react";
import { Grid, Button, TextField } from "@material-ui/core";
import Stepper from "@material-ui/core/Stepper";
import Typography from "@material-ui/core/Typography";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import NoImageIcon from "media/images/add-location.png";
import LoadingScreen from "common/screens/LoadingScreen";
import FileUploadDialog from "common/dialogs/FileUploadDialog";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/DeleteOutline";
import DialogTitle from "@material-ui/core/DialogTitle";
import Select from "react-select";
import styles from "./styles";

type Props = {
  SupplierOfferStore: any,
  controller: any,
  classes: any,
  offerForm: any,
  contactForm: any
};

@inject("SupplierOfferStore")
@observer
class AddOfferView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const {
      activeStep,
      steps,
      displayLoading,
      handleBack,
      handleNext,
      backDisabled,
      handleCancel,
      handleCancelClose,
      handleCancelConfirm,
      showCancelDialog
    } = controller;
    if (displayLoading) {
      return <LoadingScreen />;
    }
    return (
      <Grid container>
        {activeStep !== steps.length && (
          <div className={classes.stepperContainer}>
            <Stepper activeStep={activeStep}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
          </div>
        )}
        <Grid container>{this.renderContentForStep()}</Grid>
        <Grid item xs={12} className={classes.stepperButtonContainer}>
          <Button onClick={handleCancel}>Cancel</Button>
          <Button onClick={handleBack} disabled={backDisabled}>
            Back
          </Button>
          <Button onClick={handleNext} variant="contained" color="primary">
            {activeStep === steps.length ? "Confirm and Submit" : "Next"}
          </Button>
        </Grid>
        <Dialog
          open={showCancelDialog}
          onClose={handleCancelClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Any of the changes that you have made will be lost. Press
              &quot;Yes&quot; to cancel.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCancelClose} color="primary">
              No
            </Button>
            <Button onClick={handleCancelConfirm} color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }

  renderContentForStep = () => {
    const { controller } = this.props;
    const { classes } = this.props;
    if (controller.contentForStep === "LOADING") {
      return <LoadingScreen />;
    }
    if (controller.contentForStep === "OFFER_DETAILS") {
      const { offerForm } = this.props;
      const title = offerForm.$("title");
      const description = offerForm.$("description");
      const startDate = offerForm.$("startDate");
      const endDate = offerForm.$("endDate");
      const type = null;
      const errors = offerForm.errors();
      return (
        <form onSubmit={offerForm.onSubmit} className={classes.formContainer}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                {...title.bind({ type })}
                variant="outlined"
                margin="normal"
                fullWidth
                helperText={errors.title}
                error={errors.title != null}
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                {...description.bind({})}
                variant="outlined"
                multiline
                rows="4"
                rowsMax="8"
                margin="normal"
                helperText={errors.description}
                error={errors.description != null}
                fullWidth
              />
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  {...startDate.bind({})}
                  variant="outlined"
                  margin="normal"
                  type="date"
                  InputLabelProps={{ shrink: true }}
                  helperText={errors.startDate}
                  error={errors.startDate != null}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  {...endDate.bind({})}
                  helperText={errors.endDate}
                  type="date"
                  error={errors.endDate != null}
                  InputLabelProps={{ shrink: true }}
                  variant="outlined"
                  margin="normal"
                  fullWidth
                />
              </Grid>
            </Grid>
          </Grid>
        </form>
      );
    }
    if (controller.contentForStep === "CONTACT") {
      const { contactForm } = this.props;
      const email = contactForm.$("email");
      const phone = contactForm.$("phone");
      // const location = contactForm.$("location");
      const type = null;
      const errors = contactForm.errors();
      const { suburbOptions, suburb, handleSuburbChange } = controller;
      return (
        <form onSubmit={contactForm.onSubmit} className={classes.formContainer}>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant="body1">Location</Typography>
              <Select
                className={classes.filterLocationContainer}
                options={suburbOptions}
                value={suburb}
                onChange={handleSuburbChange}
                placeholder="Select a suburb"
                margin="normal"
              />
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  {...email.bind({ type })}
                  variant="outlined"
                  margin="normal"
                  helperText={errors.email}
                  error={errors.email != null}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  {...phone.bind({ type })}
                  helperText={errors.phone}
                  error={errors.phone != null}
                  variant="outlined"
                  margin="normal"
                  fullWidth
                />
              </Grid>
            </Grid>
          </Grid>
        </form>
      );
    }
    if (controller.contentForStep === "IMAGES") {
      const { closeImageUpload, handleSubmitImage } = controller;
      const { isUploadingImage } = controller;
      const {
        didEditImage,
        images,
        addNewImage,
        showAddImageTile,
        didDeleteImage
      } = controller;
      return (
        <Grid container className={classes.imageUploadContainer}>
          <Grid item xs={12}>
            <Typography className={classes.uploadHeading} variant="subtitle2">
              Upload some images (Maximum images: {controller.imageLimit})
            </Typography>
          </Grid>
          {images.length > 0 &&
            images.map((image, key) => (
              <Grid
                key={`img-${key + 0}`}
                item
                xs={12}
                sm={6}
                md={6}
                lg={4}
                className={classes.offerImage}
              >
                <GridListTile>
                  <Grid item onClick={() => didEditImage(key)}>
                    <img
                      className={classes.offerImageSrc}
                      alt="offer icon"
                      src={image.url}
                    />
                  </Grid>
                  <GridListTileBar
                    titlePosition="top"
                    className={classes.titleBar}
                    actionIcon={
                      <IconButton
                        onClick={() => didDeleteImage(image.url)}
                        className={classes.deleteIcon}
                      >
                        <DeleteIcon />
                      </IconButton>
                    }
                    actionPosition="top"
                  />
                </GridListTile>
                {images.length === 0 && (
                  <Typography variant="body1">No images uploaded</Typography>
                )}
              </Grid>
            ))}
          {showAddImageTile && (
            <Grid
              onClick={addNewImage}
              item
              xs={12}
              sm={6}
              md={6}
              lg={4}
              className={classes.offerImage}
            >
              <img
                src={NoImageIcon}
                alt="offer icon"
                className={classes.offerImageSrc}
              />
            </Grid>
          )}
          <FileUploadDialog
            displayLoading={false}
            openDialog={isUploadingImage}
            onClose={closeImageUpload}
            onSubmit={handleSubmitImage}
          />
        </Grid>
      );
    }
    // REVIEW contentForStep
    const { images, suburb } = controller;
    const {
      title,
      description,
      email,
      phone,
      startDate,
      endDate
    } = controller.currentFormValues;
    return (
      <Grid container>
        <div className={classes.reviewHeadingContainer}>
          <Typography variant="h6" component="h2">
            Please confirm the following
          </Typography>
        </div>
        <Grid container className={classes.reviewContainer}>
          <Grid item xs={12} md={6} className={classes.titleImagesContainer}>
            <Grid item xs={12}>
              <Typography variant="caption">Title</Typography>
              <Typography variant="body1">{title}</Typography>
            </Grid>
            <Grid className={classes.reviewAnswerContainer} item xs={12}>
              <Typography variant="caption">Description</Typography>
              <Typography variant="body1">
                {description !== "" ? description : "No description provided"}
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.dateContainer}>
              <Grid className={classes.dateInput} item xs={12} sm={6}>
                <Typography variant="caption">Start Date</Typography>
                <Typography variant="body1">{startDate}</Typography>
              </Grid>
              <Grid className={classes.dateInput} item xs={12} sm={6}>
                <Typography variant="caption">End Date</Typography>
                <Typography variant="body1">{endDate}</Typography>
              </Grid>
            </Grid>
            <Grid className={classes.reviewAnswerContainer} item xs={12}>
              <Grid item xs={12}>
                <Typography variant="caption">Images</Typography>
              </Grid>

              {images.length > 0 ? (
                images.map(x => (
                  <img
                    className={classes.reviewImage}
                    alt="offer icon"
                    src={x.url}
                  />
                ))
              ) : (
                <Typography variant="body1">No images uploaded</Typography>
              )}
            </Grid>
          </Grid>
          <Grid item xs={12} md={6} className={classes.contactDetailsContainer}>
            <Grid item xs={12}>
              <Typography variant="caption">Location</Typography>
              <Typography variant="body1">{suburb.value}</Typography>
            </Grid>
            <Grid className={classes.reviewAnswerContainer} item xs={12}>
              <Typography variant="caption">Contact Email</Typography>
              <Typography variant="body1">
                {email !== "" ? email : "None provided"}
              </Typography>
            </Grid>
            <Grid className={classes.reviewAnswerContainer} item xs={12}>
              <Typography variant="caption">Contact Number</Typography>
              <Typography variant="body1">{phone}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  };
}

export default withStyles(styles)(AddOfferView);
