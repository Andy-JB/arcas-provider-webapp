// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { observable, computed, action } from "mobx";
import { withStyles } from "@material-ui/core/styles";
import LoadingScreen from "common/screens/LoadingScreen";
import PropTypes from "prop-types";
import DashboardOverlapLayout from "common/layouts/DashboardOverlapLayout";
import history from "appHistory";
import moment from "moment";
import NoImageIcon from "media/images/add-location.png";
import ViewOfferView from "./ViewOfferView";
import styles from "./styles";

type Props = {
  classes: any,
  SupplierOfferStore: PropTypes.object,
  UiEventStore: any,
  UserStore: any,
  match: any
};

@inject("AuthStore", "UserStore", "SupplierOfferStore", "UiEventStore")
@observer
class ViewOfferScreen extends Component<Props> {
  @observable
  offer: any;

  @observable
  offerId: string;

  @observable
  lightboxOpen: boolean = false;

  @observable
  currentImage: number = 0;

  constructor(props: Object) {
    super(props);
    this.toggleLightbox = this.toggleLightbox.bind(this);
    this.offerId = this.props.match.params.id;
    this.retrieveOffer();
  }

  render() {
    return (
      <DashboardOverlapLayout
        pageTitle="Viewing Offer"
        actionButton={this.actionButton}
        secondaryActionButton={this.secondaryActionButton}
        backButton={this.backButton}
      >
        {this.offer ? (
          <ViewOfferView controller={this} {...this.props} />
        ) : (
          <LoadingScreen />
        )}
      </DashboardOverlapLayout>
    );
  }

  @action
  retrieveOffer = () => {
    const { SupplierOfferStore } = this.props;
    SupplierOfferStore.getDocument(this.offerId)
      .then(doc => {
        this.offer = { ...doc.data().d, ...doc.id };
      })
      .catch(() => {
        history.push("/supplieroffers");
      });
  };

  @action
  didEditOffer = () => {
    history.push(`/supplieroffers/edit/${this.offerId}`);
  };

  @action
  confirmDeleteOffer = () => {
    const { UiEventStore, SupplierOfferStore } = this.props;
    UiEventStore.alertDialog.onOpen({
      title: "Confirm Delete",
      description: "Are you sure you want to delete this offer?",
      onPositiveButtonClicked: () => {
        SupplierOfferStore.removeOffer(this.offerId).then(() => {
          history.push("/supplieroffers#myoffers");
        });
      }
    });
  };

  @action
  onBackClicked = () => {
    history.goBack();
  };

  @action
  toggleLightbox = () => {
    const { UiEventStore } = this.props;
    let images = [];
    if (this.images.length > 0) {
      images = this.images.map(x => ({
        src: x
      }));
    }
    UiEventStore.lightbox.openLightbox(images);
  };

  @computed
  get actionButton() {
    const { UserStore } = this.props;
    if (this.offer && UserStore.isOrganisation(this.offer.orgName)) {
      return {
        label: "Edit",
        onClick: () => this.didEditOffer()
      };
    }
    return null;
  }

  @computed
  get secondaryActionButton(): any {
    const { classes, UserStore } = this.props;
    if (this.offer && UserStore.isOrganisation(this.offer.orgName)) {
      return {
        label: "Delete Offer",
        onClick: () => this.confirmDeleteOffer(),
        className: classes.deleteButton
      };
    }
    return null;
  }

  @computed
  get backButton(): Object {
    return {
      label: "Supplier Offers",
      onClick: () => this.onBackClicked()
    };
  }

  @computed
  get images(): Array<any> {
    return this.offer.images;
  }

  @computed
  get imageCount(): string {
    const numImages = this.images.length;
    if (numImages === 0) {
      return "0 Images";
    }
    if (numImages === 1) {
      return "1 Image";
    }
    return `${numImages} mages`;
  }

  @computed
  get offerImage(): string {
    if (this.offer.images && this.offer.images.length > 0) {
      return this.offer.images[0];
    }
    return NoImageIcon;
  }

  @computed
  get orgAvatar(): string {
    if (this.offer.orgAvatar !== "") {
      return this.offer.orgAvatar;
    }
    return NoImageIcon;
  }

  @computed
  get description(): string {
    const { description } = this.offer;
    if (description === null || description === "") {
      return "No description has been provided";
    }
    return description;
  }

  @computed
  get hasImages(): boolean {
    if (this.offer.images && this.offer.images.length > 0) {
      return true;
    }
    return false;
  }

  @computed
  get startDate(): string {
    if (this.offer) {
      return moment(this.offer.startDate.toDate()).format("DD MMMM YYYY");
    }
    return "";
  }

  @computed
  get endDate(): string {
    if (this.offer) {
      return moment(this.offer.endDate.toDate()).format("DD MMMM YYYY");
    }
    return "";
  }
}

export default withStyles(styles)(ViewOfferScreen);
