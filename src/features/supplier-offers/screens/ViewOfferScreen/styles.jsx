// @flow

const styles = (theme: any) => ({
  pageContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    [theme.breakpoints.up("md")]: {
      paddingTop: 20,
      paddingLeft: 70,
      paddingRight: 70
    },
    paddingBottom: 50
  },
  headingContainer: {
    paddingTop: 20,
    paddingBottom: 10
  },
  imageGalleryContainer: {
    position: "relative",
    width: "100%",
    height: 405,
    padding: 3,
    maxHeight: 600,
    [theme.breakpoints.down("xs")]: {
      height: 200
    },
    backgroundColor: "#eaebed",
    textAlign: "center",
    cursor: "pointer",
    "-webkit-transition": ".2s all",
    "&:hover": {
      "-webkit-filter": "brightness(50%)"
    }
  },
  deleteButton: {
    backgroundColor: "red",
    "&:hover": {
      backgroundColor: "red",
      opacity: 0.9
    }
  },
  galleryImage: {
    width: "100%",
    height: 400,
    [theme.breakpoints.down("xs")]: {
      height: 200
    }
  },
  imageCount: {
    position: "absolute",
    bottom: 20,
    right: 10,
    padding: 5
  },
  orgContainer: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10
  },
  orgTitle: {
    paddingTop: 7,
    marginLeft: 10
  },
  descriptionContainer: {
    paddingTop: 30
  },
  contentSubheading: {
    fontWeight: 500
  },
  locationSideContainer: {
    paddingTop: 20,
    [theme.breakpoints.up("md")]: {
      paddingLeft: 30
    }
  },
  locationPaperContainer: {
    padding: 15
  },
  mobileContentSection: {
    paddingBottom: 15
  }
}); // eslint-disable-line

export default styles;
