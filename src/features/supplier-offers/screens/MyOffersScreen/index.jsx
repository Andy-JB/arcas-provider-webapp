// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { computed, action } from "mobx";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import history from "appHistory";
import LoadingScreen from "common/screens/LoadingScreen";
import MyOffersView from "./MyOffersView";
import styles from "./styles";

type Props = {
  SupplierOfferStore: any,
  UserStore: any,
  isActiveTab: boolean
};

@inject("AuthStore", "UserStore", "SupplierOfferStore")
@observer
class MyOffersScreen extends Component<Props> {
  constructor(props: Object) {
    super(props);
    const { SupplierOfferStore } = this.props;
    SupplierOfferStore.myData = null;
  }

  componentDidMount() {
    const { SupplierOfferStore, UserStore, isActiveTab } = this.props;
    if (!SupplierOfferStore.myData && isActiveTab) {
      SupplierOfferStore.getMyOffers(UserStore.currentOrganisation.orgName);
    }
  }

  render() {
    const { SupplierOfferStore, isActiveTab } = this.props;
    if (SupplierOfferStore.myData && isActiveTab) {
      return <MyOffersView controller={this} {...this.props} />;
    }
    return <LoadingScreen />;
  }

  @computed
  get supplierObjects(): Array<Object> {
    const { SupplierOfferStore } = this.props;
    if (SupplierOfferStore.myData) {
      return SupplierOfferStore.myData.map(x => {
        let image;
        if (x.images && x.images.length > 0) {
          image = x.images[0]; // eslint-disable-line
        }
        return {
          id: x.id,
          title: x.title,
          description: x.description,
          location: x.location,
          avatar: x.orgAvatar,
          subtitle: x.orgName,
          image,
          startDate: moment(x.startDate.toDate()).format("DD MMMM YYYY"),
          endDate: moment(x.endDate.toDate()).format("DD MMMM YYYY")
        };
      });
    }
    return [];
  }

  @action
  didViewOffer = (offerId: string) => {
    history.push(`supplieroffers/view/${offerId}`);
  };
}

export default withStyles(styles)(MyOffersScreen);
