// @flow

const styles = (theme: any) => ({
  sidebarHeading: {
    fontWeight: "bold",
    paddingBottom: 20
  },
  sidebarDescription: {
    paddingBottom: 20
  },
  sidebarTerms: {
    paddingBottom: 20
  },
  shoppingCartIcon: {
    width: "100%",
    height: "100%",
    color: theme.palette.primary.dark
  },
  shoppingCartIconContainer: {
    width: 150,
    height: 150,
    textAlign: "center"
  },
  headerContainer: {
    paddingLeft: 50,
    paddingTop: 20,
    paddingBottom: 10,
    [theme.breakpoints.down("xs")]: {
      paddingLeft: 10,
      paddingTop: 20,
      paddingBottom: 10
    }
  },
  noOffersContainer: {
    textAlign: "center",
    paddingTop: 60,
    paddingBottom: 40,
    paddingLeft: 40,
    paddingRight: 40
  },
  noOfferAddIcon: {
    fontSize: 60
  },
  noOffersTitle: {
    paddingBottom: 5
  },
  paginationContainer: {
    margin: "0 auto",
    paddingTop: 15
  },
  paginationItem: {
    color: "black",
    float: "left",
    padding: "8px 16px",
    textDecoration: "none"
  }
}); // eslint-disable-line

export default styles;
