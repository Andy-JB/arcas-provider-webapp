// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer, inject } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import { SupplierOfferCard } from "common/cards";
import CardViewTemplate from "common/layouts/CardViewTemplate";
import { Grid } from "@material-ui/core";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any
};

@inject("AuthStore", "UserStore")
@observer
class PublicOffersView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const { supplierObjects, didViewOffer } = controller;
    const cards = supplierObjects.map(offer => (
      <SupplierOfferCard
        {...offer}
        key={offer.id}
        onClick={() => didViewOffer(offer.id)}
      />
    ));
    if (supplierObjects.length > 0) {
      return <CardViewTemplate cards={cards} cardsPerPage={5} disableDrawer />;
    }
    return (
      <Grid container className={classes.noOffersContainer}>
        <Grid item xs={12}>
          <Typography
            variant="h5"
            component="p"
            className={classes.noOffersTitle}
          >
            You have no current offers. Click &quot;Add New Offer&quot; to get
            started.
          </Typography>
          <Typography variant="body1">
            Offers that you create will be available to other service providers.
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(PublicOffersView);
