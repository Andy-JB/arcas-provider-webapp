// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import EmailIcon from "@material-ui/icons/EmailOutlined";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Divider from "@material-ui/core/Divider";
import AuthLayout from "common/layouts/AuthLayout";
import { FadeInUpAnimation, FadeInAnimation } from "common/animations";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any
};

@observer
class ForgotPasswordView extends Component<Props> {
  render() {
    const { classes, controller, form } = this.props;
    const { isSendingEmail } = controller;
    const email = form.$("email");
    const errors = form.errors();
    return (
      <AuthLayout>
        <CssBaseline />
        <Grid container>
          <FadeInAnimation>
            <div className={classes.formHeaderContainer}>
              <Typography
                className={classes.formHeaderTitle}
                variant="h6"
                component="h1"
              >
                Forgot your password?
              </Typography>
              <Typography variant="subtitle1">
                Reset it using this form
              </Typography>
            </div>
          </FadeInAnimation>
          <form className={classes.formContainer} onSubmit={form.onSubmit}>
            <FadeInUpAnimation>
              <TextField
                {...email.bind()}
                autoComplete="email"
                variant="outlined"
                margin="normal"
                helperText={errors.email}
                autoFocus
                error={errors.email != null}
                fullWidth
                label={
                  <InputAdornment
                    className={classes.inputAdornmentContainer}
                    position="start"
                  >
                    <EmailIcon />
                    <p className={classes.inputAdornmentLabel}>Email</p>
                  </InputAdornment>
                }
              />
            </FadeInUpAnimation>
            <FadeInAnimation>
              <div className="submitWrapper">
                <div className={classes.buttonWrapper}>
                  {isSendingEmail && (
                    <CircularProgress
                      size={22}
                      className={classes.buttonProgress}
                    />
                  )}
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    disabled={isSendingEmail}
                    color="primary"
                    className={classes.submitButton}
                  >
                    Reset Password
                  </Button>
                </div>
                <Divider className={classes.divider} />
                <Typography
                  variant="caption"
                  className={classes.centerTextTopPadding}
                >
                  <Link to="/login" className={classes.textLinkPrimary}>
                    Return to login
                  </Link>
                </Typography>
              </div>
            </FadeInAnimation>
          </form>
        </Grid>
      </AuthLayout>
    );
  }
}

export default withStyles(styles)(ForgotPasswordView);
