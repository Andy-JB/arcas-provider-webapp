// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { inject, observer } from "mobx-react";
import history from "appHistory";
import MobxForm from "util/MobxForm";
import LoginView from "./LoginView";

type Props = {
  AuthStore: any
};

@inject("AuthStore")
@observer
class LoginScreen extends Component<Props> {
  @observable
  hidePasswordText: boolean;

  @observable
  signInFailed: boolean = false;

  constructor(props: Object) {
    super(props);
    this.hidePasswordText = true;
  }

  render() {
    return (
      <LoginView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = async (form: any) => {
    const { AuthStore } = this.props;
    const { email, password } = form.values();
    try {
      await AuthStore.login(email, password);
    } catch (e) {
      this.signInFailed = true;
    }
  };

  @action
  onError = () => {};

  @action
  handleSnackbarClose = () => {
    this.signInFailed = false;
  };

  @action
  handleRegister = (e: Object) => {
    e.preventDefault();
    history.push("/signup");
  };

  @action
  handleForgotPassword = (e: Object) => {
    e.preventDefault();
    history.push("/forgotpassword");
  };

  @action
  handlePasswordClick = () => {
    this.hidePasswordText = !this.hidePasswordText;
  };

  @action
  handlePasswordMouseDown = (e: Object) => {
    e.preventDefault();
  };

  @computed
  get isSigningIn(): boolean {
    return this.props.AuthStore.isLoading;
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "email",
        type: "email",
        label: "Email",
        rules: "required|email|string"
      },
      {
        name: "password",
        type: "password",
        label: "Password",
        rules: "required|string|between:5,25"
      }
    ];
  }
}

export default LoginScreen;
