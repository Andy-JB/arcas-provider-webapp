// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Snackbar from "@material-ui/core/Snackbar";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import EmailIcon from "@material-ui/icons/EmailOutlined";
import PasswordIcon from "@material-ui/icons/LockOutlined";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import AuthLayout from "common/layouts/AuthLayout";
import SnackbarContentWrapper from "common/components/SnackBarContentWrapper";
import { FadeInUpAnimation, FadeInAnimation } from "common/animations";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any
};

@observer
class LoginView extends Component<Props> {
  @observable
  hasAnimatedOnEntry: boolean;

  @observable
  willAnimateView: boolean = false;

  constructor(props: Object) {
    super(props);
    this.setupAnimation();
  }

  setupAnimation() {
    // Trigger blank animation with a 0 stagger for all child elements
    setTimeout(() => {
      this.willAnimateView = !this.willAnimateView;
    }, 0);
  }

  render() {
    const { classes, controller, form } = this.props;
    const {
      isSigningIn,
      handlePasswordClick,
      hidePasswordText,
      handleSnackbarClose,
      signInFailed,
      handlePasswordMouseDown
    } = controller;
    const email = form.$("email");
    const password = form.$("password");
    const type = null;
    const errors = form.errors();
    return (
      <AuthLayout>
        <CssBaseline />
        <Grid container>
          <FadeInAnimation>
            <div className={classes.formHeaderContainer}>
              <FadeInAnimation>
                <Typography
                  className={classes.formHeaderTitle}
                  variant="h6"
                  component="h1"
                >
                  Hey there! Welcome back.
                </Typography>
              </FadeInAnimation>
              <FadeInAnimation>
                <Typography variant="subtitle1">
                  Enter your login details below
                </Typography>
              </FadeInAnimation>
            </div>

            <form className={classes.formContainer} onSubmit={form.onSubmit}>
              <FadeInUpAnimation>
                <TextField
                  {...email.bind({ type })}
                  autoComplete="email"
                  variant="outlined"
                  margin="normal"
                  helperText={errors.email}
                  autoFocus
                  error={errors.email != null}
                  fullWidth
                  label={
                    <InputAdornment
                      className={classes.inputAdornmentContainer}
                      position="start"
                    >
                      <EmailIcon />
                      <p className={classes.inputAdornmentLabel}>Email</p>
                    </InputAdornment>
                  }
                />
                <TextField
                  {...password.bind({ type })}
                  autoComplete="password"
                  margin="normal"
                  variant="outlined"
                  type={hidePasswordText ? "password" : "text"}
                  helperText={errors.password}
                  fullWidth
                  error={errors.password != null}
                  label={
                    <InputAdornment
                      className={classes.inputAdornmentContainer}
                      position="start"
                    >
                      <PasswordIcon />
                      <p className={classes.inputAdornmentLabel}>Password</p>
                    </InputAdornment>
                  }
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password visibility"
                          onClick={handlePasswordClick}
                          onMouseDown={handlePasswordMouseDown}
                        >
                          {hidePasswordText ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FadeInUpAnimation>

              <FadeInAnimation>
                <div className="submitWrapper">
                  <Typography
                    variant="caption"
                    className={`${classes.textLinkDark} ${classes.rightAlignText}`}
                  >
                    <Link to="/forgotpassword" className={classes.textLinkDark}>
                      Forgot Password?
                    </Link>
                  </Typography>
                  <div className={classes.buttonWrapper}>
                    {isSigningIn && (
                      <CircularProgress
                        size={22}
                        className={classes.buttonProgress}
                      />
                    )}
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      disabled={isSigningIn}
                      className={classes.submitButton}
                    >
                      Sign in
                    </Button>
                  </div>

                  <Divider className={classes.divider} />
                  <Typography
                    variant="caption"
                    className={classes.signUpDescriptionText}
                  >
                    {"Don't have an account? "}
                    <Link to="/signup" className={classes.textLinkPrimary}>
                      Sign up.
                    </Link>
                  </Typography>
                </div>
              </FadeInAnimation>
            </form>
          </FadeInAnimation>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left"
            }}
            open={signInFailed}
            autoHideDuration={6000}
            onClose={handleSnackbarClose}
          >
            <SnackbarContentWrapper
              onClose={handleSnackbarClose}
              variant="error"
              message="Invalid email or password. Please try again."
            />
          </Snackbar>
        </Grid>
      </AuthLayout>
    );
  }
}

export default withStyles(styles)(LoginView);
