// @flow
import commonStyles from "features/authentication/styles/authStyles";

const styles = (theme: any) => ({
  ...commonStyles(theme),
  signUpDescriptionText: {
    marginTop: 20,
    textAlign: "center"
  }
});

export default styles;
