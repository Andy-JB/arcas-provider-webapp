// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import queryString from "querystringify";
import history from "appHistory";
import MobxForm from "util/MobxForm";
import SignupFormView from "./SignupView";
import SignupFormInviteView from "./SignupInviteView";

type Props = {
  AuthStore: any,
  UserStore: any,
  location: any
};

@inject("AuthStore", "UserStore")
@observer
class SignupScreen extends Component<Props> {
  @observable
  signUpFailed: boolean = false;

  @observable
  errors = null;

  @observable
  isSigningUpRequest: boolean = false;

  render() {
    const parsed = queryString.parse(this.props.location.search);
    const { email } = parsed;
    const { fname } = parsed;
    const { lname } = parsed;

    if (email) {
      return (
        <SignupFormInviteView
          controller={this}
          form={
            new MobxForm(
              this.formFieldsInvite,
              this.onInviteSuccess,
              this.onError
            )
          }
          invitedemail={email}
          invitedfname={fname}
          invitedlname={lname}
        />
      );
    }
    return (
      <SignupFormView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = async (form: any) => {
    const { AuthStore, UserStore } = this.props;
    const { email, password } = form.values();
    this.isSigningUpRequest = true;
    try {
      await AuthStore.signup(email, password);
      await UserStore.saveOrganization(email, form.values());
      this.isSigningUpRequest = false;
    } catch (e) {
      this.errors = e;
      this.signUpFailed = true;
      this.isSigningUpRequest = false;
    }
  };

  @action
  onInviteSuccess = (form: Object) => {
    const { AuthStore } = this.props;
    const { email, password } = form.values();
    this.isSigningUpRequest = true;
    AuthStore.signup(email, password)
      .then(() => {
        this.navigateToDashboardIfLoggedIn();
      })
      .catch(err => {
        this.errors = err;
        this.signUpFailed = true;
      })
      .finally(() => {
        this.isSigningUpRequest = false;
      });
  };

  @action
  navigateToDashboardIfLoggedIn() {
    if (this.props.AuthStore.isAuthenticated) {
      history.push("/dashboard");
    }
  }

  @action
  onError = () => {};

  @action
  handleSnackbarClose = (): void => {
    this.signUpFailed = false;
  };

  @computed
  get isSigningUp(): boolean {
    return this.isSigningUpRequest;
  }

  @computed
  get errorMessage(): string {
    if (this.errors != null) {
      if (this.errors.code === "auth/email-already-in-use") {
        return this.errors.message;
      }
      return "Something went wrong. Please try again.";
    }
    return "";
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "name",
        type: "text",
        label: "Organisation Name",
        rules: "required|string|max:60"
      },
      {
        name: "email",
        type: "email",
        label: "Email",
        rules: "required|email|string|max:100"
      },
      {
        name: "password",
        type: "password",
        label: "Password",
        rules: "required|string|between:5,25"
      },
      {
        name: "passwordConfirm",
        type: "password",
        label: "Confirm Password",
        rules: "required|string|same:password"
      },
      {
        name: "phone",
        type: "tel",
        label: "Phone",
        rules: "required|numeric|digits:10"
      },
      {
        name: "abn",
        type: "text",
        label: "ABN",
        rules: "required|numeric|digits:11"
      },
      {
        name: "firstName",
        type: "text",
        label: "First Name",
        rules: "required|string|max:60"
      },
      {
        name: "lastName",
        type: "text",
        label: "Last Name",
        rules: "required|string|max:60"
      }
    ];
  }

  @computed
  get formFieldsInvite(): Array<Object> {
    return [
      {
        name: "email",
        type: "email",
        label: "Email",
        rules: "required|email|string"
      },
      {
        name: "password",
        type: "password",
        label: "Password",
        rules: "required|string|between:5,25"
      },
      {
        name: "passwordConfirm",
        type: "password",
        label: "Confirm Password",
        rules: "required|string|same:password"
      },
      {
        name: "firstName",
        type: "text",
        label: "First Name",
        rules: "required|string"
      },
      {
        name: "lastName",
        type: "text",
        label: "Last Name",
        rules: "required|string"
      }
    ];
  }
}

export default SignupScreen;
