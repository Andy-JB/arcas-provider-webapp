// @flow
import commonStyles from "features/authentication/styles/authStyles";

const styles = (theme: any) => ({
  ...commonStyles(theme)
});

export default styles;
