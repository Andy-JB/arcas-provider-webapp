// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { inject, observer } from "mobx-react";
import queryString from "querystringify";
import MobxForm from "util/MobxForm";
import ResetPasswordView from "./ResetPasswordView";

type Props = {
  AuthStore: any,
  location: any
};

@inject("AuthStore")
@observer
class ResetPasswordScreen extends Component<Props> {
  @observable
  resetPasswordFailed: boolean;

  @observable
  passwordResetSuccessful: boolean;

  @observable
  resetPasswordToken: string;

  constructor(props: Object) {
    super(props);
    this.resetPasswordFailed = false;
    this.passwordResetSuccessful = false;
  }

  componentWillMount() {
    const parsed = queryString.parse(this.props.location.search);
    this.resetPasswordToken = parsed.oobCode;
  }

  render() {
    return (
      <ResetPasswordView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: Object) => {
    const { AuthStore } = this.props;
    const { password } = form.values();
    this.resetPasswordFailed = false;
    AuthStore.verifyPasswordResetCode(this.resetPasswordToken)
      .then(() => {
        this.updatePassword(password);
      })
      .catch(() => {
        this.resetPasswordFailed = true;
      });
  };

  @action
  updatePassword = (newPassword: string) => {
    this.props.AuthStore.resetPassword(this.resetPasswordToken, newPassword)
      .then(() => {
        this.passwordResetSuccessful = true;
      })
      .catch(() => {
        this.resetPasswordFailed = true;
      });
  };

  @action
  onError = () => {};

  handleSnackbarClose = () => {
    this.passwordResetSuccessful = false;
    this.resetPasswordFailed = false;
  };

  @computed
  get isResettingPassword(): boolean {
    return this.props.AuthStore.isLoading;
  }

  @computed
  get snackbarMessage(): string {
    if (this.resetPasswordFailed) {
      return "Password reset failed. Please try again.";
    }
    return "Password reset successful. Please login with new password.";
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "password",
        type: "password",
        label: "New Password",
        rules: "required|string|between:5,25"
      },
      {
        name: "passwordConfirm",
        type: "password",
        label: "Confirm Password",
        rules: "required|string|same:password"
      }
    ];
  }
}

export default ResetPasswordScreen;
