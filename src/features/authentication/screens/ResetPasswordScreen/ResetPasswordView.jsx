// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import CssBaseline from "@material-ui/core/CssBaseline";
import InputAdornment from "@material-ui/core/InputAdornment";
import Divider from "@material-ui/core/Divider";
import LockIcon from "@material-ui/icons/LockOutlined";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContentWrapper from "common/components/SnackBarContentWrapper";
import AuthLayout from "common/layouts/AuthLayout";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any
};

@observer
class ResetPasswordView extends Component<Props> {
  render() {
    const { classes, controller, form } = this.props;
    const {
      handleSnackbarClose,
      resetPasswordFailed,
      passwordResetSuccessful,
      isResettingPassword,
      snackbarMessage
    } = controller;
    const password = form.$("password");
    const confirmPassword = form.$("passwordConfirm");
    const type = null;
    const errors = form.errors();
    return (
      <AuthLayout>
        <CssBaseline />
        <Grid container>
          <div className={classes.formHeaderContainer}>
            <Typography className={classes.formHeaderTitle} variant="headline">
              Reset Password
            </Typography>
            <Typography variant="caption">
              Password must contain between 5 to 25 characters
            </Typography>
          </div>
        </Grid>
        <form className={classes.formContainer} onSubmit={form.onSubmit}>
          <TextField
            {...password.bind({ type })}
            margin="normal"
            variant="outlined"
            helperText={errors.password}
            error={errors.password != null}
            label={
              <InputAdornment
                className={classes.inputAdornmentContainer}
                position="start"
              >
                <LockIcon />
                <p className={classes.inputAdornmentLabel}>Password</p>
              </InputAdornment>
            }
            fullWidth
          />
          <TextField
            {...confirmPassword.bind({ type })}
            margin="normal"
            variant="outlined"
            helperText={errors.passwordConfirm}
            error={errors.passwordConfirm != null}
            label={
              <InputAdornment
                className={classes.inputAdornmentContainer}
                position="start"
              >
                <LockIcon />
                <p className={classes.inputAdornmentLabel}>Confirm Password</p>
              </InputAdornment>
            }
            fullWidth
          />
          <div className="submitWrapper">
            <div className={classes.buttonWrapper}>
              {isResettingPassword && (
                <CircularProgress
                  size={22}
                  className={classes.buttonProgress}
                />
              )}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                disabled={isResettingPassword}
                color="primary"
                className={classes.submitButton}
              >
                Reset Password
              </Button>
            </div>
          </div>
          <Divider className={classes.divider} />
          <Typography
            variant="caption"
            className={`${classes.textLinkDark} ${classes.centerText}`}
          >
            <Link to="/login" className={classes.textLinkDark}>
              Return to login
            </Link>
          </Typography>
        </form>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={resetPasswordFailed || passwordResetSuccessful}
          autoHideDuration={6000}
          onClose={handleSnackbarClose}
        >
          <SnackbarContentWrapper
            onClose={handleSnackbarClose}
            variant={resetPasswordFailed ? "error" : "success"}
            message={snackbarMessage}
          />
        </Snackbar>
      </AuthLayout>
    );
  }
}

export default withStyles(styles)(ResetPasswordView);
