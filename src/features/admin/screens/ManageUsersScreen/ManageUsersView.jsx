// @flow
import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import CloseIcon from "@material-ui/icons/Close";
import withStyles from "@material-ui/core/styles/withStyles";
import MaterialTable from "material-table";
import { CSVLink } from "react-csv";
import LoadingScreen from "common/screens/LoadingScreen";
import { observable, action, computed } from "mobx";
import AlertDialog from "common/dialogs/AlertDialog";
import CreateUserFormDialog from "./CreateUserFormDialog";
import styles from "./styles";

type Props = {
  UserStore: any,
  users: any,
  classes: any,
  controller: any,
  form: any,
  deleteRoles: any,
  currentorg: any
};

const csvHeaders = [
  { label: "Name", key: "fullName" },
  { label: "Email", key: "email" },
  { label: "Organisation", key: "orgName" },
  { label: "Onboarded", key: "isOnboarded" },
  { label: "Role", key: "roleType" }
];

const csvFileName = "arcas-users.csv";

@inject("UserStore")
@observer
class ManageUsersView extends Component<Props> {
  @observable
  userList;

  @observable
  currentUser;

  @observable
  openDialog = {
    open: false,
    recordID: "",
    rowEnable: false
  };

  @observable
  openSnackbar = {
    open: false,
    message: ""
  };

  @observable
  selectRowID;

  render() {
    const {
      users,
      classes,
      currentorg,
      controller,
      form,
      deleteRoles
    } = this.props;
    const { createUser } = controller;

    return (
      <div className={classes.container}>
        <Grid container>
          {this.props.UserStore.isAuthorised(deleteRoles, false) ? (
            <div>
              <CSVLink filename={csvFileName} data={users} headers={csvHeaders}>
                <Button variant="outlined" className={classes.exportButton}>
                  Export Data
                </Button>
              </CSVLink>
              <Button
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "left"
                }}
                className={classes.fab}
                variant="fab"
                color="secondary"
                aria-label="Add"
                onClick={() => this.handleCreateUser()}
              >
                <AddIcon />
              </Button>
            </div>
          ) : (
            <div />
          )}
          <Grid item xs={12}>
            {this.userList && this.userList.length > 0 ? (
              <div>
                <MaterialTable
                  className={classes.table}
                  columns={[
                    { title: "Email", field: "email" },
                    { title: "Name", field: "fullName" },
                    { title: "Organisation", field: "orgName" },
                    {
                      title: "Onboarded",
                      field: "isOnboarded",
                      type: "boolean"
                    },
                    { title: "Role", field: "roleType" }
                  ]}
                  controller={controller}
                  data={users}
                  title=""
                  options={{
                    columnsButton: false,
                    filtering: true
                  }}
                  actions={[
                    {
                      icon: "edit_icon",
                      tooltip: "Edit user",
                      onClick: (event, rowData) => {
                        this.handleEditUser(rowData);
                      }
                    },
                    rowData =>
                      rowData.enabled
                        ? {
                            icon: "delete_icon",
                            tooltip: "Archives user",
                            onClick: (event, data) => {
                              this.handleArchiveUser(data.id);
                            }
                          }
                        : {
                            icon: "restore_from_trash",
                            tooltip: "Restores user",
                            onClick: (event, data) => {
                              this.handleRestoreUser(data.id);
                            }
                          }
                  ]}
                />
              </div>
            ) : (
              <LoadingScreen />
            )}
          </Grid>
        </Grid>

        {createUser.open ? (
          <CreateUserFormDialog
            form={form}
            open={createUser.open}
            controller={controller}
            currentorg={currentorg}
            user={this.currentUser}
          />
        ) : (
          <div />
        )}
        <AlertDialog
          open={this.openDialog.open}
          description="Are you sure?"
          title="Alert"
          onPositive={this.handlePositiveButton}
          onNegative={this.handleNegativeButton}
        />
        <Snackbar
          open={this.openSnackbar.open}
          autoHideDuration={2000}
          onClose={this.handleCloseSnackbar}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          aria-describedby="client-snackbar"
          message={
            <span id="client-snackbar" className={classes.message}>
              <Icon />
              {this.openSnackbar.message}
            </span>
          }
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleCloseSnackbar}
            >
              <CloseIcon className={classes.icon} />
            </IconButton>
          ]}
        />
      </div>
    );
  }

  @action
  handlePositiveButton = () => {
    this.openDialog.open = false;
    if (this.openDialog.rowEnable) {
      this.props.UserStore.archiveUser(this.openDialog.recordID);
      this.openSnackbar.message = "User Archived";
      this.openSnackbar.open = true;
    } else {
      this.props.UserStore.restoreUser(this.openDialog.recordID);
      this.openSnackbar.message = "User Restored";
      this.openSnackbar.open = true;
    }
  };

  @action
  handleCloseSnackbar = () => {
    this.openSnackbar.open = false;
  };

  @action
  handleNegativeButton = () => {
    this.openDialog.open = false;
  };

  @action
  handleCreateUser = () => {
    this.currentUser = undefined;
    this.props.controller.handleCreateUser();
  };

  @action
  handleEditUser = user => {
    this.currentUser = user;
    this.props.controller.createUser.newUser = false;
    this.props.controller.createUser.open = true;
  };

  @action
  handleArchiveUser = (id: string) => {
    this.openDialog.open = true;
    this.openDialog.rowEnable = true;
    this.openDialog.recordID = id;
  };

  @action
  handleRestoreUser = id => {
    this.openDialog.open = true;
    this.openDialog.rowEnable = false;
    this.openDialog.recordID = id;
  };

  @computed
  get fName() {
    if (this.currentUser) {
      return this.currentUser.firstName;
    }
    return undefined;
  }
}

export default withStyles(styles)(ManageUsersView);
