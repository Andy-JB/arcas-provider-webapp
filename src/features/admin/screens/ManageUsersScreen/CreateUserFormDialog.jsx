// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
// import Checkbox from "@material-ui/core/Checkbox";
import MenuItem from "@material-ui/core/MenuItem";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import withStyles from "@material-ui/core/styles/withStyles";
import { action } from "mobx";
import styles from "./styles";

type Props = {
  controller: any,
  form: any,
  user: Object,
  open: boolean
};

@observer
class CreateUserFormDialog extends Component<Props> {
  constructor(props: Object) {
    super(props);
    if (this.props.user !== undefined) {
      this.props.form.$("firstName").value = this.props.user.firstName;
      this.props.form.$("lastName").value = this.props.user.lastName;
      this.props.form.$("email").value = this.props.user.email;
      this.props.form.$("organisation").value = this.props.user.orgName;
      this.props.form.$("role").value = this.props.user.role.id;
    } else {
      this.props.form.$("firstName").value = "";
      this.props.form.$("lastName").value = "";
      this.props.form.$("email").value = "";
    }
  }

  @action
  handleChangeRole = () => event => {
    this.props.form.$("role").value = event.target.value;
  };

  render() {
    const { controller, form, open } = this.props;
    const email = form.$("email");
    const firstName = form.$("firstName");
    const lastName = form.$("lastName");
    const organisation = form.$("organisation");
    const role = form.$("role");
    const type = null;
    const errors = form.errors();

    return (
      <div>
        <Dialog
          open={open}
          onClose={() => controller.handleDialogClose()}
          aria-labelledby="form-dialog-title"
          fullWidth
          maxWidth="md"
        >
          <DialogTitle id="form-dialog-title">Invite User</DialogTitle>
          <form onSubmit={form.onSubmit}>
            <DialogContent>
              <DialogContentText>
                To invite a new user, fill in the details below.
              </DialogContentText>
              <br />
              <br />
              <TextField
                {...email.bind({ type })}
                autoFocus
                margin="dense"
                helperText={errors.email}
                error={errors.email != null}
                fullWidth
              />
              <TextField
                {...firstName.bind({ type })}
                margin="normal"
                helperText={errors.firstName}
                error={errors.firstName != null}
                fullWidth
              />
              <br />
              <br />
              <TextField
                {...lastName.bind({ type })}
                margin="dense"
                autoComplete="lastName"
                helperText={errors.lastName}
                error={errors.lastName != null}
                fullWidth
              />
              <br />
              <br />
              <TextField
                {...organisation.bind({ type })}
                margin="dense"
                disabled
                fullWidth
              />
              <br />
              <br />
              <TextField
                {...role.bind({ type })}
                select
                margin="dense"
                onChange={this.handleChangeRole()}
                helperText="Please select user role"
              >
                {controller.userRoles.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
              <br />
              <br />
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => controller.handleDialogClose()}
                color="primary"
              >
                Cancel
              </Button>
              <Button type="submit" color="primary">
                {this.props.user !== undefined ? "Save" : "Invite"}
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(CreateUserFormDialog);
