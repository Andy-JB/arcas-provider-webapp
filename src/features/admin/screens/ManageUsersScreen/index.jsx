// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { action, observable, computed } from "mobx";
import withStyles from "@material-ui/core/styles/withStyles";
import LoadingScreen from "common/screens/LoadingScreen";
import MobxForm from "util/MobxForm";
import LockIcon from "@material-ui/icons/LockOutlined";
import DashboardLayout from "common/layouts/DashboardLayout";
import ManageUsersView from "./ManageUsersView";
import styles from "./styles";

const roles = [
  {
    value: "super",
    label: "Super Admin"
  },
  {
    value: "orgadmin",
    label: "Organisation Admin"
  },
  {
    value: "orguser",
    label: "Organisation User"
  },
  {
    value: "appuser",
    label: "App User"
  }
];

type Props = {
  UserStore: any,
  AuthStore: any,
  classes: any
};

@inject("AuthStore", "UserStore")
@observer
class ManageUsersScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  createUser = {
    open: false,
    newUser: false
  };

  @observable
  signUpFailed: boolean = false;

  @observable
  users;

  componentDidMount() {
    this.users = this.props.UserStore.users;
  }

  render() {
    const { classes } = this.props;
    return (
      <DashboardLayout pageTitle="Manage Users" headerIcon={LockIcon}>
        {!this.users ? (
          <LoadingScreen />
        ) : (
          <div className={classes.root}>
            <ManageUsersView
              form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
              controller={this}
              deleteRoles={["super"]}
              currentorg={this.props.UserStore.currentOrganisation}
              users={this.users}
              handleCreateUser={this.handleCreateUser}
            />
          </div>
        )}
      </DashboardLayout>
    );
  }

  @action
  onSuccess = (form: any) => {
    const o = form.values();
    if (this.createUser.newUser) {
      this.props.AuthStore.inviteUser(o.email, o.firstName, o.lastName);
    }
    this.props.UserStore.saveUser(
      o.email,
      this.props.UserStore.currentOrganisation.id,
      o,
      o.role
    );
    this.createUser.open = false;
    this.createUser.newUser = false;
  };

  @action
  onError = () => {};

  @action
  handleCreateUser = (): void => {
    this.createUser.open = true;
    this.createUser.newUser = true;
  };

  @action
  handleDeleteUser = (id: string): void => {
    this.props.UserStore.delete(id);
  };

  @action
  handleSnackbarClose = (): void => {
    this.signUpFailed = false;
  };

  @action
  handleDialogClose = (): void => {
    this.createUser.open = false;
    this.createUser.newUser = false;
  };

  @computed
  get userRoles() {
    return roles;
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "email",
        type: "text",
        label: "Email",
        placeholder: "Enter the email address",
        rules: "required|string|between:5,25"
      },
      {
        name: "firstName",
        type: "text",
        label: "First Name",
        placeholder: "First Name",
        rules: "string|between:0,100"
      },
      {
        name: "lastName",
        type: "text",
        label: "Last Name",
        placeholder: "Last Name",
        rules: "string|between:0,100"
      },
      {
        name: "organisation",
        type: "text",
        label: "Organisation",
        placeholder: "Organisation Name"
      },
      {
        name: "role",
        type: "text",
        label: "Role",
        value: "orgadmin",
        placeholder: "Select Role"
      },
      {
        name: "isOnboarded",
        type: "text",
        label: "Onboarded",
        placeholder: "Has user been onboarded?",
        checked: false
      }
    ];
  }
}

export default withStyles(styles)(ManageUsersScreen);
