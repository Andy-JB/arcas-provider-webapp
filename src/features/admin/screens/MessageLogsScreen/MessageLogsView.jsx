// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import withStyles from "@material-ui/core/styles/withStyles";

import styles from "./styles";

type Props = {
  classes: any,
  messages: any
};

@observer
class ManageUsersView extends Component<Props> {
  render() {
    const { classes, messages } = this.props;

    return (
      <div className={classes.container}>
        <Grid container>
          <Grid item xs={12}>
            <Paper>
              <div className={classes.tableWrapper}>
                <Table className={classes.table}>
                  <TableHead>
                    <TableRow>
                      <TableCell>Title</TableCell>
                      <TableCell>Description</TableCell>
                      <TableCell>Organisation</TableCell>
                      <TableCell>IP</TableCell>
                      <TableCell>Timestamp</TableCell>
                      <TableCell>Who</TableCell>
                      <TableCell>User Agent</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {messages &&
                      messages.map(topic =>
                        topic ? (
                          <TableRow key={topic.id}>
                            <TableCell>{topic.title}</TableCell>
                            <TableCell>{topic.description}</TableCell>
                            <TableCell>Unknown</TableCell>
                            <TableCell>Unknown</TableCell>
                            <TableCell>Unknown</TableCell>
                            <TableCell>Unknown</TableCell>
                            <TableCell>{topic.userAgent}</TableCell>
                          </TableRow>
                        ) : (
                          <div />
                        )
                      )}
                  </TableBody>
                </Table>
              </div>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(ManageUsersView);
