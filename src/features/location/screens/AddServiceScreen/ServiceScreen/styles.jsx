// @flow
const styles = (theme: any) => ({
  buttons: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(3),
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  formControl: {
    margin: theme.spacing(3)
  },
  group: {
    margin: `${theme.spacing(1)}px 0`
  },
  serviceIcon: {
    marginRight: 10
  }
});
export default styles;
