// @flow
const styles = (theme: any) => ({
  layout: {
    paddingTop: 20,
    paddingBottom: 40,
    paddingRight: 80,
    paddingLeft: 80,
    "@media screen and (max-width:1150px) and (min-width:950px)": {
      paddingRight: 10,
      paddingLeft: 10
    },
    [theme.breakpoints.down("sm")]: {
      paddingRight: 20,
      paddingLeft: 20
    }
  },
  stepper: {
    padding: `${theme.spacing(3)}px 0 ${theme.spacing(5)}px`
  },
  summaryWrapper: {
    display: "flex",
    justifyContent: "center"
  },
  serviceListButton: {
    marginTop: 16,
    display: "flex",
    justifyContent: "center"
  },
  progressWrapper: {
    display: "flex",
    justifyContent: "center"
  },
  progress: {
    margin: theme.spacing(2)
  }
});

export default styles;
