// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import withStyles from "@material-ui/core/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Stepper from "@material-ui/core/Stepper";
import LoadingScreen from "common/screens/LoadingScreen";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any
};

@observer
class AddServiceView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const {
      activeStep,
      steps,
      getStepContent,
      isServiceCreating,
      handleCancelClose,
      handleCancelConfirm,
      showCancelDialog
    } = controller;
    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          {isServiceCreating ? <LoadingScreen /> : getStepContent(activeStep)}
          <Dialog
            open={showCancelDialog}
            onClose={handleCancelClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Any of the changes that you have made will be lost. Press
                &quot;Yes&quot; to cancel.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCancelClose} color="primary">
                No
              </Button>
              <Button onClick={handleCancelConfirm} color="primary" autoFocus>
                Yes
              </Button>
            </DialogActions>
          </Dialog>
        </main>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(AddServiceView);
