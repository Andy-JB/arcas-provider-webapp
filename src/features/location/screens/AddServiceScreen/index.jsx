// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import OpeningHoursScreen from "features/location/components/OpeningHoursView";
import DashboardOverlapLayout from "common/layouts/DashboardOverlapLayout";
import LocationIcon from "@material-ui/icons/LocationOn";
import history from "appHistory";
import AddServiceView from "./AddServiceView";
import ServiceScreen from "./ServiceScreen";

type Props = {
  LocationStore: any,
  RefStore: any
};

@inject("LocationStore", "RefStore")
@observer
class AddServiceScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  activeStep = 0;

  @observable
  steps = ["Service", "Opening hours"];

  @observable
  service = {
    category: null,
    age: [],
    gender: [],
    openingHours: {}
  };

  @observable
  isServiceCreating: boolean;

  @observable
  isLoading: boolean;

  @observable
  showCancelDialog: boolean;

  constructor(props: Props) {
    super(props);
    this.isLoading = false;
    this.isServiceCreating = false;
    this.showCancelDialog = false;
  }

  render() {
    return (
      <DashboardOverlapLayout pageTitle="Add Service" headerIcon={LocationIcon}>
        <AddServiceView controller={this} />
      </DashboardOverlapLayout>
    );
  }

  @action
  getStepContent = (step: any) => {
    switch (step) {
      case 0:
        return <ServiceScreen controller={this} />;
      case 1:
      default:
        return <OpeningHoursScreen controller={this} buttonTitle="Submit" />;
    }
  };

  @action
  handleCancelConfirm = () => {
    this.showCancelDialog = false;
    history.push("/locations/overview#services");
  };

  @action
  handleCancelClose = () => {
    this.showCancelDialog = false;
  };

  @action
  handleCancel = () => {
    this.showCancelDialog = true;
  };

  @action
  handleNext = () => {
    this.activeStep += 1;
  };

  @action
  handleBack = () => {
    if (this.activeStep > 0) {
      this.activeStep -= 1;
    }
  };

  @action
  addServiceCategory = (service: any) => {
    this.service.category = service.category;
    this.service.age = service.age;
    this.service.gender = service.gender;
  };

  @action
  addOpeningHours = (openingHours: any) => {
    this.service.openingHours = openingHours;
    this.handleAddService();
  };

  @action
  handleAddService = () => {
    const { LocationStore } = this.props;
    this.isServiceCreating = true;
    const newService = this.service;
    if (newService.age == null || newService.age.length === 0) {
      newService.age = this.ages;
    }
    if (newService.gender == null || newService.gender.length === 0) {
      newService.gender = this.genders;
    }
    newService.age = this.getAgeIds(newService.age);
    newService.gender = this.getGenderIds(newService.gender);
    LocationStore.createOrUpdateService(this.service)
      .then(() => {
        this.isServiceCreating = false;
        history.push("/locations/overview/#services");
      })
      .catch(() => {
        this.isServiceCreating = false;
      });
  };

  @action
  onServiceListClicked = () => {
    history.push("/locations/overview/#services");
  };

  @action
  getAgeIds = (ages: any): Array<string> => {
    const ageIds = [];
    ages.map(age =>
      ageIds.push(
        (this.ageReference.find(ageRef => ageRef.name === age): any).id
      )
    );
    return ageIds;
  };

  @action
  getGenderIds = (genders: any): Array<string> => {
    const genderIds = [];
    genders.map(gender =>
      genderIds.push(
        (this.genderReference.find(genderRef => genderRef.name === gender): any)
          .id
      )
    );
    return genderIds;
  };

  @computed
  get openingHours() {
    return this.props.LocationStore.openingHours;
  }

  @computed
  get ageReference(): Array<any> {
    const { ageRefs } = this.props.RefStore;
    return ageRefs;
  }

  @computed
  get genderReference(): Array<any> {
    const { genderRefs } = this.props.RefStore;
    return genderRefs;
  }

  @computed
  get ages(): Array<string> {
    const ageList = [];
    this.ageReference.map(age => ageList.push(age.name));
    return ageList;
  }

  @computed
  get genders(): Array<string> {
    const genderList = [];
    this.genderReference.map(gender => genderList.push(gender.name));
    return genderList;
  }
}

export default AddServiceScreen;
