// @flow
import addOfferStyles from "styles/addOfferStyles";

const styles = (theme: any) => addOfferStyles(theme);

export default styles;
