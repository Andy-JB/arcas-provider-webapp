// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import LoadingScreen from "common/screens/LoadingScreen";
import { computed, observable } from "mobx";
import styles from "./styles";
import FeedbackView from "./FeedbackView";

type Props = {
  LocationStore: any
};

@inject("AuthStore", "UserStore", "LocationStore")
@observer
class FeedbackScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  feedbackData: Array<any>;

  constructor(props: Object) {
    super(props);
    const { LocationStore } = this.props;
    LocationStore.getAllFeedback().then(documents => {
      this.feedbackData = documents;
    });
  }

  render() {
    // TODO show loading
    return !this.feedbackData ? (
      <LoadingScreen />
    ) : (
      <FeedbackView controller={this} />
    );
  }

  @computed
  get overallRating(): number | string {
    if (this.feedbackData) {
      const feedbackCount = this.feedbackData.length;
      const totalFeedback = this.feedbackData.reduce(
        (accumulator, feedback) => accumulator + feedback.rating,
        0
      );
      return (totalFeedback / feedbackCount).toFixed(1);
    }
    return "N/A";
  }

  @computed
  get feedbackCount(): number {
    return this.cardData.length;
  }

  @computed
  get cardData(): Array<any> {
    if (this.feedbackData) {
      return this.feedbackData;
    }
    return [];
  }
}

export default withStyles(styles)(FeedbackScreen);
