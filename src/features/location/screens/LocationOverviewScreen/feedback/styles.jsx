// @flow
const styles = (theme: any) => ({
  headerDescription: {
    padding: 20,
    [theme.breakpoints.up("md")]: {
      paddingLeft: 50
    }
  },
  statsContainer: {
    padding: 20,
    [theme.breakpoints.up("md")]: {
      paddingTop: 20,
      paddingBottom: 20,
      paddingLeft: 50,
      paddingRight: 50
    }
  },
  feedbackReceivedTile: {
    padding: 20,
    height: 130
  },
  scoreTile: {
    padding: 20,
    height: 130
  },
  noFeedbackContainer: {
    textAlign: "center",
    paddingTop: 60,
    paddingBottom: 40,
    paddingLeft: 40,
    paddingRight: 40
  },
  noFeedbackTitle: {
    paddingBottom: 5
  }
});

export default styles;
