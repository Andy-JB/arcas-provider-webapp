// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer } from "mobx-react";
import { Grid, Typography } from "@material-ui/core";
import AnnouncementCard from "features/location/components/AnnouncementCard";
import styles from "./styles";

type Props = {
  classes: any,
  controller: any
};

@observer
class AnnouncementView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const { announcements, onEdit, onDelete } = controller;
    if (announcements.length > 0) {
      return (
        <Grid container spacing={2} className={classes.root}>
          <Grid item xs={12}>
            <Typography
              className={classes.pageDescription}
              variant="h6"
              component="h2"
            >
              Got something to say? Let users of the Arcas mobile app know!
            </Typography>
          </Grid>

          {announcements.map(x => (
            <Grid key={x.id} item xs={12} sm={4} xl={4}>
              <AnnouncementCard
                title={x.title}
                description={x.description}
                eventDate={x.eventDate}
                onEdit={() => onEdit(x.id)}
                onDelete={() => onDelete(x.id)}
              />
            </Grid>
          ))}
        </Grid>
      );
    }
    return (
      <Grid container className={classes.noOffersContainer}>
        <Grid item xs={12}>
          <Typography
            variant="h5"
            component="p"
            className={classes.noOffersTitle}
          >
            Click &quot;Add An Announcement&quot; to get started.
          </Typography>
          <Typography variant="body1">
            Announcements that you create will be available to users of the
            Arcas mobile app.
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(AnnouncementView);
