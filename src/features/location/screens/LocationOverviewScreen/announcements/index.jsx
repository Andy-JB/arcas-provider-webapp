// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import { computed, observable } from "mobx";
import LoadingScreen from "common/screens/LoadingScreen";
import moment from "moment";
import history from "appHistory";
import AnnouncementView from "./AnnouncementView";
import styles from "./styles";

type Props = {
  LocationStore: any,
  UiEventStore: any
};

@inject("AuthStore", "UiEventStore", "UserStore", "LocationStore")
@observer
class AnnouncementScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  announcements: Array<any> = null;

  constructor(props: Object) {
    super(props);
    this.retrieveAnnouncements();
  }

  render() {
    if (this.isLoading) {
      return <LoadingScreen />;
    }
    return <AnnouncementView controller={this} />;
  }

  @computed
  get isLoading(): boolean {
    const { LocationStore } = this.props;
    return this.announcements == null || LocationStore.isUpdatingData;
  }

  retrieveAnnouncements = () => {
    this.props.LocationStore.getAnnouncements().then(announcements => {
      this.announcements = announcements.map(x => {
        return {
          id: x.id,
          title: x.title,
          description: x.description,
          location: x.location,
          avatar: x.orgAvatar,
          subtitle: x.orgName,
          eventDate: moment(x.eventDate.toDate()).format("DD MMMM YYYY")
        };
      });
    });
  };

  onEdit = (id: string) => {
    history.push(`/locations/announcements/edit/${id}`);
  };

  onDelete = (id: string) => {
    this.props.UiEventStore.alertDialog.onOpen({
      key: 1,
      title: "Remove this announcement",
      description: `Are you sure you want to remove this announcement?`,
      onPositiveButtonClicked: () => {
        this.onDeleteConfirmed(id);
      }
    });
  };

  onDeleteConfirmed = (id: string) => {
    this.props.LocationStore.removeAnnouncement(id);
    this.retrieveAnnouncements();
  };
}

export default withStyles(styles)(AnnouncementScreen);
