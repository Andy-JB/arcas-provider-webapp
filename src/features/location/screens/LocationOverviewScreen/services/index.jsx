// @flow
import React, { Component } from "react";
import { action, observable, computed, autorun } from "mobx";
import { observer, inject } from "mobx-react";
import LocationServicesView from "./LocationServicesView";

type Props = {
  LocationStore: any,
  RefStore: any,
  UiEventStore: any
};

@inject("LocationStore", "RefStore", "UiEventStore")
@observer
class LocationServicesScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];

  @observable
  serviceRef: Array<{ anchorEl: any, expanded: boolean }> = [];

  @observable
  services: Array<any> = [];

  @observable
  selectedServiceForEdit: ?Object;

  @observable
  openDialogTitle: string;

  @observable
  disposer;

  constructor(props: Object) {
    super(props);
    this.openDialogTitle = "";
    this.selectedServiceForEdit = null;
    this.disposer = autorun(() => {
      this.services = this.props.LocationStore.services
        .map(service => {
          const initialValue = this.services.find(x => service.id === x.id);
          if (initialValue) {
            return {
              ...initialValue,
              ...service
            };
          }
          return {
            ...service
          };
        })
        .sort((a, b) => (a.category > b.category ? 1 : -1));
    });
  }

  componentWillUnmount() {
    this.disposer();
  }

  render() {
    return <LocationServicesView controller={this} />;
  }

  @action
  openEditDialog = (dialogName: string, service: any) => {
    this.openDialogTitle = dialogName;
    this.selectedServiceForEdit = service;
  };

  @computed
  get selectedServiceId() {
    if (this.selectedServiceForEdit) {
      return this.selectedServiceForEdit.id;
    }
    return null;
  }

  @computed
  get currentServiceCategoryName(): string {
    if (this.selectedServiceForEdit) {
      return this.getServiceCategoryName(this.selectedServiceForEdit.category);
    }
    return "";
  }

  @computed
  get currentServiceCategoryId(): string {
    if (this.selectedServiceForEdit) {
      return this.selectedServiceForEdit.category;
    }
    return "";
  }

  @computed
  get editDialogs(): Array<Object> {
    if (this.selectedServiceForEdit && this.openDialogTitle) {
      return [
        this.ageDialog,
        this.genderDialog,
        this.openingHoursDialog,
        this.categoryDialog
      ].map(dialog => {
        const updatedDialog: any = dialog;
        updatedDialog.itemId = this.selectedServiceId;
        updatedDialog.onClose = () => {
          this.openDialogTitle = "";
          this.selectedServiceForEdit = null;
        };
        updatedDialog.isOpen = this.openDialogTitle === updatedDialog.title;
        return updatedDialog;
      });
    }
    return [];
  }

  @action
  handleExpandClick = (index: number) => {
    const isExpanded = this.services[index].expanded;
    this.services[index].expanded = !isExpanded;
  };

  @action
  handleMoreMenuClick = (event: any, index: number) => {
    event.preventDefault();
    this.services[index].anchorEl = event.currentTarget;
  };

  @action
  handleMenuClose = () => {
    for (let i = 0; i < this.services.length; i += 1) {
      this.services[i].anchorEl = null;
    }
  };

  @action
  handleDeleteService = (service: any) => {
    this.selectedServiceForEdit = service;
    const serviceName = this.getServiceCategoryName(service);
    const dialogParams = {
      description: `Are you sure you want to delete ${serviceName} service?`,
      onPositiveButtonClicked: this.onDeleteServiceConfirmed
    };
    this.props.UiEventStore.alertDialog.onOpen(dialogParams);
  };

  @action
  onDeleteServiceConfirmed = () => {
    const { LocationStore } = this.props;
    LocationStore.deleteService(this.selectedServiceId);
  };

  // Reference data

  getServiceCategoryName = (service: any): string => {
    const { RefStore } = this.props;
    if (service) {
      return RefStore.getServiceCategoryName(service.category);
    }
    return "";
  };

  @computed
  get currentAgeRestrictions(): Array<string> {
    const { RefStore } = this.props;
    return RefStore.getAgeRestrictionsForService(this.selectedServiceForEdit);
  }

  @computed
  get currentGenderRestrictions(): Array<string> {
    const { RefStore } = this.props;
    return RefStore.getGenderRestrictionsForService(
      this.selectedServiceForEdit
    );
  }

  @computed
  get categories(): Array<Object> {
    const { serviceCategories } = this.props.RefStore;
    return serviceCategories;
  }

  @computed
  get ageReference(): Array<any> {
    const { ageRefs } = this.props.RefStore;
    return ageRefs;
  }

  @computed
  get genderReference(): Array<any> {
    const { genderRefs } = this.props.RefStore;
    return genderRefs;
  }

  @computed
  get isLoading(): boolean {
    return this.props.LocationStore.isGettingServices;
  }

  // Edit dialogs

  closeDialog = () => {
    this.openDialogTitle = "";
    this.selectedServiceForEdit = null;
    this.handleMenuClose();
  };

  @computed
  get genderDialog() {
    return {
      title: "gender",
      dialogTitle: this.currentServiceCategoryId,
      label: "Gender Restriction",
      helperText: "You may leave blank if no restrictions apply",
      options: this.genderReference,
      selectedOptions: this.currentGenderRestrictions,
      onEdit: (newValue: string) => {
        const serviceId = this.selectedServiceId;
        this.props.LocationStore.updateServiceGender(serviceId, newValue);
        this.closeDialog();
      },
      multichoice: true
    };
  }

  @computed
  get ageDialog() {
    return {
      title: "age",
      dialogTitle: this.currentServiceCategoryId,
      label: "Age Restriction",
      helperText: "You may leave blank if no restrictions apply",
      options: this.ageReference,
      selectedOptions: this.currentAgeRestrictions,
      onEdit: (newValue: string) => {
        const serviceId = this.selectedServiceId;
        this.props.LocationStore.updateServiceAge(serviceId, newValue);
        this.closeDialog();
      },
      multichoice: true
    };
  }

  @computed
  get categoryDialog() {
    return {
      title: "category",
      dialogTitle: "Update Service Category",
      label: "Service category",
      options: this.categories,
      selectedOption: this.currentServiceCategoryId,
      onEdit: (newValue: string) => {
        const serviceId = this.selectedServiceId;
        this.props.LocationStore.updateServiceCategory(serviceId, newValue);
        this.closeDialog();
      }
    };
  }

  @computed
  get selectedService(): Object {
    if (this.selectedServiceForEdit) {
      return this.selectedServiceForEdit;
    }
    return {};
  }

  @computed
  get openingHoursDialog() {
    return {
      title: "openingHours",
      openingHours: this.selectedService.openingHours,
      onEdit: (newValue: string) => {
        const serviceId = this.selectedServiceId;
        this.props.LocationStore.updateServiceOpeningHours(serviceId, newValue);
        this.closeDialog();
      }
    };
  }
}

export default LocationServicesScreen;
