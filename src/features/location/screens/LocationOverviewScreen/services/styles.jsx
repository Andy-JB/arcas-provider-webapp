// @flow
const styles = (theme: any) => ({
  card: {
    margin: 8
  },
  sectionTitle: {},
  cardHeader: {
    backgroundColor: "#fff",
    color: "#fff",
    height: 60
  },
  cardContent: {
    backgroundColor: "#fff"
  },
  actions: {
    display: "flex",
    backgroundColor: "#fff"
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: "auto",
    [theme.breakpoints.up("sm")]: {
      marginRight: -8
    }
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  actionHeading: {
    marginLeft: "12px"
  },
  actionEdit: {
    marginLeft: "8px"
  },
  editButton: {
    width: 36,
    height: 36,
    padding: 0,
    marginTop: -4
  },
  editServiceButton: {
    width: 36,
    height: 36,
    padding: 10,
    marginTop: -4,
    color: "black"
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row"
  },
  headerAvatar: {
    margin: 4
  },
  categoryTitle: {
    paddingLeft: 20
  },
  openingHours: {
    paddingTop: 10,
    paddingLeft: 20,
    "@media screen and (max-width:1600px) and (min-width:300px)": {
      paddingLeft: 20
    }
  },
  serviceCardContainer: {
    padding: 10,
    margin: 8
  },
  openingHoursContainer: {
    paddingTop: 20,
    textAlign: "right"
  },
  editTitle: {
    paddingTop: 5
  },
  opHoursEditButton: {
    textAlign: "right"
  },
  noOffersContainer: {
    textAlign: "center",
    paddingTop: 60,
    paddingBottom: 40,
    paddingLeft: 40,
    paddingRight: 40
  },
  noOfferAddIcon: {
    fontSize: 60
  },
  noOffersTitle: {
    paddingBottom: 5
  }
});

export default styles;
