// @flow
import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import "typeface-roboto";
import withStyles from "@material-ui/core/styles/withStyles";
import classnames from "classnames";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import LoadingScreen from "common/screens/LoadingScreen";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import ActionIcon from "@material-ui/icons/MoreVertOutlined";
import DeleteIcon from "@material-ui/icons/Delete";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import EditIcon from "@material-ui/icons/Edit";
import Typography from "@material-ui/core/Typography";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EditMultiChoiceTextDialog from "features/location/components/EditMultiChoiceTextDialog";
import EditSingleChoiceTextDialog from "features/location/components/EditSingleChoiceTextDialog";
import EditOpeningHoursDialog from "features/location/components/EditOpeningHoursDialog";
import { Divider } from "@material-ui/core";
import * as ServiceUtils from "util/ServiceUtils";
import styles from "./styles";
import DateUtils from "../../../../../util/DateUtils";

type Props = {
  controller: any,
  classes: any,
  RefStore: any
};

@inject("RefStore")
@observer
class LocationServicesView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const { isLoading, services } = controller;

    if (isLoading) {
      return <LoadingScreen />;
    }
    if (services.length > 0) {
      return (
        <React.Fragment>
          <Grid container spacing={2}>
            {services.map((service, index) => (
              <React.Fragment key={service.id}>
                {this.renderService(service, index)}
                {this.renderCardMenu(service)}
              </React.Fragment>
            ))}
          </Grid>
          {this.renderEditDialogs()}
        </React.Fragment>
      );
    }
    return (
      <Grid container className={classes.noOffersContainer}>
        <Grid item xs={12}>
          <Typography variant="title" className={classes.noOffersTitle}>
            You have no current services. Click &quot;Add New Service&quot; to
            get started.
          </Typography>
        </Grid>
      </Grid>
    );
  }

  renderService = (service, index) => {
    const { classes, controller, RefStore } = this.props;
    const { days } = controller;
    const { openEditDialog, handleMoreMenuClick } = controller;
    const ageRestrictions = RefStore.getAgeRestrictionsForService(service);
    const genderRestrictons = RefStore.getGenderRestrictionsForService(service);
    const categoryName = controller.getServiceCategoryName(service);
    return (
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <Paper className={classes.serviceCardContainer}>
          <div className={classes.headerContainer}>
            <FontAwesomeIcon
              className={classes.headerAvatar}
              icon={ServiceUtils.getIconForService(service.category)}
              size="2x"
            />
            <Grid container className={classes.categoryTitle}>
              <Grid item xs={10}>
                <Typography variant="subtitle2">
                  <b>{categoryName}</b>
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <IconButton
                  className={classes.editServiceButton}
                  aria-label="Edit"
                  onClick={event => handleMoreMenuClick(event, index, service)}
                >
                  <ActionIcon />
                </IconButton>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="body2">
                  {ageRestrictions.join(", ")}
                  <IconButton
                    className={classes.editButton}
                    aria-label="Edit"
                    onClick={() => openEditDialog("age", service)}
                  >
                    <EditIcon />
                  </IconButton>
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="body2">
                  {genderRestrictons.join(", ")}
                  <IconButton
                    className={classes.editButton}
                    aria-label="Edit"
                    onClick={() => openEditDialog("gender", service)}
                  >
                    <EditIcon />
                  </IconButton>
                </Typography>
              </Grid>
            </Grid>
          </div>
          <div className={classes.openingHoursContainer}>
            <Button
              color="primary"
              variant="text"
              onClick={() => controller.handleExpandClick(index)}
            >
              {service.expanded ? "Hide Opening Hours" : "View Opening Hours"}
            </Button>
          </div>

          <Collapse in={service.expanded} timeout="auto" unmountOnExit>
            <Divider />
            <div className={classes.openingHours}>
              {days.map(day => this.renderOpeningTimeDay(day, service))}
            </div>
            <div className={classes.opHoursEditButton}>
              <IconButton
                className={classnames(classes.actionEdit, classes.editButton)}
                aria-label="Edit"
                onClick={() => openEditDialog("openingHours", service)}
              >
                <EditIcon />
              </IconButton>
            </div>
          </Collapse>
        </Paper>
      </Grid>
    );
  };

  renderEditDialogs = () => {
    const { controller } = this.props;
    const { editDialogs } = controller;
    return editDialogs.map(dialog => {
      if (dialog.multichoice) {
        return (
          <EditMultiChoiceTextDialog
            key={dialog.id}
            dialogOpen={dialog.isOpen}
            onClose={dialog.onClose}
            buttonTitle="Update"
            dialogTitle={dialog.dialogTitle}
            selectedOptions={dialog.selectedOptions}
            onUpdate={dialog.onEdit}
            options={dialog.options}
            itemId={dialog.id}
            textFieldLabel={dialog.label}
            helperText={dialog.helperText}
          />
        );
      }
      if (dialog.openingHours) {
        return (
          <EditOpeningHoursDialog
            key={dialog.id}
            dialogOpen={dialog.isOpen}
            onClose={dialog.onClose}
            onUpdate={dialog.onEdit}
            openingHours={dialog.openingHours}
            itemId={dialog.id}
          />
        );
      }
      return (
        <EditSingleChoiceTextDialog
          key={`${dialog.id}-${dialog.label}`}
          dialogOpen={dialog.isOpen}
          onClose={dialog.onClose}
          buttonTitle="Update"
          dialogTitle={dialog.dialogTitle}
          selectedOption={dialog.selectedOption}
          onUpdate={dialog.onEdit}
          options={dialog.options}
          itemId={dialog.id}
          textFieldLabel={dialog.label}
        />
      );
    });
  };

  renderCardMenu = service => {
    const { classes, controller } = this.props;
    const menuOpen = Boolean(service.anchorEl);
    const { handleMenuClose, handleDeleteService, openEditDialog } = controller;
    return (
      <div>
        <Menu
          id="long-menu"
          anchorEl={service.anchorEl}
          open={menuOpen}
          onClose={() => handleMenuClose()}
        >
          <MenuItem
            key="edit"
            onClick={() => openEditDialog("category", service)}
          >
            <ListItemIcon className={classes.icon}>
              <EditIcon />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.primary }}
              primary="Edit"
            />
          </MenuItem>
          <MenuItem key="delete" onClick={() => handleDeleteService(service)}>
            <ListItemIcon className={classes.icon}>
              <DeleteIcon />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.primary }}
              primary="Delete"
            />
          </MenuItem>
        </Menu>
      </div>
    );
  };

  renderOpeningTimeDay = (dayName, service) => (
    <Grid container key={`${dayName}-${service}`}>
      <Grid item xs={4}>
        <Typography variant="body2">
          <b>{dayName}</b>
        </Typography>
      </Grid>
      <Grid item xs={8}>
        {this.renderOpeningTimes(dayName, service)}
      </Grid>
    </Grid>
  );

  renderOpeningTimes = (dayName, service) => {
    const { openingHours } = service;
    if (!openingHours[dayName].isOpen) {
      return (
        <Typography variant="body2" gutterBottom>
          Closed
        </Typography>
      );
    }
    return openingHours[dayName].times.map(time => (
      <React.Fragment key={time.startTime}>
        <Typography variant="body2" gutterBottom>
          {DateUtils.getFormattedTimeDuration(time.startTime, time.endTime)}
        </Typography>
      </React.Fragment>
    ));
  };
}

export default withStyles(styles)(LocationServicesView);
