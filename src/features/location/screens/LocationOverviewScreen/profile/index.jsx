// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import LocationInfoView from "./LocationProfileView";

type Props = {
  LocationStore: any,
  UiEventStore: any
};

@inject("LocationStore", "UiEventStore")
@observer
class LocationProfileScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];

  @observable
  locationImages = [];

  @observable
  imageLimit: number;

  @observable
  isUpdatingLocationImage: boolean;

  @observable
  locationImageIndex: number;

  @observable
  openDialogTitle: string;

  constructor(props: Object) {
    super(props);
    this.openDialogTitle = "";
    this.isUpdatingLocationImage = false;
    this.locationImageIndex = -1;
    this.imageLimit = 6;
  }

  componentDidMount() {
    this.syncLocationImages();
  }

  render() {
    return <LocationInfoView controller={this} />;
  }

  @computed
  get isLoading(): boolean {
    return this.props.LocationStore.isLocationRetrieved !== true;
  }

  @computed
  get location(): Object {
    return this.props.LocationStore.location;
  }

  @computed
  get title() {
    if (this.location) {
      return this.location.title;
    }
    return "";
  }

  @computed
  get phone() {
    if (this.location) {
      return this.location.phone;
    }
    return "";
  }

  @computed
  get email() {
    if (this.location) {
      return this.location.email;
    }
    return "";
  }

  @computed
  get website() {
    if (this.location) {
      return this.location.website;
    }
    return "";
  }

  @computed
  get description() {
    if (this.location) {
      return this.location.description;
    }
    return "";
  }

  @computed
  get address() {
    if (this.location) {
      return this.location.address;
    }
    return {};
  }

  @computed
  get hasLocationImages() {
    return this.locationImages.length > 0;
  }

  @computed
  get formattedAddress(): string {
    if (this.location) {
      return this.location.address.formattedAddress;
    }
    return "";
  }

  @computed
  get openingHours() {
    if (this.location) {
      return this.location.openingHours;
    }
    return null;
  }

  @computed
  get locationLatLng(): ?Object {
    if (this.location) {
      return this.location.latLng;
    }
    return null;
  }

  @computed
  get locationLatitude(): ?Object {
    if (this.location) {
      return this.location.latLng.lat;
    }
    return null;
  }

  @computed
  get locationLongitude(): ?Object {
    if (this.location) {
      return this.location.latLng.lng;
    }
    return null;
  }

  @computed
  get googleMapkey(): string {
    return (process: any).env.REACT_APP_GOOGLE_MAP_KEY;
  }

  @computed
  get mapBoxkey(): string {
    return (process: any).env.REACT_APP_MAPBOX_KEY;
  }

  @action
  closeLocationPictureModal = () => {
    this.isUpdatingLocationImage = false;
  };

  @action
  openLocationPictureModal = (index: number) => {
    this.locationImageIndex = index;
    this.isUpdatingLocationImage = true;
  };

  @action
  openImageGalleryView = () => {
    const { UiEventStore } = this.props;
    let images = [];
    if (this.locationImages.length > 0) {
      images = this.locationImages.map(x => ({
        src: x.image
      }));
    }
    UiEventStore.lightbox.openLightbox(images);
  };

  @action
  deleteImage = (event: any, index: string) => {
    event.stopPropagation();
    event.preventDefault();
    const indexInt = parseInt(index, 10);
    const newImages = [];
    this.locationImages.forEach((image, i) => {
      if (indexInt !== i) {
        newImages.push(image.image);
      }
    });
    this.props.LocationStore.deleteLocationImage(newImages)
      .then(() => {
        this.syncLocationImages();
      })
      .catch(() => {});
  };

  @action
  handleSubmitImage = (imagePreviewFile: any, url: string) => {
    this.closeLocationPictureModal();
    if (this.locationImageIndex === -1) {
      // new image so append to end
      this.locationImageIndex = this.locationImages.length;
      const imageObj = {};
      imageObj.index = this.locationImages.length;
      imageObj.image = url;
      this.locationImages.push(imageObj);
    } else {
      const image = this.locationImages[this.locationImageIndex];
      image.image = url;
    }
    const { LocationStore } = this.props;
    LocationStore.uploadLocationImage(
      imagePreviewFile,
      this.locationImageIndex
    );
  };

  @action
  openEditDialog = (dialogName: string) => {
    this.openDialogTitle = dialogName;
  };

  @computed
  get editDialogs(): Array<Object> {
    return [
      {
        title: "title",
        label: "Location Name",
        defaultValue: this.title,
        rules: "required|string|max:60",
        storeRef: this.props.LocationStore.updateTitle.bind(this)
      },
      {
        title: "description",
        label: "Description",
        defaultValue: this.description,
        rules: "required|string|max:1000",
        multiline: true,
        storeRef: this.props.LocationStore.updateDescription.bind(this)
      },
      {
        title: "address",
        label: "Address",
        address: this.location,
        storeRef: this.props.LocationStore.updateAddress.bind(this)
      },
      {
        title: "phone",
        label: "Contact Number",
        defaultValue: this.phone,
        rules: "required|numeric|digits:10",
        storeRef: this.props.LocationStore.updatePhone.bind(this)
      },
      {
        title: "email",
        label: "Contact Email",
        defaultValue: this.email,
        rules: "required|email|string|max:100",
        storeRef: this.props.LocationStore.updateEmail.bind(this)
      },
      {
        title: "website",
        label: "Website",
        defaultValue: this.website,
        rules: "website|string",
        storeRef: this.props.LocationStore.updateWebsite.bind(this)
      },
      {
        title: "openingHours",
        label: "Opening Hours",
        openingHours: this.openingHours,
        storeRef: this.props.LocationStore.updateOpeningHours.bind(this)
      }
    ].map(dialog => {
      const updatedDialog: any = dialog;
      updatedDialog.onClose = () => {
        this.openDialogTitle = "";
      };
      updatedDialog.onEdit = (newValue: string) => {
        updatedDialog.storeRef(newValue);
        this.openDialogTitle = "";
      };
      updatedDialog.isOpen = this.openDialogTitle === updatedDialog.title;
      return updatedDialog;
    });
  }

  @action
  syncLocationImages = () => {
    if (this.location != null) {
      const { images } = this.location;
      if (images !== undefined) {
        this.locationImages = [];
        const imageKeys = Object.keys(images);
        imageKeys.forEach(key => {
          const imageUrl = images[key];
          const imageObj = {};
          imageObj.index = key;
          imageObj.image = imageUrl;
          this.locationImages.push(imageObj);
        });
      }
    }
  };
}

export default LocationProfileScreen;
