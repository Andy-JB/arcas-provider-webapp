// @flow
import React, { Component } from "react";
import { computed } from "mobx";
import { observer } from "mobx-react";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from "@material-ui/icons/Edit";
import Typography from "@material-ui/core/Typography";
import LoadingScreen from "common/screens/LoadingScreen";
import FileUploadDialog from "common/dialogs/FileUploadDialog";
import EditTextDialog from "common/dialogs/EditTextDialog";
import EditOpeningHoursDialog from "features/location/components/EditOpeningHoursDialog";
import ReactMapGL, {Marker} from 'react-map-gl';
import DateUtils from "util/DateUtils";
import MarkerImg from "media/images/marker256.png";

import EditAddressDialog from "features/location/components/EditAddressDialog";

import addLocatonImage from "media/images/add-location.png";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any
};

@observer
class LocationProfileView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const {
      isLoading,
      openEditDialog,
      title,
      email,
      website,
      description,
      formattedAddress,
      phone,
      days,
      locationLatitude,
      locationLongitude,
      mapBoxkey
    } = controller;
    if (isLoading) {
      return <LoadingScreen />;
    }
    return (
      <React.Fragment>
        <div className={classes.layout}>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6}>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Location Name
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      className={classes.editButton}
                      aria-label="Edit"
                      onClick={() => openEditDialog("title")}
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Typography variant="body2" align="left">
                  {title}
                </Typography>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Description
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      className={classes.editButton}
                      aria-label="Edit"
                      onClick={() => openEditDialog("description")}
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={10}>
                    <Typography
                      className={classes.multilineText}
                      variant="body2"
                      align="left"
                    >
                      {description}
                    </Typography>
                  </Grid>
                </Grid>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Address
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("address")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Typography variant="body2" align="left">
                  {formattedAddress}
                </Typography>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Contact Number
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("phone")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Typography variant="body2" align="left">
                  {phone}
                </Typography>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Contact Email
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("email")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Typography variant="body2" align="left">
                  {email}
                </Typography>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Website
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("website")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Typography variant="body2" align="left">
                  {website || "No website"}
                </Typography>
              </div>
              <div className={classes.profileInformationField}>
                <Grid container>
                  <Grid item xs={11}>
                    <Typography variant="caption" align="left">
                      Opening hours
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      onClick={() => openEditDialog("openingHours")}
                      className={classes.editButton}
                      aria-label="Edit"
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Grid container>
                  {days.map(day => (
                    <React.Fragment key={day}>
                      <Grid item xs={4}>
                        <Typography variant="body2" gutterBottom>
                          {day}
                        </Typography>
                      </Grid>
                      <Grid item xs={8}>
                        {this.renderOpeningTimes(day)}
                      </Grid>
                      <br />
                    </React.Fragment>
                  ))}
                </Grid>
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Paper className={classes.mapContainer}>
                {/*<GoogleMapReact
                  bootstrapURLKeys={{
                    key: googleMapkey
                  }}
                  options={{
                    zoomControl: false,
                    scaleControl: false,
                    scrollwheel: false,
                    disableDoubleClickZoom: true,
                    draggable: false,
                    navigationControl: false,
                    fullscreenControl: false
                  }}
                  center={locationLatLng}
                  zoom={15}
                >
                  <MapMarker lat={locationLatitude} lng={locationLongitude} />
                </GoogleMapReact>*/}
                <ReactMapGL
                  latitude={locationLatitude}
                  longitude={locationLongitude}
                  mapboxApiAccessToken={mapBoxkey}
                  width={"100%"}
                  height={"100%"}
                  zoom={16}
                  mapStyle = "mapbox://styles/mapbox/streets-v11"
                ><Marker
                  latitude={locationLatitude}
                  longitude={locationLongitude}
                  ><img src={MarkerImg} alt="Marker" width="24" height="24" />
                  </Marker>
                </ReactMapGL>
              </Paper>
              <br />
              {this.imagesView}
            </Grid>
            {this.renderFileUploadDialog()}
            {this.renderEditDialogs()}
          </Grid>
        </div>
      </React.Fragment>
    );
  }

  @computed
  get mapCenter(): Object {
    return { lat: -34.92492, lng: 138.59712 };
  }

  @computed
  get imagesView() {
    const { classes, controller } = this.props;
    const {
      locationImages,
      hasLocationImages,
      deleteImage,
      openLocationPictureModal,
      openImageGalleryView
    } = controller;
    return (
      <div className={classes.profileInformationField}>
        <Grid container>
          <Grid item xs={6}>
            <Typography variant="caption">Location Images</Typography>
          </Grid>
          {hasLocationImages && (
            <Grid item xs={6}>
              <div className={classes.viewImagesContainer}>
                <Button
                  color="primary"
                  className={classes.viewImagesButton}
                  onClick={() => openImageGalleryView()}
                >
                  View All
                </Button>
              </div>
            </Grid>
          )}
        </Grid>
        <GridList cellHeight={130} className={classes.gridList} cols={3}>
          {locationImages.map(image => (
            <GridListTile
              key={image.index}
              onClick={() => openLocationPictureModal(image.index)}
            >
              <img src={image.image} alt={image.index} />
              <GridListTileBar
                titlePosition="top"
                className={classes.titleBar}
                actionIcon={
                  <IconButton
                    onClick={event => deleteImage(event, image.index)}
                    className={classes.deleteIcon}
                  >
                    <DeleteIcon />
                  </IconButton>
                }
                actionPosition="right"
              />
            </GridListTile>
          ))}
          {locationImages.length < controller.imageLimit && (
            <GridListTile
              key="new"
              onClick={() => openLocationPictureModal(-1)}
            >
              <img src={addLocatonImage} alt="location" />
            </GridListTile>
          )}
        </GridList>
      </div>
    );
  }

  renderEditDialogs = () =>
    this.props.controller.editDialogs.map(dialog => {
      if (dialog.address) {
        return (
          <EditAddressDialog
            key={dialog.label}
            dialogOpen={dialog.isOpen}
            onClose={dialog.onClose}
            onUpdate={dialog.onEdit}
            location={dialog.address}
          />
        );
      }
      if (dialog.openingHours) {
        return (
          <EditOpeningHoursDialog
            key={dialog.label}
            dialogOpen={dialog.isOpen}
            onClose={dialog.onClose}
            onUpdate={dialog.onEdit}
            openingHours={dialog.openingHours}
            itemId={null}
          />
        );
      }
      return (
        <EditTextDialog
          dialogOpen={dialog.isOpen}
          onClose={dialog.onClose}
          buttonTitle="Update"
          dialogTitle="Profile Update"
          defaultValue={dialog.defaultValue}
          onUpdate={dialog.onEdit}
          textFieldLabel={dialog.label}
          key={dialog.label}
          validationRules={dialog.rules}
          multiline={dialog.multiline}
        />
      );
    });

  renderOpeningTimes = dayName => {
    const { openingHours } = this.props.controller;
    if (openingHours == null) {
      return "";
    }
    if (!openingHours[dayName].isOpen) {
      return (
        <Typography variant="body2" gutterBottom>
          Closed
        </Typography>
      );
    }
    return openingHours[dayName].times.map(time => (
      <React.Fragment key={time.startTime}>
        <Typography variant="body2" gutterBottom>
          {DateUtils.getFormattedTimeDuration(time.startTime, time.endTime)}
        </Typography>
      </React.Fragment>
    ));
  };

  renderFileUploadDialog = () => {
    const { controller } = this.props;
    const { closeLocationPictureModal, handleSubmitImage } = controller;
    const { isUpdatingLocationImage } = controller;
    return (
      <FileUploadDialog
        displayLoading={false}
        openDialog={isUpdatingLocationImage}
        onClose={closeLocationPictureModal}
        onSubmit={handleSubmitImage}
      />
    );
  };
}

export default withStyles(styles)(LocationProfileView);
