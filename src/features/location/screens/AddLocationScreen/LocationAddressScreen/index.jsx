// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import LocationAddressView from "./LocationAddressView";

type Props = {
  controller: any
};

@observer
class LocationAddressScreen extends Component<Props> {
  @observable
  states = [
    {
      value: "NSW",
      label: "New South Wales"
    },
    {
      value: "QLD",
      label: "Queensland"
    },
    {
      value: "VIC",
      label: "Victoria"
    },
    {
      value: "SA",
      label: "South Australia"
    },
    {
      value: "TAS",
      label: "Tasmania"
    },
    {
      value: "WA",
      label: "Western Australia"
    },
    {
      value: "ACT",
      label: "Australian Capital Territory"
    },
    {
      value: "NT",
      label: "Northern Territory"
    }
  ];

  @observable
  address = {};

  render() {
    return (
      <LocationAddressView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: any) => {
    this.address = form.values();
    const { addAddress, handleNext } = this.props.controller;
    addAddress(this.address);
    handleNext();
  };

  @action
  onError = () => {};

  @action
  handleBack = () => {
    this.props.controller.handleBack();
  };

  @computed
  get formFields(): Array<Object> {
    const { address } = this.props.controller.location;
    return [
      {
        name: "address1",
        type: "text",
        label: "Address line 1",
        rules: "required|string",
        value: address.address1
      },
      {
        name: "address2",
        type: "text",
        label: "Address line 2",
        rules: "string",
        value: address.address2
      },
      {
        name: "suburb",
        type: "text",
        label: "Suburb",
        rules: "required|string",
        value: address.suburb
      },
      {
        name: "state",
        type: "text",
        label: "State",
        rules: "required|string",
        value: address.state
      },
      {
        name: "postcode",
        type: "text",
        label: "Postcode",
        rules: "required|numeric|digits:4",
        value: address.postcode
      },
      {
        name: "country",
        type: "text",
        label: "Country",
        value: "Australia",
        rules: "required|string"
      }
    ];
  }
}

export default LocationAddressScreen;
