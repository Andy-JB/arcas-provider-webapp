// @flow
const styles = (theme: any) => ({
  menu: {
    width: 200
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  }
});
export default styles;
