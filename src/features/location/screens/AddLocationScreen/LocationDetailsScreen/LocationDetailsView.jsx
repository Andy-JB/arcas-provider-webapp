// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import styles from "./styles";

type Props = {
  classes: any,
  form: any
};

@observer
class LocationDetailsView extends Component<Props> {
  render() {
    const { classes, form } = this.props;
    const title = form.$("title");
    const description = form.$("description");
    const phone = form.$("phone");
    const email = form.$("email");
    const website = form.$("website");
    const type = null;
    const errors = form.errors();
    return (
      <React.Fragment>
        <form className={classes.form} onSubmit={form.onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                variant="outlined"
                {...title.bind({ type })}
                fullWidth
                autoFocus
                error={errors.title != null}
                helperText={errors.title}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                variant="outlined"
                {...description.bind({ type })}
                fullWidth
                multiline
                rows="4"
                rowsMax="8"
                margin="normal"
                error={errors.description != null}
                helperText={errors.description}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                required
                variant="outlined"
                {...phone.bind({ type })}
                fullWidth
                autoComplete="phone"
                margin="normal"
                error={errors.phone != null}
                helperText={errors.phone}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                required
                variant="outlined"
                {...email.bind({ type })}
                fullWidth
                autoComplete="email"
                margin="normal"
                error={errors.email != null}
                helperText={errors.email}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                variant="outlined"
                {...website.bind({ type })}
                fullWidth
                autoComplete="website"
                margin="normal"
                error={errors.website != null}
                helperText={errors.website}
              />
            </Grid>
          </Grid>
          <React.Fragment>
            <div className={classes.buttons}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
              >
                Next
              </Button>
            </div>
          </React.Fragment>
        </form>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(LocationDetailsView);
