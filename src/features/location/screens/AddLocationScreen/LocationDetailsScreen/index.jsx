// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import LocationDetailsView from "./LocationDetailsView";

type Props = {
  controller: any
};

@observer
class LocationDetailsScreen extends Component<Props> {
  @observable
  locationDetails = {};

  render() {
    return (
      <LocationDetailsView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: Object) => {
    this.locationDetails = form.values();
    const { addLocationDetails, handleNext } = this.props.controller;
    addLocationDetails(this.locationDetails);
    handleNext();
  };

  @action
  onError = () => {};

  @computed
  get formFields(): Array<Object> {
    const {
      title,
      description,
      email,
      phone,
      website
    } = this.props.controller.location;

    return [
      {
        name: "title",
        type: "text",
        label: "Title",
        rules: "required|string",
        value: title
      },
      {
        name: "description",
        type: "text",
        label: "Description",
        rules: "required|string|max:500",
        value: description
      },
      {
        name: "phone",
        type: "tel",
        label: "Phone",
        rules: "required|numeric",
        value: phone
      },
      {
        name: "email",
        type: "email",
        label: "Email",
        rules: "required|email|string",
        value: email
      },
      {
        name: "website",
        type: "website",
        label: "Website URL",
        rules: "website|string",
        value: website,
        placeholder: "https://"
      }
    ];
  }
}

export default LocationDetailsScreen;
