// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import styles from "./styles";
import DateUtils from "../../../../../util/DateUtils";

type Props = {
  controller: any,
  classes: any
};

@observer
class LocationSummaryView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const {
      days,
      title,
      description,
      email,
      phone,
      addressDetails
    } = controller;
    return (
      <React.Fragment>
        <div className={classes.reviewHeadingContainer}>
          <Typography variant="title">Please confirm the following</Typography>
        </div>
        <div className={classes.questionContainer}>
          <Grid container spacing={16}>
            <Grid item xs={12} sm={6}>
              <div className={classes.question}>
                <Typography variant="caption">Title</Typography>
                <Typography variant="body1">{title}</Typography>
              </div>
              <div className={classes.question}>
                <Typography variant="caption">Description</Typography>
                <Typography variant="body1">{description}</Typography>
              </div>
              <div className={classes.question}>
                <Typography variant="caption">Email</Typography>
                <Typography variant="body1">{email}</Typography>
              </div>
              <div className={classes.question}>
                <Typography variant="caption">Phone</Typography>
                <Typography variant="body1">{phone}</Typography>
              </div>
              <div className={classes.question}>
                <Typography variant="caption">Address</Typography>
                <Typography variant="body1">{addressDetails}</Typography>
              </div>
            </Grid>
            <Grid item container direction="column" xs={12} sm={6}>
              <Typography className={classes.question} variant="caption">
                Opening hours
              </Typography>
              <Grid container>
                {days.map(day => (
                  <React.Fragment key={day}>
                    <Grid item xs={6}>
                      <Typography gutterBottom>{day}</Typography>
                    </Grid>
                    <Grid item xs={6}>
                      {this.renderOpeningTimes(day)}
                    </Grid>
                    <br />
                  </React.Fragment>
                ))}
              </Grid>
            </Grid>
          </Grid>
        </div>
        <React.Fragment>
          <div className={classes.buttons}>
            <Button
              className={classes.button}
              onClick={() => controller.handleBack()}
            >
              Back
            </Button>
            <Button
              type="button"
              variant="contained"
              color="primary"
              onClick={() => controller.handleNext()}
              className={classes.button}
            >
              Confirm
            </Button>
          </div>
        </React.Fragment>
      </React.Fragment>
    );
  }

  renderOpeningTimes = dayName => {
    const { openingHours } = this.props.controller;
    if (!openingHours[dayName].isOpen) {
      return <Typography gutterBottom>Close</Typography>;
    }
    return openingHours[dayName].times.map(time => (
      <Typography gutterBottom>
        {DateUtils.getFormattedTimeDuration(time.startTime, time.endTime)}
      </Typography>
    ));
  };
}

export default withStyles(styles)(LocationSummaryView);
