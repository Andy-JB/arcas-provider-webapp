// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import LocationSummaryView from "./LocationSummaryView";

type Props = {
  controller: any
};

@observer
class LocationSummaryScreen extends Component<Props> {
  @observable
  days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];

  @observable
  location: Object;

  constructor(props: Object) {
    super(props);
    this.location = this.props.controller.location;
  }

  render() {
    return <LocationSummaryView controller={this} />;
  }

  @computed
  get addressDetails(): string {
    if (this.location && this.location.address) {
      return this.location.address.formattedAddress;
    }
    return "";
  }

  @computed
  get title(): string {
    return this.location.title;
  }

  @computed
  get description(): string {
    return this.location.description;
  }

  @computed
  get email(): string {
    return this.location.email;
  }

  @computed
  get phone(): string {
    return this.location.phone;
  }

  @computed
  get openingHours(): Object {
    return this.location.openingHours;
  }

  @action
  handleNext = () => {
    this.props.controller.handleNext();
    this.props.controller.handleAddLocation();
  };

  @action
  handleBack = () => {
    this.props.controller.handleBack();
  };
}

export default LocationSummaryScreen;
