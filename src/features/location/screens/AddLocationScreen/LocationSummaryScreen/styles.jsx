// @flow
const styles = (theme: any) => ({
  submit: {
    marginTop: theme.spacing(3)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: 20
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  reviewHeadingContainer: {
    display: "flex",
    justifyContent: "center",
    paddingTop: 30
  },
  questionContainer: {
    marginTop: 30
  },
  question: {
    marginTop: 10
  }
});
export default styles;
