// @flow
const styles = (theme: any) => ({
  layout: {
    paddingTop: 20,
    paddingBottom: 40,
    paddingRight: 80,
    paddingLeft: 80,
    "@media screen and (max-width:1150px) and (min-width:950px)": {
      paddingRight: 10,
      paddingLeft: 10
    },
    [theme.breakpoints.down("sm")]: {
      paddingRight: 20,
      paddingLeft: 20
    }
  },
  stepper: {
    padding: `${theme.spacing(3)}px 0 ${theme.spacing(5)}px`
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  summaryWrapper: {
    display: "flex",
    justifyContent: "center"
  },
  progressWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 400
  },
  progress: {
    margin: theme.spacing(2)
  }
});

export default styles;
