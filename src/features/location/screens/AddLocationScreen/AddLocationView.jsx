// @flow
import React, { Component } from "react";
import { computed } from "mobx";
import { observer } from "mobx-react";
import withStyles from "@material-ui/core/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import CircularProgress from "@material-ui/core/CircularProgress";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import history from "appHistory";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any
};

@observer
class AddLocationView extends Component<Props> {
  render() {
    const { classes, controller } = this.props;
    const { activeStep, steps, getStepContent } = controller;
    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          {activeStep < steps.length && (
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
          )}
          <React.Fragment>
            {activeStep > steps.length ? (
              this.summaryView
            ) : (
              <React.Fragment>{getStepContent(activeStep)}</React.Fragment>
            )}
          </React.Fragment>
        </main>
      </React.Fragment>
    );
  }

  @computed
  get summaryView() {
    const { classes } = this.props;
    const { isLocationCreating } = this.props.controller;
    if (isLocationCreating) {
      return (
        <React.Fragment>
          <div className={classes.progressWrapper}>
            <CircularProgress className={classes.progress} size={50} />
          </div>
        </React.Fragment>
      );
    }
    history.push("/locations/overview/");
    return null;
  }
}

export default withStyles(styles)(AddLocationView);
