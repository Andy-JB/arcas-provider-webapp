// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer, inject } from "mobx-react";
import history from "appHistory";
import OpeningHoursView from "features/location/components/OpeningHoursView";
import DashboardOverlapLayout from "common/layouts/DashboardOverlapLayout";
import LocationIcon from "@material-ui/icons/LocationOn";
import AddLocationView from "./AddLocationView";
import LocationDetailsView from "./LocationDetailsScreen";
import LocationSummaryView from "./LocationSummaryScreen";
import AutocompleteAddressView from "../../components/AutocompleteAddressView";

type Props = {
  UserStore: any,
  LocationStore: any
};

@inject("UserStore", "LocationStore")
@observer
class AddLocationScreen extends Component<Props> {
  static defaultProps: Props;

  @observable
  activeStep = 0;

  @observable
  steps = ["Location details", "Address details", "Opening hours"];

  @observable
  location = {
    title: "",
    description: "",
    email: "",
    phone: "",
    website: "",
    address: {},
    latLng: {},
    openingHours: {}
  };

  @observable
  isLocationCreating = false;

  constructor(props: Object) {
    super(props);
    const { UserStore } = this.props;
    this.location.email = UserStore.user.email;
    this.location.website = UserStore.currentOrganisation.website;
    this.location.phone = UserStore.currentOrganisation.phone;
  }

  render() {
    return (
      <DashboardOverlapLayout
        pageTitle="Add Location"
        headerIcon={LocationIcon}
      >
        <AddLocationView controller={this} />
      </DashboardOverlapLayout>
    );
  }

  @action
  getStepContent = (step: any) => {
    switch (step) {
      case 0:
        return <LocationDetailsView controller={this} />;
      case 1:
        return <AutocompleteAddressView controller={this} />;
      case 2:
        return <OpeningHoursView controller={this} />;
      case 3:
        return <LocationSummaryView controller={this} />;
      default:
        throw new Error("Unknown step");
    }
  };

  @action
  addLocationDetails = (locationDetails: any) => {
    this.location.title = locationDetails.title;
    this.location.description = locationDetails.description;
    this.location.email = locationDetails.email;
    this.location.phone = locationDetails.phone;
    this.location.website = locationDetails.website;
  };

  @action
  addAddress = (location: Object) => {
    this.location.latLng = location.latLng;
    this.location.address = location.address;
  };

  @action
  addOpeningHours = (openingHours: Object) => {
    this.location.openingHours = openingHours;
  };

  @action
  handleNext = () => {
    this.activeStep += 1;
  };

  @action
  handleBack = () => {
    this.activeStep -= 1;
  };

  @action
  handleCancel = () => {
    history.push("/dashboard");
  };

  @action
  handleAddLocation = () => {
    const { LocationStore, UserStore } = this.props;
    const organisation = UserStore.currentOrganisation;
    this.isLocationCreating = true;
    LocationStore.createOrUpdate(organisation, this.location)
      .then(() => {
        this.isLocationCreating = false;
        window.location.reload();
      })
      .catch(() => {
        this.isLocationCreating = false;
        window.location.reload();
      });
  };

  @computed
  get openingHours() {
    return this.location.openingHours;
  }
}

export default AddLocationScreen;
