// @flow

const styles = theme => ({
  formContainer: {
    paddingTop: 20,
    width: "80%",
    paddingBottom: 50,
    margin: "auto"
  },
  descriptionContainer: {
    paddingTop: 40,
    width: "80%",
    paddingBottom: 20,
    margin: "auto"
  },
  stepperButtonContainer: {
    paddingBottom: 30,
    paddingRight: 20,
    paddingTop: 20,
    textAlign: "right"
  }
});

export default styles;
