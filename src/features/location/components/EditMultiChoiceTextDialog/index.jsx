// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import EditMultiChoiceDialogView from "./EditMultiChoiceDialogView";

type Props = {
  selectedOptions: any,
  onClose: any,
  onUpdate: any,
  textFieldLabel: any,
  options: any,
  itemId: any
};

@observer
class EditMultiChoiceDialog extends Component<Props> {
  @observable
  selectedOptionList = [];

  constructor(props: Props) {
    super(props);
    this.selectedOptionList = this.props.selectedOptions;
  }

  render() {
    return (
      <EditMultiChoiceDialogView
        controller={this}
        parentProps={this.props}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: any) => {
    let selectedOptions = form.values().textField;
    if (selectedOptions.length === 0) {
      selectedOptions = this.allOptionNames;
    }
    // ids of the options
    const selectedOptionsIds = this.getSelectedOptionsIds(selectedOptions);
    this.props.onUpdate(selectedOptionsIds, this.props.itemId);
  };

  @computed
  get allOptionNames(): Array<string> {
    const optionNames = [];
    this.props.options.map(option => optionNames.push(option.name));
    return optionNames;
  }

  @action
  getSelectedOptionsIds(selectedOptions: Array<string>): Array<string> {
    const optionIds = [];
    selectedOptions.map(option =>
      optionIds.push(this.props.options.find(ref => ref.name === option).id)
    );
    return optionIds;
  }

  @action
  onError = () => {};

  @action
  handleClose = () => {
    this.props.onClose();
  };

  @action
  handleOptionChange = (event: any) => {
    this.selectedOptionList = event.target.value;
  };

  @computed
  get formFields(): Array<Object> {
    const { textFieldLabel } = this.props;
    return [
      {
        name: "textField",
        type: "select",
        label: textFieldLabel,
        value: this.selectedOptionList
      }
    ];
  }
}

export default EditMultiChoiceDialog;
