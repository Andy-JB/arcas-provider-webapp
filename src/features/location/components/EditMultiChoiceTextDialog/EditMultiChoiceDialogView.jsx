// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import Checkbox from "@material-ui/core/Checkbox";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any,
  parentProps: any
};

@observer
class EditMultiChoiceDialogView extends Component<Props> {
  render() {
    const { classes, controller, form, parentProps } = this.props;
    const { selectedOptionList, handleOptionChange, handleClose } = controller;
    const {
      dialogOpen,
      dialogTitle,
      dialogContentText,
      textFieldLabel,
      options,
      helperText,
      buttonTitle
    } = parentProps;
    const textField = form.$("textField");
    const type = null;
    return (
      <div>
        <Dialog
          open={dialogOpen}
          fullWidth
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form className={classes.form} onSubmit={form.onSubmit}>
            <DialogTitle id="form-dialog-title">{dialogTitle}</DialogTitle>
            <DialogContent>
              <DialogContentText>{dialogContentText}</DialogContentText>
              <FormControl fullWidth>
                <InputLabel htmlFor="select-multiple-checkbox">
                  {textFieldLabel}
                </InputLabel>
                <Select
                  autoFocus
                  required
                  multiple
                  {...textField.bind({ type })}
                  onChange={handleOptionChange}
                  input={<Input id="select-multiple-checkbox" />}
                  renderValue={selected => selected.join(", ")}
                  MenuProps={{
                    className: classes.menu
                  }}
                >
                  {options &&
                    options.map(ref => (
                      <MenuItem key={ref.id} value={ref.name}>
                        <Checkbox
                          checked={selectedOptionList.indexOf(ref.name) > -1}
                        />
                        <ListItemText primary={ref.name} />
                      </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{helperText}</FormHelperText>
              </FormControl>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="primary">
                {buttonTitle}
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditMultiChoiceDialogView);
