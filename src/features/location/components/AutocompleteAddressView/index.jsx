// @flow
import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { observable, action, computed } from "mobx";
import MobxForm from "util/MobxForm";
import AutocompleteAddressView from "./AutocompleteAddressView";

type Props = {
  LocationStore: any,
  controller: any,
  isDialog: any
};

@inject("LocationStore")
@observer
class AutocompleteAddress extends Component<Props> {
  static defaultProps: Props;

  @observable
  suggestions: Array<any> = [];

  @observable
  searchValue: string;

  @observable
  selectedLocation: ?Object;

  constructor(props: Object) {
    super(props);
    this.selectedLocation = this.previousLocation();
    this.searchValue = this.previousAddress();
  }

  render() {
    return (
      <AutocompleteAddressView
        controller={this}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  handleSuggestionsFetch = ({ value }: Object) => {
    this.fetchAddressSuggestions(value);
  };

  @action
  handleSuggestionsClear = () => {
    this.suggestions = [];
  };

  @action
  onSuggestionSelected = (event: Object, { suggestion }: Object): void => {
    this.fetchAddressDetails(suggestion.id);
  };

  @action
  handleChange = () => (event: any, { newValue }: Object) => {
    this.searchValue = newValue;
  };

  @action
  async fetchAddressSuggestions(newValue: string) {
    try {
      this.suggestions = await this.props.LocationStore.getGooglePlaces(
        this.googleMapkey,
        newValue
      );
    } catch (e) {
      this.suggestions = [];
    }
  }

  @action
  fetchAddressDetails = async (placeId: string) => {
    try {
      this.selectedLocation = await this.props.LocationStore.getGooglePlaceDetails(
        this.googleMapkey,
        placeId
      );
    } catch (e) {
      this.suggestions = [];
    }
  };

  @action
  parseAddressComponents = (addressComponents: Array<Object>): Object => {
    const address = {};
    addressComponents.forEach(component => {
      const { types } = component;
      if (types.includes("floor")) {
        address.floor = component.long_name;
      } else if (types.includes("street_number")) {
        address.streetNumber = component.long_name;
      } else if (types.includes("route")) {
        address.streetName = component.long_name;
      } else if (types.includes("locality")) {
        address.suburb = component.long_name;
      } else if (types.includes("administrative_area_level_1")) {
        address.state = component.short_name;
      } else if (types.includes("country")) {
        address.country = component.long_name;
      } else if (types.includes("postal_code")) {
        address.postcode = component.long_name;
      }
    });
    return address;
  };

  @action
  getSuggestionValue = (suggestion: Object): string => suggestion.label;

  @action
  handleBack = () => {
    this.props.controller.handleBack();
  };

  @action
  handleCancel = () => {
    this.props.controller.handleCancel();
  };

  @action
  onSuccess = () => {
    this.props.controller.addAddress(this.selectedLocation);
    this.props.controller.handleNext();
  };

  @action
  onError = () => {};

  @action
  mapBoxUpdateAddress = (resultComponent) => {
    try {
      const locationInfo = {
        latLng: {
          lat: resultComponent.result.geometry.coordinates[1],
          lng: resultComponent.result.geometry.coordinates[0]
        },
        address: {
          country: "Australia",
          formattedAddress: resultComponent.result.place_name || "",
          postcode: resultComponent.result.context[0].text || "",
          state: resultComponent.result.context[3].short_code.slice(3,) || "",
          streetName: resultComponent.result.text || "",
          streetNumber: resultComponent.result.address || "", 
          suburb: resultComponent.result.context[1].text || ""
        }
      };
      this.selectedLocation = locationInfo;
      this.searchValue = locationInfo.address.formattedAddress;
    }
    catch {
      alert("Invalid Address!");
      this.selectedLocation = null;
      this.searchValue = null;
    }
  };

  @computed
  get googleMapkey(): string {
    return (process: any).env.REACT_APP_GOOGLE_MAP_KEY;
  }

  @computed
  get mapBoxkey(): string {
    return (process: any).env.REACT_APP_MAPBOX_KEY;
  }

  previousLocation(): Object {
    const { address, latLng } = this.props.controller.location;
    if (this.isEmpty(address)) {
      return {};
    }
    const location = {};
    location.address = address;
    location.latLng = latLng;
    return location;
  }

  previousAddress(): string {
    if (this.selectedLocation && this.selectedLocation.address) {
      return this.selectedLocation.address.formattedAddress;
    }
    return "";
  }

  @computed
  get isEditDialog(): boolean {
    return this.props.isDialog;
  }

  @computed
  get formFields(): Array<Object> {
    return [
      {
        name: "address",
        type: "text",
        label: "Address",
        value: this.searchValue,
        placeholder: "Start typing your address",
        rules: "required|string"
      }
    ];
  }

  @computed
  get isLocationSelected(): boolean {
    return !this.isEmpty(this.selectedLocation);
  }

  isEmpty(obj: ?Object): boolean {
    if (!obj) return true;
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }
}
export default AutocompleteAddress;
