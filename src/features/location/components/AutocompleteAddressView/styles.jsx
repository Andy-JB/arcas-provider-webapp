// @flow
const styles = (theme: any) => ({
  root: {
    width: "100%"
  },
  mapContainer: {
    width: "100%",
    height: "40vh",
    marginTop: 20
  },
  autosuggestContainer: {
    display: "flex",
    justifyContent: "center"
  },
  container: {
    position: "relative",
    width: "75%"
  },
  suggestionsContainerOpen: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0
  },
  suggestion: {
    display: "block"
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: "none"
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  dialogActionButtons: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: 10
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  mapboxSearch: {
    display: "grid",
    overflow: "visible",
    justifyContent: "center"
  }
});

export default styles;
