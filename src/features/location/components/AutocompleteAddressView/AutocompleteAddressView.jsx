// @flow
import "mapbox-gl/dist/mapbox-gl.css";
import "react-map-gl-geocoder/dist/mapbox-gl-geocoder.css";
import React, { Component } from "react";
import { computed } from "mobx";
import { observer } from "mobx-react";
import withStyles from "@material-ui/core/styles/withStyles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ReactMapGL from 'react-map-gl';
import Geocoder from 'react-map-gl-geocoder'
import styles from "./styles";

type Props = {
  classes: any,
  controller: any,
  form: any
};

@observer
class AutocompleteAddressView extends Component<Props> {
  state = {
    viewport: {
      width: "100%",
      height: "100%",
      latitude: -34.9282,
      longitude: 138.60,
      zoom: 13,
    }
  };
  mapRef = React.createRef();
  geocoderContainerRef = React.createRef();
  handleViewportChange = viewport => {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport }
    });
  };
  handleGeocoderViewportChange = viewport => {
    const geocoderDefaultOverrides = { transitionDuration: 1000 };
    return this.handleViewportChange({
      ...viewport,
      ...geocoderDefaultOverrides
    });
  };
  render() {
    const { classes, controller, form } = this.props;
    const {
      isLocationSelected,
      mapBoxkey,
      isEditDialog,
      mapBoxUpdateAddress
    } = controller;
    
    const { viewport } = this.state;
    return (
      <div className={classes.root}>
        <form onSubmit={form.onSubmit}>
            <div className={classes.mapboxSearch}
              ref={this.geocoderContainerRef}
            ></div>
          <Paper className={classes.mapContainer}>
            <ReactMapGL
              ref={this.mapRef}
              {...viewport}
              onViewportChange={this.handleViewportChange}
              mapboxApiAccessToken={mapBoxkey}
              mapStyle = "mapbox://styles/mapbox/streets-v11"

            />
            <Geocoder
              mapRef={this.mapRef}
              onViewportChange={this.handleGeocoderViewportChange}
              mapboxApiAccessToken={mapBoxkey}
              position="top-left"
              countries="au"
              types={"poi,address"}
              onResult={mapBoxUpdateAddress}
              containerRef={this.geocoderContainerRef}
          />
          </Paper>
          {!isEditDialog && (
            <React.Fragment>
              <div className={classes.buttons}>
                <Button
                  className={classes.button}
                  onClick={() => controller.handleCancel()}
                >
                  Cancel
                </Button>
                {!isEditDialog && (
                  <Button
                    className={classes.button}
                    onClick={() => controller.handleBack()}
                  >
                    Back
                  </Button>
                )}
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  disabled={isLocationSelected === false}
                >
                  Next
                </Button>
              </div>
            </React.Fragment>
          )}
          {isEditDialog && (
            <React.Fragment>
              <div className={classes.dialogActionButtons}>
                <Button
                  className={classes.button}
                  color="primary"
                  onClick={() => controller.handleCancel()}
                >
                  Cancel
                </Button>
                <Button
                  type="submit"
                  color="primary"
                  className={classes.button}
                  disabled={isLocationSelected === false}
                >
                  Update
                </Button>
              </div>
            </React.Fragment>
          )}
        </form>
      </div>
    );
  }

  @computed
  get initialMapCenter(): Object {
    return { lat: -25.734968, lng: 134.489563 };
  }

  @computed
  get addressMapCenter(): Object {
    return this.props.controller.selectedLocation.latLng;
  }
}

export default withStyles(styles)(AutocompleteAddressView);
