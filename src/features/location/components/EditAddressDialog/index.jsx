// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import EditAddressDialogView from "./EditAddressDialogView";

type Props = {
  onClose: any,
  onUpdate: any,
  location: any
};

@observer
class EditAddressDialog extends Component<Props> {
  @observable
  selectedLocation: {};

  render() {
    return <EditAddressDialogView controller={this} parentProps={this.props} />;
  }

  @action
  addAddress = (newLocation: Object) => {
    this.props.onUpdate(newLocation);
  };

  @action
  handleNext = () => {};

  @action
  handleCancel = () => {
    this.props.onClose();
  };

  @computed
  get location() {
    return this.props.location;
  }
}

export default EditAddressDialog;
