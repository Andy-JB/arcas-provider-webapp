// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import AutocompleteAddressView from "../AutocompleteAddressView";
import styles from "./styles";

type Props = {
  controller: any,
  parentProps: any
};

@observer
class EditAddressDialogView extends Component<Props> {
  render() {
    const { controller, parentProps } = this.props;
    const { dialogOpen, dialogContentText } = parentProps;
    const { handleClose } = controller;
    return (
      <div>
        <Dialog
          open={dialogOpen}
          fullWidth
          maxWidth="md"
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <DialogContentText>{dialogContentText}</DialogContentText>
            <AutocompleteAddressView controller={controller} isDialog />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditAddressDialogView);
