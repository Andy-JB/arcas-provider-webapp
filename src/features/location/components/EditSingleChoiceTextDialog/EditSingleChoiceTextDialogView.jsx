// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as ServiceUtils from "util/ServiceUtils";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any,
  parentProps: any
};

@observer
class EditSingleChoiceTextDialogView extends Component<Props> {
  render() {
    const { classes, controller, form, parentProps } = this.props;
    const { dialogOpen, options, dialogTitle, buttonTitle } = parentProps;
    const { handleClose } = controller;
    const textField = form.$("textField");
    const type = null;
    const errors = form.errors();
    return (
      <div>
        <Dialog
          open={dialogOpen}
          fullWidth
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form className={classes.form} onSubmit={form.onSubmit}>
            <DialogTitle id="form-dialog-title">{dialogTitle}</DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                required
                margin="dense"
                select
                {...textField.bind({ type })}
                fullWidth
                error={errors.textField != null}
                helperText={errors.textField}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu
                  }
                }}
              >
                {options.map(option => (
                  <MenuItem key={option.id} value={option.id}>
                    <FontAwesomeIcon
                      className={classes.serviceIcon}
                      icon={ServiceUtils.getIconForService(option.id)}
                      size="1x"
                    />
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="primary">
                {buttonTitle}
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditSingleChoiceTextDialogView);
