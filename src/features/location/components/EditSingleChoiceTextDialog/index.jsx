// @flow
import React, { Component } from "react";
import { action, computed } from "mobx";
import { observer } from "mobx-react";
import MobxForm from "util/MobxForm";
import EditSingleChoiceTextDialogView from "./EditSingleChoiceTextDialogView";

type Props = {
  onClose: any,
  onUpdate: any,
  selectedOption: any,
  textFieldLabel: any,
  itemId: any
};

@observer
class EditSingleChoiceTextDialog extends Component<Props> {
  render() {
    return (
      <EditSingleChoiceTextDialogView
        controller={this}
        parentProps={this.props}
        form={new MobxForm(this.formFields, this.onSuccess, this.onError)}
      />
    );
  }

  @action
  onSuccess = (form: any) => {
    const selectedOptionId = form.values().textField;
    this.props.onUpdate(selectedOptionId, this.props.itemId);
  };

  @action
  onError = () => {};

  @action
  handleClose = () => {
    this.props.onClose();
  };

  @computed
  get formFields(): Array<Object> {
    const { selectedOption, textFieldLabel } = this.props;
    return [
      {
        name: "textField",
        type: "text",
        label: textFieldLabel,
        rules: "required|string",
        value: selectedOption
      }
    ];
  }
}

export default EditSingleChoiceTextDialog;
