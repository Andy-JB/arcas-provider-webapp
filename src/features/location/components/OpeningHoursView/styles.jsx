// @flow
const styles = (theme: any) => ({
  submit: {
    marginTop: theme.spacing(3)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  },
  dayName: {
    marginTop: theme.spacing(1)
  },
  addButton: {
    marginTop: theme.spacing(2)
  }
});

export default styles;
