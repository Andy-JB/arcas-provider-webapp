// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { observable, action, computed } from "mobx";
import moment from "moment";
import OpeningHoursView from "./OpeningHoursView";

type Props = {
  controller: any,
  buttonTitle?: any
};

@observer
class OpeningHours extends Component<Props> {
  @observable
  days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];

  @observable
  openingHours = {};

  componentWillMount() {
    this.setOpeningHours();
  }

  render() {
    return <OpeningHoursView controller={this} parentProps={this.props} />;
  }

  @computed
  get submitButtonTitle(): string {
    const title = this.props.buttonTitle;
    return title === undefined ? "Next" : title;
  }

  @action
  addTime = (day: string) => {
    this.openingHours[day].times.push({
      startTime: "09:00",
      endTime: "17:00",
      hasError: false
    });
  };

  @action
  deleteTime = (day: string, index: number) => {
    if (index === 0) return;
    this.openingHours[day].times.splice(index, 1);
  };

  @action
  updateTime = (event: any, fieldName: string, day: string, index: number) => {
    this.openingHours[day].times[index][fieldName] = event.target.value;
  };

  @action
  handleNext = () => {
    // validate all the timing
    if (this.validate()) {
      // call back or save
      this.props.controller.addOpeningHours(this.openingHours);
      this.props.controller.handleNext();
    }
  };

  @action
  validate = (): boolean => {
    let noError = true;
    this.days.forEach(day => {
      this.openingHours[day].times.forEach((time, index) => {
        if (!this.validateTime(time)) {
          this.setError(day, index, true);
          noError = false;
        } else {
          this.setError(day, index, false);
        }
      });
    });
    return noError;
  };

  @action
  validateTime = (time: { startTime: string, endTime: string }): boolean => {
    const startVal = time.startTime;
    const endVal = time.endTime;

    const startMoment = moment(startVal, "HH:mm");
    const endMoment = moment(endVal, "HH:mm");

    return startMoment.isBefore(endMoment);
  };

  @action
  handleBack = () => {
    // handle back
    this.props.controller.handleBack();
  };

  @action
  handleCancel = () => {
    this.props.controller.handleCancel();
  };

  @action
  handleOpenSwitchChange = (event: any, dayName: string) => {
    this.openingHours[dayName].isOpen = event.target.checked;

    // TODO: Check how this is used
    // if (!event.target.checked) {
    //   this.openingHours[dayName].times.map((time, index) =>
    //     this.deleteTime(dayName, index)
    //   );
    // }
  };

  @action
  setError = (dayName: string, timeIndex: number, showError: boolean) => {
    this.openingHours[dayName].times[timeIndex].hasError = showError;
  };

  setOpeningHours = () => {
    const openingHoursObj = JSON.parse(
      JSON.stringify(this.props.controller.openingHours)
    );

    this.days.forEach(day => {
      const dayObj = openingHoursObj[day];
      let openingTimes = [];
      let openThisDay = true;
      if (dayObj != null) {
        openingTimes = dayObj.times;
        openThisDay = dayObj.isOpen;
      } else {
        openingTimes.push({
          // default time 9 to 5
          startTime: "09:00",
          endTime: "17:00",
          hasError: false
        });
        openThisDay = true;
      }

      this.openingHours[day] = {
        isOpen: openThisDay,
        times: openingTimes
      };
    });
  };
}
export default OpeningHours;
