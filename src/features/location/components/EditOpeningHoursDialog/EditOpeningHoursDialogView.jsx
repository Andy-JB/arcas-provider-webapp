// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import OpeningHoursView from "../OpeningHoursView";
import styles from "./styles";

type Props = {
  controller: any,
  parentProps: any
};

@observer
class EditOpeningHoursDialogView extends Component<Props> {
  render() {
    const { controller, parentProps } = this.props;
    const { dialogOpen, dialogContentText } = parentProps;
    const { openingHoursRef, handleClose, handleUpdate } = controller;
    return (
      <div>
        <Dialog
          open={dialogOpen}
          fullWidth
          maxWidth="md"
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Opening Hours</DialogTitle>
          <DialogContent>
            <DialogContentText>{dialogContentText}</DialogContentText>
            <OpeningHoursView
              ref={openingHoursRef}
              controller={controller}
              isDialog
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleUpdate} color="primary">
              Update
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditOpeningHoursDialogView);
