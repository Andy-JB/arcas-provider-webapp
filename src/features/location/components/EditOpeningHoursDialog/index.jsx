// @flow
import React, { Component } from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import EditOpeningHoursDialogView from "./EditOpeningHoursDialogView";

type Props = {
  onClose: any,
  openingHours: any,
  onUpdate: any,
  itemId: any
};

@observer
class EditOpeningHoursDialog extends Component<Props> {
  @observable
  openingHoursRef: ?Object = null;

  constructor(props: Object) {
    super(props);
    this.openingHoursRef = React.createRef();
  }

  render() {
    return (
      <EditOpeningHoursDialogView controller={this} parentProps={this.props} />
    );
  }

  @action
  handleUpdate = () => {
    if (this.openingHoursRef && this.openingHoursRef.current.validate()) {
      this.props.onUpdate(
        this.openingHoursRef.current.openingHours,
        this.props.itemId
      );
    }
  };

  @action
  handleClose = () => {
    this.props.onClose();
  };

  @computed
  get openingHours() {
    return this.props.openingHours;
  }
}

export default EditOpeningHoursDialog;
