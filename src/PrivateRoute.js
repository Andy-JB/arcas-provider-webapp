import React from "react";
import { Route, Redirect } from "react-router-dom";

export default function PrivateRoute({
  component: Component, // eslint-disable-line
  accessRule, // eslint-disable-line
  redirectUrl = "/login", // eslint-disable-line
  ...rest // eslint-disable-line
}) {
  return (
    <Route
      {...rest}
      render={props =>
        accessRule === true ? (
          <Component {...props} {...rest} />
        ) : (
          <Redirect to={redirectUrl} />
        )
      }
    />
  );
}
