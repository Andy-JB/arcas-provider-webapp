// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { observer, inject } from "mobx-react";
import AuthLayout from "common/layouts/AuthLayout";
import Grid from "@material-ui/core/Grid";
import DashboardOverlapLayout from "common/layouts/DashboardOverlapLayout";
import NotFoundIcon from "@material-ui/icons/ErrorOutlined";
import styles from "./styles";

type Props = {
  AuthStore: any,
  classes: any
};

@inject("AuthStore")
@observer
class PageNotFoundView extends Component<Props> {
  render() {
    const { AuthStore, classes } = this.props;
    const { isAuthenticated } = AuthStore;
    if (isAuthenticated) {
      return (
        <DashboardOverlapLayout
          headerIcon={NotFoundIcon}
          pageTitle="Page Not Found"
        >
          <Grid container>
            <div className={classes.dashboardPageContainer}>
              <Typography variant="subtitle">Where are we?</Typography>
              <Typography variant="subtitle">
                Use the sidebar to get back to somewhere more interesting!
              </Typography>
            </div>
          </Grid>
        </DashboardOverlapLayout>
      );
    }
    return (
      <AuthLayout>
        <Typography>Page Not Found</Typography>
      </AuthLayout>
    );
  }
}

export default withStyles(styles)(PageNotFoundView);
