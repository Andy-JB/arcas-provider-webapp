// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import PageNotFoundView from "./PageNotFoundView";

type Props = {};

@inject("AuthStore")
@observer
class PageNotFoundScreen extends Component<Props> {
  render() {
    return <PageNotFoundView />;
  }
}

export default PageNotFoundScreen;
