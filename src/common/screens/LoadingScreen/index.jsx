// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer, inject } from "mobx-react";
import CircularProgress from "@material-ui/core/CircularProgress";
import styles from "./styles";

type Props = {
  classes: any
};

@inject("AuthStore")
@observer
class LoadingScreen extends Component<Props> {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.loading}>
        <CircularProgress size={88} />
      </div>
    );
  }
}

export default withStyles(styles)(LoadingScreen);
