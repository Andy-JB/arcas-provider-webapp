// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import { observer, inject } from "mobx-react";
import { observable, action } from "mobx";
import { Typography } from "@material-ui/core";
import DashboardIcon from "@material-ui/icons/DashboardOutlined";
import LocationAdd from "@material-ui/icons/AddLocation";
import LocationOn from "@material-ui/icons/LocationOnOutlined";
import LocalOffer from "@material-ui/icons/LocalOfferOutlined";
import Lock from "@material-ui/icons/LockOutlined";
import Grid from "@material-ui/core/Grid";
import history from "appHistory";
import LoadingScreen from "common/screens/LoadingScreen";
import styles from "./styles";
import SideBarListItem from "./SideBarListItem";

type Props = {
  UserStore: any,
  LocationStore: any,
  classes: any,
  mobileOpen: boolean,
  onClose: any,
  mobileOnly: boolean
};

@inject("UserStore", "LocationStore")
@observer
class SideBar extends Component<Props> {
  @observable
  open: boolean = false;

  @observable
  logoUrl: string;

  @observable
  logoError: string;

  render() {
    const { classes, mobileOpen, onClose, mobileOnly, UserStore } = this.props;
    const drawerContents = this.renderDrawer(UserStore.pendingReferences === 0);
    if (mobileOnly) {
      return (
        <div className={classes.root}>
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={onClose}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawerContents}
          </Drawer>
        </div>
      );
    }

    return (
      <div className={classes.root}>
        <Hidden mdUp>
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={onClose}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawerContents}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper
            }}
          >
            {drawerContents}
          </Drawer>
        </Hidden>
      </div>
    );
  }

  @action
  navigateToUrl = (url: string) => {
    this.props.onClose();
    history.push(url);
  };

  renderDashboardButton = () => {
    const urlPath = "/dashboard";
    return (
      <SideBarListItem
        title="Dashboard"
        isSelected={this.currentUrlContains(urlPath)}
        onClick={() => this.navigateToUrl(urlPath)}
        viewRoles={this.props.UserStore.ORGADMIN}
        icon={<DashboardIcon />}
      />
    );
  };

  renderOffersButton = () => {
    const urlPath = "/supplieroffers";
    return (
      <SideBarListItem
        title="Supplier Offers"
        isSelected={this.currentUrlContains(urlPath)}
        onClick={() => this.navigateToUrl(urlPath)}
        viewRoles={this.props.UserStore.ORGADMIN}
        icon={<LocalOffer />}
      />
    );
  };

  renderLocationsButton = () => {
    const urlPath = "/locations";
    return (
      <SideBarListItem
        title="My Location"
        isSelected={this.currentUrlContains(`${urlPath}/overview`)}
        onClick={() => this.navigateToUrl(`${urlPath}/overview`)}
        viewRoles={this.props.UserStore.ORGADMIN}
        icon={<LocationOn />}
      />
    );
  };

  renderAddLocationButton = () => {
    const urlPath = "/locations";
    return (
      <SideBarListItem
        title="Add Location"
        isSelected={this.currentUrlContains(`${urlPath}/addlocation`)}
        onClick={() => this.navigateToUrl(`${urlPath}/addlocation`)}
        viewRoles={this.props.UserStore.ORGADMIN}
        icon={<LocationAdd />}
      />
    );
  };

  renderAdminButton = () => {
    const urlPath = "/admin";
    return (
      <div>
        <SideBarListItem
          title="Admin"
          isSelected={this.currentUrlContains(urlPath)}
          viewRoles={this.props.UserStore.SUPER}
          icon={<Lock />}
        >
          <SideBarListItem
            title="Manage Users"
            isSelected={this.currentUrlContains(`${urlPath}/users`)}
            onClick={() => this.navigateToUrl(`${urlPath}/users`)}
            viewRoles={this.props.UserStore.SUPER}
          />
          <SideBarListItem
            title="Activity Logs"
            isSelected={this.currentUrlContains(`${urlPath}/auditactivity`)}
            onClick={() => this.navigateToUrl(`${urlPath}/auditactivity`)}
            viewRoles={this.props.UserStore.SUPER}
          />
        </SideBarListItem>
      </div>
    );
  };

  renderUsersButton = () => {
    const urlPath = "/manageusers";
    return (
      <SideBarListItem
        title="Manage Users"
        isSelected={this.currentUrlContains(urlPath)}
        onClick={() => this.navigateToUrl(urlPath)}
        viewRoles={this.props.UserStore.SUPER}
        icon={<Lock />}
      />
    );
  };

  renderDrawer = (fetchedOrg: boolean) => {
    const { classes, UserStore, LocationStore } = this.props;
    if (!fetchedOrg) {
      return <LoadingScreen />;
    }
    return (
      <div>
        <Grid container className={classes.toolbar}>
          <Grid container className={classes.orgToolbar}>
            <Grid item xs={12} className={classes.orgLogoContainer}>
              {UserStore.orgLogo != null ? (
                <img
                  src={UserStore.orgLogo}
                  alt="Org Logo"
                  className={classes.orgLogo}
                />
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <Typography
                className={
                  LocationStore.location
                    ? classes.organisationName
                    : classes.organisationNameNoLocation
                }
                variant="body1"
              >
                {fetchedOrg && UserStore.user.organisation.orgName}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography
                className={classes.organisationCaption}
                variant="body1"
              >
                {LocationStore.location && LocationStore.location.title}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <div className={classes.menuItemContainer}>
          <Typography variant="caption" className={classes.groupCaption}>
            APPLICATION
          </Typography>
          {this.renderDashboardButton()}
          {LocationStore.location && this.renderLocationsButton()}
          {!LocationStore.location && this.renderAddLocationButton()}
          <Typography variant="caption" className={classes.groupCaption}>
            SERVICES
          </Typography>
          {this.renderOffersButton()}
        </div>
        {UserStore.isAuthorised(UserStore.SUPER, false) ? (
          <div>
            <Typography variant="caption" className={classes.groupCaption}>
              ADMIN
            </Typography>
            {this.renderAdminButton()}
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  };

  currentUrlContains(url) {
    return history.location.pathname.includes(url);
  }
}

export default withStyles(styles)(SideBar);
