// @flow

const selectedListItemStyles = () => ({
  itemSelected: {
    marginTop: 0,
    marginRight: 10,
    backgroundColor: "#039be5",
    "border-top-right-radius": "30px 30px",
    "border-bottom-right-radius": "30px 30px"
  },
  textSelected: {
    color: "#fff"
  },
  iconSelected: {
    color: "#fff"
  }
});

const styles = () => ({
  ...selectedListItemStyles(),
  itemNormal: {
    marginRight: 10,
    "border-top-right-radius": "30px 30px",
    "border-bottom-right-radius": "30px 30px",
    "&:hover": {
      marginRight: 10
    },
    color: "#fff"
  },
  textNormal: {
    color: "#fff"
  },
  iconNormal: {
    color: "#fff"
  },
  noIconStyle: {
    paddingLeft: 40
  },
  expand: {
    maxWidth: 15,
    maxHeight: 15
  },
  listItemContainerSelected: {
    marginTop: 0,
    backgroundColor: "rgba(0, 0, 0, 0.15)"
  }
});

export default styles;
