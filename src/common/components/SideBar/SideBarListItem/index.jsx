// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import { observer, inject } from "mobx-react";
import { observable, computed } from "mobx";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";
import styles from "./styles";

type Props = {
  AuthStore: any,
  UserStore: any,
  title: string,
  icon: any,
  children: any,
  onClick: any,
  viewRoles: any,
  classes: any,
  isSelected: boolean,
  isChild: boolean
};

@inject("AuthStore", "UserStore")
@observer
class SideBarListItem extends Component<Props> {
  @observable
  expandOpen: boolean;

  constructor(props: Object) {
    super(props);
    if (props.isSelected && props.children) {
      this.expandOpen = true;
    } else {
      this.expandOpen = false;
    }
  }

  renderExpand = () => {
    const { classes } = this.props;
    if (this.expandOpen) {
      return <ExpandLess className={classes.expand} />;
    }
    return <ExpandMore className={classes.expand} />;
  };

  renderCollapsable = () => {
    const { children } = this.props;
    const childComponents = React.Children.map(children, child =>
      React.cloneElement(child, {
        isChild: true
      })
    );
    if (this.hasSubItems) {
      return (
        <Collapse in={this.expandOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {childComponents}
          </List>
        </Collapse>
      );
    }
    return null;
  };

  toggleExpand = () => {
    this.expandOpen = !this.expandOpen;
  };

  render() {
    const { title, icon, isChild, classes } = this.props;
    let { onClick } = this.props;
    if (this.hasSubItems) {
      onClick = this.toggleExpand;
    }
    if (this.canView) {
      return (
        <React.Fragment>
          <div className={this.containerStyle}>
            <ListItem
              dense={isChild}
              button
              onClick={onClick}
              className={this.listItemStyle}
            >
              {icon ? (
                <ListItemIcon className={this.iconStyle}>{icon}</ListItemIcon>
              ) : (
                <div className={classes.noIconStyle} />
              )}
              <ListItemText
                disableTypography
                primary={
                  <Typography type="subtitle" className={this.textStyle}>
                    {title}
                  </Typography>
                }
              />
              {this.hasSubItems && this.renderExpand()}
            </ListItem>
            {this.hasSubItems && this.renderCollapsable()}
          </div>
        </React.Fragment>
      );
    }
    return null;
  }

  @computed
  get textStyle() {
    const { classes } = this.props;
    if (this.childItemIsSelected) {
      return classes.textSelected;
    }
    return classes.textNormal;
  }

  @computed
  get iconStyle() {
    const { classes } = this.props;
    if (this.childItemIsSelected) {
      return classes.iconSelected;
    }
    return classes.iconNormal;
  }

  @computed
  get listItemStyle() {
    const { classes } = this.props;
    if (this.childItemIsSelected) {
      return classes.itemSelected;
    }
    return classes.itemNormal;
  }

  @computed
  get containerStyle() {
    const { classes } = this.props;
    if (this.expandOpen) {
      return classes.listItemContainerSelected;
    }
    if (this.childItemIsSelected) {
      return classes.itemSelected;
    }
    return classes.itemNormal;
  }

  @computed
  get childItemIsSelected(): boolean {
    const { isSelected } = this.props;
    return isSelected && !this.hasSubItems;
  }

  @computed
  get hasSubItems() {
    const { children } = this.props;
    return children != null;
  }

  @computed
  get canView(): boolean {
    if (this.props.AuthStore.isAuthenticated) {
      return this.props.UserStore.isAuthorised(this.props.viewRoles, false);
    }
    return true;
  }
}

export default withStyles(styles)(SideBarListItem);
