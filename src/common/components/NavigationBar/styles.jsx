// @flow
const drawerWidth = 250;

const styles = (theme: any) => ({
  // eslint-disable-line
  root: {
    flexGrow: 1
  },
  appBarWithSideBar: {
    zIndex: 1,
    position: "absolute",
    marginLeft: drawerWidth,
    backgroundColor: "#fff",
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  appBar: {
    zIndex: 1
  },
  flex: {
    flexGrow: 1
  },
  navIcon: {
    color: "#333"
  },
  navIconDescription: {
    color: "#333",
    paddingLeft: 5
  },
  navIconHide: {
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  menuButton: {
    display: "none",
    color: "black"
  },
  arcasLogo: {
    height: 25
  },
  arcasToolbarHeading: {
    color: "#000",
    fontWeight: 500,
    textAlign: "left"
  },
  navButton: {
    height: 40
  },
  navContainer: {
    width: "100%",
    textAlign: "right",
    marginRight: 20
  },
  navigationTitle: {
    color: "black",
    opacity: 0.7,
    paddingLeft: 10,
    textDecoration: "none",
    "&:hover": {
      opacity: 1
    }
  },
  "@media (max-width: 600px)": {
    navButton: {
      display: "none"
    },
    menuButton: {
      color: "black",
      display: "inline",
      marginRight: 15
    }
  }
});

export default styles;
