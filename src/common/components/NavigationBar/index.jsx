// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { observable, computed, action } from "mobx";
import AccountCircle from "@material-ui/icons/AccountCircleOutlined";
import Grid from "@material-ui/core/Grid";
import ArcasLogo from "media/images/arcas-logo-2.png";
import history from "appHistory";
import styles from "./styles";

type Props = {
  AuthStore: any,
  classes: any,
  isAuthenticated: boolean,
  hidePermanentSideBar: boolean,
  onNavbarPress: any,
  UserStore: any,
  UiEventStore: any,
  LocationStore: any
};

@inject("AuthStore", "UserStore", "UiEventStore", "LocationStore")
@observer
class NavigationBar extends Component<Props> {
  @observable
  anchorEl = null;

  render() {
    const { classes, onNavbarPress, hidePermanentSideBar } = this.props;
    return (
      <AppBar
        className={
          hidePermanentSideBar === true
            ? classes.appBar
            : classes.appBarWithSideBar
        }
        title="Test"
      >
        <Toolbar>
          <IconButton
            aria-label="Open menu"
            onClick={onNavbarPress}
            className={this.menuBarStyle()}
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={this.anchorEl}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            open={this.menuOpen}
            onClose={this.handleClose}
          >
            <MenuItem onClick={this.handleSettings}>Settings</MenuItem>
            <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
          </Menu>
          <Grid container className={classes.arcasToolbar}>
            <Grid item xs={1}>
              <img
                alt="Arcas Logo"
                src={ArcasLogo}
                className={classes.arcasLogo}
              />
            </Grid>
            <Grid item xs={11}>
              <Typography
                className={classes.arcasToolbarHeading}
                variant="body2"
              >
                ARCAS
              </Typography>
            </Grid>
          </Grid>
          <div className={classes.navContainer}>
            <Button onClick={this.handleAccountClick} aria-haspopup="true">
              <AccountCircle className={classes.navIcon} />
              <Typography
                className={classes.navIconDescription}
                variant="caption"
              >
                {this.userFirstName} {this.userLastName}
              </Typography>
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    );
  }

  menuBarStyle = () => {
    const { hidePermanentSideBar, classes } = this.props;
    const { isAuthenticated } = this.props.AuthStore;
    if (!hidePermanentSideBar) {
      return classes.navIconHide;
    }
    if (!isAuthenticated) {
      return classes.menuButton;
    }
    return null;
  };

  @action
  handleAccountClick = event => {
    this.anchorEl = event.currentTarget;
  };

  @action
  handleMenu = event => {
    this.anchorEl = event.currentTarget;
  };

  @action
  handleClose = () => {
    this.anchorEl = null;
  };

  @action
  handleDashboard = () => {
    this.handleClose();
    history.push("/dashboard");
  };

  @action
  handleSignIn = () => {
    this.handleClose();
    history.push("/login");
  };

  @action
  handleRegister = () => {
    this.handleClose();
    history.push("/signup");
  };

  @action
  handleSettings = () => {
    history.push("/settings");
  };

  @action
  handleLogout = async () => {
    this.handleClose();
    const { AuthStore, UserStore, LocationStore } = this.props;
    await AuthStore.doSignOut();
    AuthStore.resetStore();
    UserStore.resetStore();
    LocationStore.resetStore();
    history.push("/login");
    this.props.UiEventStore.triggerSnackbarSuccess("Logged out successfully");
  };

  @computed
  get menuOpen(): boolean {
    return Boolean(this.anchorEl);
  }

  @computed
  get userFirstName(): string {
    return this.props.UserStore.user ? this.props.UserStore.user.firstName : "";
  }

  @computed
  get userLastName(): string {
    return this.props.UserStore.user ? this.props.UserStore.user.lastName : "";
  }
}

export default withStyles(styles)(NavigationBar);
