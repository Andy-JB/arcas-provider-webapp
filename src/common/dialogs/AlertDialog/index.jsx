// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import AlertDialogView from "./AlertDialogView";

type Props = {};

@observer
class AlertDialog extends Component<Props> {
  render() {
    return <AlertDialogView controller={this} props={this.props} />;
  }
}

export default AlertDialog;
