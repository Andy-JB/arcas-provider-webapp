// @flow
const styles = (theme: any) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      marginLeft: 100,
      marginRight: 100
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  pageHeading: {
    marginBottom: 20,
    marginTop: 10
  },
  submitButton: {
    marginTop: 30
  },
  changePasswordField: {
    marginBottom: 25
  },
  dropzoneContainer: {
    backgroundColor: "#fff",
    maxWidth: "100%",
    padding: 5,
    margin: "auto"
  },
  dropzoneOnDrag: {
    backgroundColor: "#fff"
  },
  dropzone: {
    border: "dashed #333",
    textAlign: "center",
    height: "100%",
    margin: "auto",
    paddingTop: 30,
    paddingBottom: 30
  },
  panelContainer: {},
  imagePreview: {
    height: 100,
    width: 100
  },
  cloudIcon: {
    color: "#333",
    height: 90,
    width: 100,
    textAlign: "center"
  },
  uploadDescription: {
    color: "#333"
  },
  loading: {
    textAlign: "center"
  }
});

export default styles;
