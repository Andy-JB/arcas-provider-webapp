// @flow
import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { observable, action } from "mobx";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dropzone from "react-dropzone";
import CloudUploadOutlined from "@material-ui/icons/CloudUploadOutlined";
import CircularProgress from "@material-ui/core/CircularProgress";
import styles from "./styles";

type Props = {
  classes: any,
  displayLoading: boolean,
  openDialog: boolean,
  onClose: () => mixed,
  onSubmit: (any, any) => mixed,
  disableActions: boolean
};

@inject("UserStore")
@observer
class FileUploadDialog extends Component<Props> {
  @observable
  imagePreviewUrl: ?string;

  @observable
  imagePreviewFile: ?string;

  @observable
  isDragging: boolean = false;

  @observable
  dropzoneStyle: any;

  resetImagePreview = () => {
    this.imagePreviewUrl = undefined;
    this.imagePreviewFile = undefined;
  };

  render() {
    const {
      openDialog,
      onClose,
      onSubmit,
      displayLoading,
      disableActions
    } = this.props;
    return (
      <Dialog
        fullWidth
        maxWidth="md"
        open={openDialog}
        onRendered={this.resetImagePreview}
        onClose={onClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Upload Image</DialogTitle>
        <DialogContent>
          {displayLoading ? this.renderLoading() : this.renderDropzone()}
        </DialogContent>
        <DialogActions>
          <Button disabled={disableActions} onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button
            disabled={this.imagePreviewUrl == null || disableActions}
            onClick={() =>
              onSubmit(this.imagePreviewFile, this.imagePreviewUrl)
            }
            color="primary"
          >
            Upload
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  updateDropzoneStyle(isHovering: boolean) {
    const { classes } = this.props;
    if (isHovering) {
      this.dropzoneStyle = classes.dropzoneOnDrag;
    } else {
      this.dropzoneStyle = classes.dropzoneContainer;
    }
  }

  renderLoading = () => {
    const { classes } = this.props;
    return (
      <div className={classes.loading}>
        <CircularProgress />
      </div>
    );
  };

  renderDropzone = () => {
    const { classes } = this.props;
    return (
      <Dropzone
        accept="image/jpeg, image/png, image/jpg"
        onDrop={this.onImageDropped}
      >
        {({ getRootProps, getInputProps }) => (
          <section>
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <div className={classes.dropzone}>
                {this.imagePreviewUrl ? (
                  <img
                    src={this.imagePreviewUrl}
                    alt="img-preview"
                    className={classes.imagePreview}
                  />
                ) : (
                  <CloudUploadOutlined className={classes.cloudIcon} />
                )}

                <Typography
                  className={classes.uploadDescription}
                  variant="h6"
                  component="h2"
                >
                  {this.imagePreviewUrl ? (
                    <div>
                      <b>Press upload</b> to upload the image
                    </div>
                  ) : (
                    <div>
                      <b>Choose a file</b> or drag it here
                    </div>
                  )}
                </Typography>
              </div>
            </div>
          </section>
        )}
      </Dropzone>
    );
  };

  @action
  onImageDropped = files => {
    const file = files[0];
    const reader: Object = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imagePreviewUrl = reader.result;
      this.imagePreviewFile = file;
    };
  };
}

export default withStyles(styles)(FileUploadDialog);
