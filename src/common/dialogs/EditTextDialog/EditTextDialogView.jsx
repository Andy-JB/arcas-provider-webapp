// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import styles from "./styles";

type Props = {
  controller: any,
  classes: any,
  form: any,
  props: any
};

@observer
class EditTextDialogView extends Component<Props> {
  render() {
    const { classes, controller, form, props } = this.props;
    const textField = form.$("textField");
    const type = null;
    const errors = form.errors();
    return (
      <div>
        <Dialog
          open={props.dialogOpen}
          fullWidth
          onClose={controller.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form className={classes.form} onSubmit={form.onSubmit}>
            <DialogTitle id="form-dialog-title">
              {props.dialogTitle}
            </DialogTitle>
            <DialogContent>
              <DialogContentText>{props.dialogContentText}</DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                {...textField.bind({ type })}
                multiline={props.multiline}
                fullWidth
                error={errors.textField != null}
                helperText={errors.textField}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={controller.handleClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="primary">
                {props.buttonTitle}
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditTextDialogView);
