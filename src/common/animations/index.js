// @flow
import posed from "react-pose";

export const FadeInAnimation = posed.div({
  open: { opacity: 1, staggerChildren: 100 },
  closed: { opacity: 0 }
});

export const FadeInUpAnimation = posed.div({
  open: { y: 0, opacity: 1 },
  closed: { opacity: 0, y: 100 }
});

export const FadeInLeftAnimation = posed.div({
  open: { x: 0, opacity: 1 },
  closed: { opacity: 0, x: -100 }
});

export const FadeInRightAnimation = posed.div({
  open: { x: 0, opacity: 1 },
  closed: { opacity: 0, x: 100 }
});

export const AnimatedView = posed.div({
  open: { opacity: 1, staggerChildren: 0 },
  closed: { opacity: 1 }
});
