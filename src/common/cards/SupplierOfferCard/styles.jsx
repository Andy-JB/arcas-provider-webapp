// @flow

const card = (theme: any) => ({
  card: {
    display: "flex",
    width: "100%"
  },
  normalCard: {},
  normalMedia: {
    height: 140
  },
  cardAvatar: {
    width: 30,
    height: 30,
    marginRight: 10,
    marginTop: "auto",
    marginBottom: "auto"
  },
  cardOrg: {
    float: "clear"
  },
  orgContainer: {
    display: "flex",
    flexDirection: "row"
  },
  orgLogo: {
    maxWidth: 100,
    maxHeight: 50,
    padding: 10,
    overflow: "hidden"
  },
  locationContainer: {
    display: "flex",
    flexDirection: "row",
    paddingLeft: 5
  },
  dateContainer: {
    paddingTop: 5,
    paddingLeft: 6
  },
  dateLocationContainer: {
    paddingTop: 15
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "100%"
  },
  content: {
    flex: "1 0 auto",
    width: "100%"
  },
  cover: {
    minWidth: 151,
    maxWidth: 151
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  playIcon: {
    height: 38,
    width: 38
  }
});

const styles = (theme: any) => ({
  ...card(theme)
});

export default styles;
