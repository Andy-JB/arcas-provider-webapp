// @flow
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observer } from "mobx-react";
import { Grid } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Switch from "@material-ui/core/Switch";
import Button from "@material-ui/core/Button";
import HelpIcon from "@material-ui/icons/Help";
import MaterialTooltip from "@material-ui/core/Tooltip";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as ServiceUtils from "util/ServiceUtils";
import styles from "./styles";

type Props = {
  classes: any,
  service: any,
  toggleServiceEnabled: (any, any) => mixed,
  onCapacityChange: (any, any) => mixed
};

@observer
class ServiceDashboardCard extends React.Component<Props> {
  render() {
    const { classes, service, toggleServiceEnabled } = this.props;
    return (
      <Grid item xs={12} sm={6} md={6} lg={4}>
        <Paper className={classes.serviceCard}>
          <Grid container>
            <Grid item xs={1}>
              <FontAwesomeIcon
                className={classes.serviceIcon}
                icon={ServiceUtils.getIconForService(service.category)}
                size="2x"
              />
            </Grid>
            <Grid item xs={8} className={classes.serviceCardHeading}>
              <div>
                <Typography className={classes.serviceCardText} variant="body2">
                  {service.category}
                </Typography>
              </div>
              <Typography className={classes.serviceCardText} variant="caption">
                Open until {service.endTime}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Switch
                checked={service.isEnabled}
                className={classes.serviceSwitch}
                color="primary"
                onChange={(event: any) =>
                  toggleServiceEnabled(service.id, event.target.checked)
                }
              />
              <MaterialTooltip
                title="Setting to 'Off' will hide your service from users"
                placement="top"
              >
                <HelpIcon fontSize="small" />
              </MaterialTooltip>
            </Grid>
          </Grid>
          <Grid container className={classes.trafficContainer}>
            <Grid item xs={12}>
              <Typography className={classes.serviceCardText} variant="body2">
                Toggle service capacity
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.sliderContainer}>
              {this.renderCapacityButton("LOW", classes.lowButton, service)}
              {this.renderCapacityButton(
                "MEDIUM",
                classes.mediumButton,
                service
              )}
              {this.renderCapacityButton("FULL", classes.fullButton, service)}
              {this.renderCapacityButton("NA", classes.naButton, service)}
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    );
  }

  onCapacityClick = (label: any, service: any) => {
    this.props.onCapacityChange(service.id, label);
  };

  renderCapacityButton = (label, className, service) => {
    if (service.capacityLevel !== label) {
      return (
        <Button
          xs={3}
          onClick={() => this.onCapacityClick(label, service)}
          variant="text"
        >
          {label}
        </Button>
      );
    }
    return (
      <Button
        xs={3}
        onClick={() => this.onCapacityClick(label, service)}
        variant="contained"
        className={className}
      >
        {label}
      </Button>
    );
  };
}

export default withStyles(styles)(ServiceDashboardCard);
