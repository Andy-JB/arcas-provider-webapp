// @flow
import red from "@material-ui/core/colors/red";
import green from "@material-ui/core/colors/green";
import yellow from "@material-ui/core/colors/yellow";

const styles = (theme: any) => ({
  wouldReturnText: {
    paddingRight: 20,
    paddingTop: 10,
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 10
    }
  },
  ratingContainer: {
    paddingLeft: 8,
    paddingTop: 10
  },
  descriptionContainer: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  ratingDateContainer: {
    display: "flex",
    flexDirection: "row"
  },
  thumbsUpIcon: {
    color: green[800]
  },
  thumbsDownIcon: {
    color: red[800]
  },
  ratingIcon: {
    paddingTop: 10,
    textAlign: "right"
  },
  starIconActive: {
    color: yellow[800]
  },
  starIconHidden: {
    color: "gray",
    opacity: 0.2
  },
  content: {
    padding: 0
  },
  card: {
    minHeight: 100,
    flexGrow: 1
  }
});

export default styles;
