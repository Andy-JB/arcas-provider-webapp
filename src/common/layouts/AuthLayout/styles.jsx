// @flow
import SplashBackground from "media/images/adelaide-photo1.jpg";

const styles = (theme: any) => ({
  content: {
    height: "100%"
  },
  sideContainer: {
    background: `url(${SplashBackground}) no-repeat center center`,
    backgroundSize: "auto 100%",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    },
    minHeight: "1000px"
  },
  arcasLogoMobileContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 40,
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 60,
      paddingRight: 60
    },
    [theme.breakpoints.up("md")]: {
      display: "none"
    },
    paddingBottom: 20
  },
  splashArcasLogo: {
    width: 45,
    height: 40,
    top: 0,
    left: 0,
    paddingRight: 10,
    float: "left"
  },
  splashArcasLogoMobile: {
    width: 40,
    height: 40,
    float: "left"
  },
  arcasTitle: {
    paddingLeft: 50,
    paddingTop: 10,
    color: "#fff"
  },
  splashDescriptionContainer: {
    position: "absolute",
    marginTop: 120,
    paddingBottom: 80,
    paddingLeft: 100,
    color: "#fff"
  },
  featureContainer: {
    paddingTop: 8
  },
  featureTitle: {
    paddingTop: 2,
    color: "#fff"
  },
  iconContainer: {
    height: 30,
    width: 30,
    padding: 3,
    marginRight: 10,
    float: "left"
  },
  clear: {
    clear: "both"
  },
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: `url(${SplashBackground})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
});

export default styles;
