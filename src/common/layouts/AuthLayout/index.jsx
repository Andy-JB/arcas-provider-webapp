// @flow
import React, { Component } from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import CssBaseline from "@material-ui/core/CssBaseline";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import styles from "./styles";

type Props = {
  children: any,
  classes: any
};

@observer
class AuthLayout extends Component<Props> {
  @observable
  mobileOpen: boolean;

  @observable
  isOpen: boolean = false;

  constructor(props: Object) {
    super(props);
    setTimeout(this.startAnimating, 0);
  }

  startAnimating = () => {
    this.isOpen = !this.isOpen;
  };

  render() {
    const { children, classes } = this.props;
    return (
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>{children}</div>
        </Grid>
      </Grid>
    );
  }

  handleDrawerToggle = () => {
    this.mobileOpen = !this.mobileOpen;
  };
}

export default withStyles(styles)(AuthLayout);
