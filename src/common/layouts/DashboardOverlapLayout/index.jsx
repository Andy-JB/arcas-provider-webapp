// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observable, action, computed } from "mobx";
import { observer } from "mobx-react";
import { Grid, Paper, Button } from "@material-ui/core";
import SideBar from "common/components/SideBar";
import NavigationBar from "common/components/NavigationBar";
import Typography from "@material-ui/core/Typography";
import DefaultIcon from "@material-ui/icons/SettingsOutlined";
import BackIcon from "@material-ui/icons/ArrowBack";
import {
  FadeInUpAnimation,
  FadeInAnimation,
  AnimatedView
} from "common/animations";

import styles from "./styles";

type Props = {
  classes: any,
  pageTitle: any,
  actionButton: any,
  secondaryActionButton: any,
  backButton: any,
  children: any,
  animatePaper: any,
  headerIcon: any
};

@observer
class DashboardOverlapLayout extends Component<Props> {
  @observable
  mobileOpen: boolean;

  @observable
  isOpen: boolean = false;

  constructor(props: Object) {
    super(props);
    setTimeout(this.startAnimating, 0);
  }

  startAnimating = () => {
    this.isOpen = !this.isOpen;
  };

  render() {
    const {
      classes,
      pageTitle,
      actionButton,
      secondaryActionButton,
      backButton,
      children,
      animatePaper
    } = this.props;
    let { headerIcon } = this.props;
    if (!headerIcon) {
      headerIcon = DefaultIcon;
    }
    const HeaderIcon = headerIcon;
    const pageHeaderStyle =
      actionButton != null
        ? classes.pageHeaderContainerWithAction
        : classes.pageHeaderContainer;
    const pageDescStyle =
      actionButton != null
        ? classes.pageDescriptionContainerWithAction
        : classes.pageDescriptionContainer;
    return (
      <AnimatedView
        pose={this.isOpen ? "open" : "closed"}
        className={classes.root}
      >
        <NavigationBar onNavbarPress={this.handleDrawerToggle} />
        <SideBar
          mobileOpen={this.mobileOpen}
          onClose={this.handleDrawerToggle}
        />
        <Grid container className={classes.appContent}>
          <Grid item xs={12}>
            <Grid item xs={12}>
              <div className={pageHeaderStyle}>
                <Grid container className={pageDescStyle}>
                  <Grid
                    item
                    xs={12}
                    sm={9}
                    className={classes.pageHeaderDetailsContainer}
                  >
                    <div>
                      {backButton && (
                        <Button
                          className={classes.backButton}
                          onClick={backButton.onClick}
                        >
                          <BackIcon className={classes.backIcon} />
                          {backButton.label}
                        </Button>
                      )}
                    </div>
                    <div className={classes.pageTitleContainer}>
                      <FadeInAnimation>
                        <HeaderIcon className={classes.pageIcon} />
                      </FadeInAnimation>
                      <Typography
                        className={classes.pageTitle}
                        variant="h5"
                        component="h1"
                      >
                        <FadeInAnimation>{pageTitle}</FadeInAnimation>
                      </Typography>
                    </div>
                  </Grid>
                  {this.hasActionButtons && (
                    <Grid
                      className={classes.actionButtonContainer}
                      item
                      xs={12}
                      sm={3}
                    >
                      <FadeInUpAnimation>
                        <Button
                          className={`${classes.actionButton} ${actionButton.className}`}
                          variant="contained"
                          color="primary"
                          onClick={actionButton.onClick}
                        >
                          {actionButton.label}
                        </Button>
                      </FadeInUpAnimation>
                      {secondaryActionButton && (
                        <FadeInUpAnimation>
                          <Button
                            className={`${classes.actionButton} ${secondaryActionButton.className}`}
                            variant="contained"
                            color="primary"
                            onClick={secondaryActionButton.onClick}
                          >
                            {secondaryActionButton.label}
                          </Button>
                        </FadeInUpAnimation>
                      )}
                    </Grid>
                  )}
                </Grid>
              </div>
            </Grid>
            <main className={classes.pageContent}>
              {animatePaper ? (
                <FadeInUpAnimation>
                  <Paper className={classes.pagePaperContainer}>
                    {children}
                  </Paper>
                </FadeInUpAnimation>
              ) : (
                <Paper className={classes.pagePaperContainer}>{children}</Paper>
              )}
            </main>
          </Grid>
        </Grid>
      </AnimatedView>
    );
  }

  @computed
  get hasActionButtons(): boolean {
    const { actionButton, secondaryActionButton } = this.props;
    return actionButton || secondaryActionButton;
  }

  @action
  handleDrawerToggle = () => {
    this.mobileOpen = !this.mobileOpen;
  };
}

export default withStyles(styles)(DashboardOverlapLayout);
