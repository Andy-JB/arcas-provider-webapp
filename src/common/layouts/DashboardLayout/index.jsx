// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Grid } from "@material-ui/core";
import SideBar from "common/components/SideBar";
import NavigationBar from "common/components/NavigationBar";
import Typography from "@material-ui/core/Typography";
import DefaultIcon from "@material-ui/icons/SettingsOutlined";
import styles from "./styles";

type Props = {
  classes: any,
  children: any,
  pageTitle: any,
  header: any,
  noPadding: boolean,
  headerIcon: any
};

@observer
class DashboardLayout extends Component<Props> {
  @observable
  mobileOpen: boolean;

  handleDrawerToggle = () => {
    this.mobileOpen = !this.mobileOpen;
  };

  render() {
    const { classes, children, pageTitle, header, noPadding } = this.props;
    let { headerIcon } = this.props;
    if (!headerIcon) {
      headerIcon = DefaultIcon;
    }
    const HeaderIcon = headerIcon;
    return (
      <div className={classes.root}>
        <NavigationBar onNavbarPress={this.handleDrawerToggle} />
        <SideBar
          mobileOpen={this.mobileOpen}
          onClose={this.handleDrawerToggle}
        />
        <Grid container className={classes.appContent}>
          <Grid item xs={12}>
            <Grid item xs={12}>
              <div className={classes.pageDescriptionContainer}>
                <HeaderIcon className={classes.pageIcon} />
                <Typography
                  className={classes.pageTitle}
                  variant="h5"
                  component="h1"
                >
                  {pageTitle}
                </Typography>
                <div>{header}</div>
              </div>
            </Grid>
            <main
              className={
                noPadding ? classes.pageContent : classes.pageContentPadded
              }
            >
              {children}
            </main>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(DashboardLayout);
