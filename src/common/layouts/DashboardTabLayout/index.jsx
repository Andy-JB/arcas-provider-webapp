// @flow
import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import { Tabs, Tab } from "@material-ui/core";
import history from "appHistory";
import LoadingScreen from "common/screens/LoadingScreen";
import styles from "./styles";
import DashboardOverlapLayout from "../DashboardOverlapLayout";

type Props = {
  classes: any,
  pageTitle: any,
  location: any,
  tabs: any,
  initialTab: any,
  actionButton: any,
  secondaryActionButton: any,
  onTabChange: any,
  headerIcon: any,
  showLoading: boolean
};

@observer
class DashboardTabLayout extends Component<Props> {
  @observable
  mobileOpen: boolean;

  @observable
  currentTab: number;

  constructor(props: Object) {
    super(props);
    this.setupInitialTab();
  }

  render() {
    const {
      classes,
      pageTitle,
      actionButton,
      secondaryActionButton,
      tabs,
      headerIcon
    } = this.props;
    const CurrentComponent = tabs[this.currentTab].component;
    return (
      <DashboardOverlapLayout
        actionButton={actionButton}
        secondaryActionButton={secondaryActionButton}
        pageTitle={pageTitle}
        headerIcon={headerIcon}
      >
        <Tabs
          className={classes.tabs}
          value={this.currentTab}
          onChange={this.handleTabChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
        >
          {tabs.map(tab => (
            <Tab className={classes.tab} key={tab.label} label={tab.label} />
          ))}
        </Tabs>
        {!this.props.showLoading && <CurrentComponent isActiveTab />}
        {this.props.showLoading && <LoadingScreen />}
      </DashboardOverlapLayout>
    );
  }

  setupInitialTab = () => {
    const { initialTab, tabs, location } = this.props;
    if (initialTab != null && tabs != null) {
      if (initialTab < tabs.length) {
        this.currentTab = initialTab;
        this.notifyTabChange(this.currentTab);
        return;
      }
    }
    if (location) {
      const index = tabs.findIndex(tab => tab.path === location.hash);
      this.currentTab = index === -1 ? 0 : index;
      this.notifyTabChange(this.currentTab);
    } else {
      this.currentTab = 0;
    }
  };

  @action
  handleTabChange = (event, value) => {
    this.currentTab = value;
    const { path } = this.props.tabs[this.currentTab];
    history.push(path);
    this.notifyTabChange(this.currentTab);
  };

  @action
  notifyTabChange = (newTab: number) => {
    const { onTabChange } = this.props;
    if (onTabChange) {
      onTabChange(newTab);
    }
  };

  @action
  handleDrawerToggle = () => {
    this.mobileOpen = !this.mobileOpen;
  };
}

export default withStyles(styles)(DashboardTabLayout);
