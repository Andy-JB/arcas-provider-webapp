module.exports = {
  "parser": "babel-eslint",
  "extends": ["airbnb", "plugin:flowtype/recommended", "prettier", "prettier/flowtype", "prettier/react"],
  "plugins": [
    "react",
    "flowtype",
    "jsx-a11y",
    "prettier"
  ],
  "settings": {
    "import/resolver": {
      "node": {
        "paths": ["src", "node_modules"]
      }
    },
  },
  "rules": {
    "react/require-default-props": [0, "never"],
    "react/jsx-filename-extension": [
      1, { "extensions": [".js", ".jsx"] }
    ],
    "react/forbid-prop-types": [0, { "forbid": [] }],
    "import/extensions": [1, "never", { "svg": "always" }],
    "import/no-extraneous-dependencies": [
      "error",
      {
        "devDependencies": true,
        "optionalDependencies": false,
        "peerDependencies": false
      }
    ],
    "react/sort-comp": [0, "never"],
    "semi": [0, "never"],
    "padded-blocks": [0, "never"],
    "no-use-before-define": ["error", { "variables": false }],
    "class-methods-use-this": [0, "never"],
    "react/destructuring-assignment": [0, "never"],
    "react/prefer-stateless-function": [0, "never"],
    "prettier/prettier": "error",
    "no-use-before-define": [0, "false"]
},
"env": {
  "browser": true
}
};

